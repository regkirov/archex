%% ARCHitectural EXploration Framework (ArchEx 2.0) - RPL DEMO
%
% This script demonstrates the ArchEx functionality on the case study of
% Reconfigurable Production Lines (RPL), in particular, using flow, load
% and timing constraints in the problem formulation. Architectural
% templates have different sizes, i.e., one can also see the scalability.
%
% For details please refer to RPL_DEMO_README.txt.
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: March 2017

%% Demo problem input file
%  Please select one of the problems and then just run the script.
%
%% ========================================================================
% RPL_DEMO1: RPL architecture with 2 product types (A,B), 2 machines along
% the functional flow and 2 operation modes.
problem_file = 'demos/RPL_DEMO1.txt';
%% ========================================================================
% RPL_DEMO2: Similar to RPL_DEMO1, but with extra timing constraint that
% limits the total idle rate of machines in the RPL.
%problem_file = 'demos/RPL_DEMO2.txt';
%% ========================================================================
% RPL_DEMO3: example of a larger RPL with 3 product types and longer
% functional flow (4 machines) - NOT IMPLEMENTED YET
%problem_file = 'demos/RPL_DEMO3.txt';
%% ========================================================================
% RPL_DEMO4: example of an UNFEASIBLE problem.
%problem_file = 'demos/RPL_DEMO4.txt';
% =========================================================================

%% launch ArchEx for specified demo EPN problem
ArchEx('RPL', problem_file, 'demos/DEMO_RPL_LIB.txt');

%% nice vizualization
if ~isa(graph, 'double')
    subtypes = {'A', 'B', 'AB'};
    updateGraphColoring(graph, subtypes);
end
