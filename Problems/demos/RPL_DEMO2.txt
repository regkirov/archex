% RPL_DEMO2: another demo for Reconfigurable Production Line (RPL) case
% study with the following difference from RPL_DEMO1: timing constraint. 
% The constraint limits the total idle rate of machines of RPL (the sum of 
% idle rates of machines).
%
% The optimal architecture from RPL_DEMO1 has machines with larger processing
% rate, which is cheaper than having several sub-lines with slower machines.
% However, in this case the resource of these fast machines will be utilized
% at around 25-30% in several operation modes, which means that they spend
% more time in "idle" state instead of "active" (working). The cost of a 
% machine and/or of a business process often includes the utilization rate,
% i.e., it is important not only to minimize the monetary cost but also to
% maximize the fraction of total time, in which the machine is performing
% its task (is not idle).
%
% In this demo, the architecture is more expensive in terms of monetary cost
% of machines and conveyors as it has more sub-lines with slower machines, 
% however, these machines are utilized at around 90% of time. 
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
%
%% PROBLEM DESCRIPTION SECTION
BEGIN (Description) 
Name: RPL DEMO 2;
Variable components: Machine, Conveyor;
Fixed components: Source, Sink;
Machines: Machine1, Machine2;
Functional flow: Source,Conveyor1,Machine1,Conveyor2,Machine2,Conveyor3,Sink;
Tags: A,B;
Edge cost: 1000;
END (Description)
%
%% TEMPLATE STRUCTURE SECTION
%  (how many components of each type for each production sub-line; in RPL 
%   exploration problem tags (A,B) are used to denote sub-lines)
% 
BEGIN (Template)
[A] Source 1;
[A] Conveyor1 3;
[A] Machine1 3;
[A] Conveyor2 3;
[A] Machine2 3;
[A] Conveyor3 3;
[A] Sink 1;
[B] Source 1;
[B] Conveyor1 2;
[B] Machine1 2;
[B] Conveyor2 2;
[B] Machine2 2;
[B] Conveyor3 2;
[B] Sink 1;
END (Template)
%
%% COMPOSITION RULES OF TEMPLATE/LIBRARY COMPONENTS
%  (what can be connected, sort of whitelist; everything not mentioned here
%  cannot be connected)
%
BEGIN (Composition rules)
[A] Source     => [A] Conveyor1;
[B] Source     => [B] Conveyor1;
[A] Conveyor1  => [A] Conveyor1;
[A] Conveyor1  => [B] Conveyor1;
[A] Conveyor1  => [A] Machine1;
[B] Conveyor1  => [B] Conveyor1;
[B] Conveyor1  => [A] Conveyor1;
[B] Conveyor1  => [B] Machine1;
[A] Machine1   => [A] Conveyor2;
[B] Machine1   => [B] Conveyor2;
[A] Conveyor2  => [A] Conveyor2;
[A] Conveyor2  => [B] Conveyor2;
[A] Conveyor2  => [A] Machine2;
[B] Conveyor2  => [B] Conveyor2;
[B] Conveyor2  => [A] Conveyor2;
[B] Conveyor2  => [B] Machine2;
[A] Machine2   => [A] Conveyor3;
[B] Machine2   => [B] Conveyor3;
[A] Conveyor3  => [A] Conveyor3;
[A] Conveyor3  => [B] Conveyor3;
[A] Conveyor3  => [A] Sink;
[B] Conveyor3  => [B] Conveyor3;
[B] Conveyor3  => [A] Conveyor3;
[B] Conveyor3  => [B] Sink;
END (Composition rules)
%
%% CONSTRAINTS SECTION
%  Interconnection (and other) constraints are defined here using patterns
%
BEGIN (Constraints)
%
% Every sources and every sink must be connected to exactly one conveyor
exactly_N_connections(Source A, Conveyor1 A,1);
exactly_N_connections(Source B, Conveyor1 B,1);
exactly_N_connections(Sink A, Conveyor3 A,1);
exactly_N_connections(Sink B, Conveyor3 B,1);
%
% machines cannot be connected to multiple conveyors
at_most_N_connections(Conveyor1,Machine1,1);
at_most_N_connections(Machine1,Conveyor1,1);
at_most_N_connections(Machine1,Conveyor2,1);
at_most_N_connections(Conveyor2,Machine1,1);
at_most_N_connections(Conveyor2,Machine2,1);
at_most_N_connections(Machine2,Conveyor2,1);
at_most_N_connections(Machine2,Conveyor3,1);
at_most_N_connections(Conveyor3,Machine2,1);
%
% conveyors that have some input flow must deliver it to machines
in_conn_implies_out_conn(Conveyor1,Source | Conveyor1, Machine1);
in_conn_implies_out_conn(Conveyor2,Machine1 | Conveyor2, Machine2);
% 
% machines that have input flow from a conveyor must have output to another 
% conveyor
in_conn_implies_out_conn(Machine1, Conveyor1, Conveyor2);
in_conn_implies_out_conn(Machine2, Conveyor2, Conveyor3);
%
% conveyors, which are not connected to any machines, must not have any 
% input product flow
no_out_conn_implies_no_in_conn(Conveyor1,Machine1,Conveyor1);
no_out_conn_implies_no_in_conn(Conveyor2,Machine2,Conveyor2);
no_out_conn_implies_no_in_conn(Conveyor3,Machine2 | Sink,Conveyor3);
%
% conveyors without any input flow cannot be connected to other conveyors
no_in_conn_implies_no_out_conn(Conveyor1,Source|Conveyor1,Conveyor1);
no_in_conn_implies_no_out_conn(Conveyor2,Machine1,Conveyor2);
no_in_conn_implies_no_out_conn(Conveyor3,Machine2,Conveyor3);
%
% no self-loops: conveyor cannot be connected to itself
no_self_loops(Conveyor1);
no_self_loops(Conveyor2);
no_self_loops(Conveyor3);
%
% flow balance equations: input flow to a component must be equal to output flow
flow_balance(Conveyor1);
flow_balance(Conveyor2);
flow_balance(Conveyor3);
flow_balance(Machine1);
flow_balance(Machine2);
%
% machines must not be overloaded (input flow <= output flow)
no_overloads(Machine1);
no_overloads(Machine2);
%
% operation modes
has_operation_mode(1A+1B);
has_operation_mode(2A+0B);
%
% mapping restrictions: machines accepting different product types must be 
% reconfigurable (e.g. machine B accepting units of A must be AB, i.e., 
% support both A and B; AB is a subtype).
in_flow_implies_mapping_to(Machine1 B,A,AB);
in_flow_implies_mapping_to(Machine1 A,B,AB);
in_flow_implies_mapping_to(Machine2 B,A,AB);
in_flow_implies_mapping_to(Machine2 A,B,AB);
%
% machines on line A can support either only product A or both A and B (i.e.
% have subtype A or AB). Same for line B.
at_most_N_components(Machine1 A,B,0);
at_most_N_components(Machine2 A,B,0);
at_most_N_components(Machine1 B,A,0);
at_most_N_components(Machine2 B,A,0);
%
% conveyors on line A and B cannot have other subtype but A and B, respectively
% NOTE: if all conveyors are same this is not needed. Here we do it only for
% nice coloring of the resulting graph.
at_most_N_components(Conveyor1 A,B,0);
at_most_N_components(Conveyor1 B,A,0);
at_most_N_components(Conveyor2 A,B,0);
at_most_N_components(Conveyor2 B,A,0);
at_most_N_components(Conveyor3 A,B,0);
at_most_N_components(Conveyor3 B,A,0);
%
% TIMING CONSTRAINT: limiting the total idle rate of machines to 10 units/sec
max_idle_rate_sum(TOTAL,10);
%
END (Constraints)