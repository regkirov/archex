% EPN_DEMO3: this demo has following difference from EPN_DEMO1: TRU can 
% connect only an HV part to an LV part (extra constraint). With this demo 
% one can obtain a redundant and more expensive architecture.
%
% NOTE: each row starting with "%" is a comment
% For ease of reading and for consistency it is recommended that all unused
% (empty) rows also should start with "%" (though it's not obligatory)
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
%
% PROBLEM DESCRIPTION SECTION
BEGIN (Description) 
Name: EPN DEMO 3;
Variable components: Generator, ACBus, Rectifier, DCBus;
Fixed components: Load;
Functional flow: Generator, ACBus, Rectifier, DCBus, Load;
Tags: Left, Middle, Right;
Edge cost: 1000;
Edge failure: 0;
END (Description)
%
% TEMPLATE STRUCTURE SECTION
% (how many components of each type on each side)
% 
BEGIN (Template)
[Left] Generator 2;
[Left] ACBus 4;
[Left] Rectifier 5;
[Left] DCBus 4;
[Left] Load 8;
[Right] Generator 2;
[Right] ACBus 4;
[Right] Rectifier 5;
[Right] DCBus 4;
[Right] Load 8;
[Middle] Generator 2;
END (Template)
%
% COMPOSITION RULES OF TEMPLATE/LIBRARY COMPONENTS
% (what can be connected, sort of whitelist; everything not mentioned here
% cannot be connected)
%
BEGIN (Composition rules)
[Left]      Generator   => [Left]     ACBus;
[Right]     Generator   => [Right]    ACBus;
[Middle]    Generator   => [Left]     ACBus;
[Middle]    Generator   => [Right]    ACBus;
[Left]      ACBus       => [Left]     ACBus;
[Left]      ACBus       => [Right]    ACBus;
[Left]      ACBus       => [Left]     Rectifier;
[Right]     ACBus       => [Right]    Rectifier;
[Right]     ACBus       => [Left]     ACBus;
[Right]     ACBus       => [Right]    ACBus;
[Left]      Rectifier   => [Left]     DCBus;
[Right]     Rectifier   => [Right]    DCBus;
[Left]      DCBus       => [Right]    DCBus;
[Left]      DCBus       => [Left]     DCBus;
[Left]      DCBus       => [Left]     Load;
[Right]     DCBus       => [Left]     DCBus;
[Right]     DCBus       => [Right]    DCBus;
[Right]     DCBus       => [Right]    Load;
END (Composition rules)
%
% CONSTRAINTS SECTION
% Interconnection (and other) constraints are defined here using patterns
%
BEGIN (Constraints)
%
% Limit the number of connections allowed between components of type A and B
at_most_N_connections(Generator Left, ACBus Left, 1);
at_most_N_connections(Generator Right, ACBus Right, 1);
at_most_N_connections(ACBus Left, Generator Left, 1);
at_most_N_connections(ACBus Right, Generator Right, 1);
at_most_N_connections(Rectifier,ACBus,1);
at_most_N_connections(ACBus,Rectifier,1);
at_most_N_connections(Rectifier,DCBus,1);
at_most_N_connections(DCBus,Rectifier,1);
%
% If A has an outgoing connection to any of B then it must have an incoming
% connection from any of C
out_conn_implies_in_conn(DCBus ,DCBus | Load, Rectifier);
out_conn_implies_in_conn(ACBus, ACBus| Rectifier, Generator);
out_conn_implies_in_conn(Rectifier ,DCBus, ACBus);
%
% If A has an incoming connection from any of B then it must have an outgoing
% connection to any of C
in_conn_implies_out_conn(ACBus, Generator, ACBus | Rectifier);
in_conn_implies_out_conn(Rectifier, ACBus, DCBus);
%
% If A is NOT connected to any of B then it must NOT be connected to any of C
no_out_conn_implies_no_in_conn(ACBus,Rectifier,ACBus);
no_out_conn_implies_no_in_conn(DCBus,Load,DCBus);
no_out_conn_implies_no_in_conn(ACBus,Rectifier | ACBus,Generator Middle);
no_out_conn_implies_no_in_conn(DCBus,Load | DCBus,Rectifier);
%
% Component cannot be connected to itself
no_self_loops(ACBus);
no_self_loops(DCBus);
%
% Each load must be connected to exactly one bus
exactly_N_connections(Load, DCBus,1);
%
% If and only if APU is used then it must be connected to both Left and Right parts
exactly_N_connections_if_used(Generator Middle, ACBus Left,1); 
exactly_N_connections_if_used(Generator Middle, ACBus Right,1);
at_most_N_connections(ACBus Left,Generator Middle,1);
at_most_N_connections(ACBus Right,Generator Middle,1);
%
% Horizontal connections are bidirectional (A connected to B also means B connected to A)
bidirectional_connection(ACBus, ACBus);
bidirectional_connection(DCBus, DCBus);
%
% Power constraints: Components in A (alltogether) must be able to power all components in B
has_sufficient_power(Generator Left, Load Left);
has_sufficient_power(Generator Right, Load Right);
has_sufficient_power(Generator Middle, Load Left + Load Right);
%
% Both HV and LV generators from both Left and Right engines must be used
exactly_N_components(Generator Left, High voltage,1);
exactly_N_components(Generator Left, Low voltage,1);
exactly_N_components(Generator Right, High voltage,1);
exactly_N_components(Generator Right, Low voltage,1);
%
% There must be at least one APU (but can be more)
at_least_N_components(Generator Middle, APU,1);
%
% "Middle" generators can be only APUs
exactly_N_components(Generator Middle, Low voltage,0);
exactly_N_components(Generator Middle, High voltage,0);
%
% Blacklist of connections between certain subtypes of components. Following 
% set basically declares that HV and LV components (any types) cannot be 
% connected together
cannot_connect(Load, High voltage, DCBus, Low voltage);
cannot_connect(Load, Low voltage, DCBus, High voltage);
cannot_connect(DCBus, Low voltage, DCBus, High voltage);
cannot_connect(Rectifier, Low voltage,DCBus, High voltage);
cannot_connect(Rectifier, High voltage,DCBus, Low voltage);
cannot_connect(Rectifier, Low voltage,ACBus, High voltage);
cannot_connect(Rectifier, High voltage,ACBus, Low voltage);
cannot_connect(ACBus, Low voltage, ACBus, High voltage);
cannot_connect(ACBus, Low voltage, Generator, High voltage);
cannot_connect(ACBus, High voltage, Generator, Low voltage);
cannot_connect(ACBus, Low voltage, Generator, APU);
%
% If needed, one can explicitly state that there must be at least 1 TRU on
% each side (though some of them might be eventually used only as rectifiers)
at_least_N_components(Rectifier Left,TRU,1);
at_least_N_components(Rectifier Right,TRU,1);
%
% These can be used to force the optimizer to use TRU ONLY for connecting 
% LV and HV parts together:
cannot_connect(ACBus, Low voltage, Rectifier, TRU);
cannot_connect(DCBus, High voltage, Rectifier, TRU);
%
% reliability requirement for each load is also specified using patterns
max_failprob_of_connection(Load 1,1e-9);
max_failprob_of_connection(Load 2,1e-9);
max_failprob_of_connection(Load 3,1e-5);
max_failprob_of_connection(Load 4,1e-5);
max_failprob_of_connection(Load 5,1e-9);
max_failprob_of_connection(Load 6,1e-9);
max_failprob_of_connection(Load 7,1e-5);
max_failprob_of_connection(Load 8,1e-5);
max_failprob_of_connection(Load 9,1e-9);
max_failprob_of_connection(Load 10,1e-9);
max_failprob_of_connection(Load 11,1e-5);
max_failprob_of_connection(Load 12,1e-5);
max_failprob_of_connection(Load 13,1e-9);
max_failprob_of_connection(Load 14,1e-9);
max_failprob_of_connection(Load 15,1e-5);
max_failprob_of_connection(Load 16,1e-5);
%
END (Constraints)