classdef (HandleCompatible) RPLComponentInterface
% An interface to extend the Industrial (RPL) library components with
% extra label - processing rate.
    
% % Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: December 2016
    
    properties
        ProcessingRate % processing time of a component (RPL-specific value)
    end
    
    methods
        % interface constructor
        function obj = RPLComponentInterface(proc_rate)
            if nargin > 0
                obj.ProcessingRate = proc_rate;
            else
                obj.ProcessingRate = 0;
            end
        end
    end
    
end

