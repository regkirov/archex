classdef RPLProblem < Problem
% RPLPROBLEM: a class for the architecture exploration problem of
% reconfigurable production lines (RPL). An RPL consists of several (semi-)
% automated lines for assembling/processing/packaging different types of
% products (represented by tags here). Each line has a fixed Source, which
% provides initial components/details, a fixed Sink for collecting product
% output from the line, and a set of Machines along the path of the product
% flow, which do the processing. Machines are connected with each other
% with Conveyors. The latter can also be connected together ("horizontal"
% connections) so that products can be forwarded to other lines.
%
% An RPL can have several operation modes, which have different product
% flow rates (different demand). In certain modes demand for some products
% can be zero, which means that corresponding lines are stalled (not used).
% Such lines can be reused (reconfigured) to process other products, which
% have high demand so that their own lines cannot handle it. In such a way
% RPL may be able to support these operation modes at lower system costs,
% without the need of installing extra sub-lines, which can be costly.
%
% RPL Library consists of Source (SRC), Conveyor (C), Machine (M) and Sink
% (SNK) components. They are labeled with costs and subtypes. SRC has input
% flow rate, M have processing rates, C have speed value.
%
% Problem statement:
% FIND 
%   system configuration and library mapping 
% SUBJECT TO 
%   interconnennection, flow, load and timing constraints
% SUCH THAT
%   overall system cost is minimized.
%
% Load constraints presume that machines are not overloaded (can handle the
% input traffic). Timing constraints ensure that cycle time requirements
% are satisfied and total system idle time is bounded by a value.
%
% Problem input consists of problem and library text files, which are
% organized similarly to EPS problem files (see description of EPSProblem)
%
% Author: Dmitrii Kirov
% Last modified: December 2016
    
    properties (GetAccess = public, SetAccess = {?Helpers})
        % data structures used to store the input of the problem        
        machines                               
        
        % costs
        SystemCost
        EdgeCost
        
        % misc
        solved  
    end
    
    properties (Access = public)
        % debug
        total_idle
%         solver_in
%         solver_out
    end
    
    methods (Access = public)
        function obj = RPLProblem(name, lib_filename, input_filename, algorithm)
            obj@Problem(name, lib_filename, input_filename, algorithm);
            if nargin == 4
                str = sprintf('RPLProblem: Creating new problem instance (name = %s)', obj.Name);
                disp(str);
                
                % load the library of components from the input file
                obj.Library = RPLLibrary(lib_filename);                                
                
                % load and process the problem input: description variables
                % (e.g. Flow, Tags) and template structure (max number of
                % components of each type/tag, composition rules)
                obj.ProcessDescription;
                obj.ProcessTemplate;                
                
                % define data structures, which will act as input and
                % output of the optimization problem.
                obj.DefineDecisionVariables;                                                
                
                % init requirements
                obj.SetRequirement();
                
                % read and process the constraints from the input file
                obj.ProcessConstraints;
                
                % create the cost function
                obj.DefineCostFunction;                                                                                              
                
                % initialize the analysis class (for RPL problem it is the
                % timing analysis)
                obj.Analysis = TimingAnalysis(obj);
            else
                error('RPLProblem: incorrect number of input parameters.');
            end
        end
        
        
        function ProcessDescription(obj)
            % call the same method from Problem to do common stuff
            obj.ProcessDescriptionGeneric();

            % RPL-specific input is the list of component types, which are
            % processing machines
            pattern = '(?m)(?<=^(Machines:))[^;]*';
            res = char(regexp(obj.input.description, pattern, 'match'));
            obj.machines = strtrim(strsplit(res, ','));                                 
        end
        
        
        function ProcessTemplate(obj)
            % call the same method from Problem to do common stuff
            obj.ProcessTemplateGeneric();
            
            % problem-specific code can be added here
            %
        end                
        
        
        function ProcessConstraints(obj)            
            % call the same method from Problem to do common stuff
            obj.ProcessConstraintsGeneric();
            
            % ConstraintHandler class is responsible for parsing the input
            % file for constraints, classifying them and getting their MILP
            % formulations using patterns
            obj.ConstraintHandler = ConstraintHandler(obj, obj.input.constraints);
            
            % get general (e.g. max number of components), interconnection,
            % flow and load MILP constraints. They will be used both in 
            % iterative and monolithic problems.
            milp_constraints = obj.ConstraintHandler.getConstraintsMILP(obj, ...
                obj.ConstraintHandler.General,...
                obj.ConstraintHandler.Interconnection,...
                obj.ConstraintHandler.Operation);
            
            obj.addConstraint(milp_constraints);
            
            if strcmp(obj.Algorithm, 'eager')
                disp('RPLProblem: extra variables required for the "eager" algorithm. Creating...');
                aux_milp_constraints = obj.ConstraintHandler.getConstraintsMILP(obj, ...
                    obj.ConstraintHandler.Flow,...
                    obj.ConstraintHandler.Workload,...
                    obj.ConstraintHandler.Timing,...
                    obj.ConstraintHandler.Mapping);                
                obj.addAuxConstraint(aux_milp_constraints); 
            end                                                                       
        end                                                               
        
        
        function DefineDecisionVariables(obj)
            disp('RPLProblem: Creating decision variables and data structures...');
            
            % create the adjacency matrix
            obj.AdjacencyMatrix = AdjacencyMatrix(obj.NumComponents, ...
                obj.ConnectionRules);
            
            % add adjacency matrix constraints to ensure that all 
            % connections not mentioned in composition rules are forbidden
            adjm_constraints = obj.AdjacencyMatrix.getConstraints(0, obj.Flow);
            obj.addConstraint(adjm_constraints);
            
            % generate the ordered list of matrix keys (starting from
            % source and ending with sink following the functional flow)
            ordered_keys = {};
            for i = obj.Flow
                for j = obj.Tags
                    new_key = [char(i), ' ', char(j)];
                    new_key_index = length(ordered_keys) + 1;
                    ordered_keys{new_key_index} = new_key; 
                end
            end

            % save the key order to the matrix object for future use
            obj.AdjacencyMatrix.setOrdering(ordered_keys); 
            
            % initialize the LibraryMapping variables and add corresponding
            % mapping constraints
            [obj.LibraryMapping, mapping_constraints] = ...
                Helpers.createLibraryMapping(obj); 
            obj.addConstraint(mapping_constraints);                        
            
            % initialize the storage of flow rate matrices
            obj.flowRateMatrices = containers.Map;
        end
        
        
        function DefineCostFunction(obj)
            % Compose a cost function. For an RPL problem the objective is
            % composed from monetary cost of components and horizontal
            % edges, which represent junction/connection conveyors, and the
            % idle time of machines
            disp('RPLProblem: Defining the cost function...');            
            
            % init
            obj.SystemCost   = 0;
            obj.CostFunction = 0;
            
            %% monetary cost
            for i = 1:length(obj.Flow)                
                % go through the functional flow
                component = obj.Flow{i};
                is_variable_component = 0;
                for j = obj.components_variable
                   % check if current component is variable or fixed
                   j = char(j);
                   if ~isempty(strfind(component, j))
                       is_variable_component = 1;
                       library_type_of_component = j;
                   end
                end               
                
                if is_variable_component                
                    % get the mapping matrix for current component type. The
                    % matrix is used to determine how many "real" components
                    % (from the library) are used in the solution. This is then
                    % multiplied by the vector of costs.
                    costs = obj.Library.Costs(library_type_of_component);
                    current_component_mapping = obj.LibraryMapping(component);
                    current_component_cost = sum(costs.*sum(current_component_mapping,2)');

                    % update the total cost function
                    obj.SystemCost = obj.SystemCost + current_component_cost;
                end
                
                % account for horizontal connections (junctions)
                if Helpers.canBeConnected(obj, component, component)
                    % if horizontal connections are allowed on this level
                    connectivity_matrix = ...
                        obj.AdjacencyMatrix.getSubMatrixNumeric(component, component);
                    num_junctions = sum(sum(connectivity_matrix));
                    obj.SystemCost = obj.SystemCost + num_junctions * obj.edge_cost;
                end
            end
            
            % other parts of the cost function can be added here (e.g. cost
            % of idle time/stalls). However, the cost function is single as
            % CPLEX does not support multiobjective optimization. Weight
            % coefficients is one workaround, but they must be chosen very
            % accurately.
            % obj.StallCost = ...
            
            % weight coefficients
            alpha = 1;
            %beta  = 0.5;
            
            %% define the final cost function
            obj.CostFunction = alpha * obj.SystemCost;
        end
        
        
        function GetTimingConstraints(obj)
        end                
        
        
        function SetRequirement(obj, varargin)
            % set the requirement for the RPL problem:
            % (a) operation requirement (op modes)
            % (b) timing requirement (ToDo)
            if length(varargin) == 0
                obj.Requirement.Operation = {};
            else
                new_req = varargin{1};
                len = length(obj.Requirement.Operation);
                obj.Requirement.Operation{len + 1} = new_req;
            end
        end
        
        
        function Solve(obj, options)
            % solve the RPL problem using a selected algorithm
            if nargin == 1
                options = [];
            end
            
            if strcmp(obj.Algorithm, 'eager') == 1
                % monolithic
                disp('RPLProblem: monolithic ("eager") algorithm selected.');
                obj.Solution = eagerAlgorithm(obj, options);
            elseif strcmp(obj.Algorithm, 'lazy') == 1
                %% ToDo: iterative
                disp('RPLProblem: iterative approach is not yet implemented.');
                %disp('RPLProblem: iterative ("lazy") algorithm selected.');
                %obj.Solution = lazyAlgorithm(obj, obj.Requirement.Timing, options);
            else
                str = sprintf('RPLProblem: the specified algorithm (%s) is not implemented', obj.Algorithm);
                disp(str);
            end                                                                       
            
            obj.solved = 1;
            str = sprintf('Elapsed time: %0.2fs', obj.Solution.time_elapsed);
            disp(str);
            fprintf('============================================================\n');
        end                
        
        
        function graph = ShowResult(obj)
            % prepare the results, draw the resulting graph 
            disp('RPLProblem: Drawing the graph...');
            
            % descriptions (information about mapping of each component).
            % In the graph it will appear by double clicking on a node.
            %% ToDo: this is also generic?
            num_components = obj.AdjacencyMatrix.Size;
            descriptions = cell(1, num_components);
            count = 1;
            for i = 1:length(obj.Flow)
                current_mapping = value(obj.LibraryMapping(obj.Flow{i}));
                current_mapping = round(current_mapping);
                lib_component = obj.Library.findRelatedType(obj.Flow{i});
                current_real_components = ...
                    obj.Library.getComponentsByType(lib_component);
                
                for j = 1:size(current_mapping, 2)
                    if sum(current_mapping(:,j)) == 0
                        % virtual component is not instantiated
                        current_description = 'NOT USED';
                        descriptions{count} = current_description;
                    else
                        library_idx = find(current_mapping(:,j),1);
                        real_component = current_real_components(library_idx);
                        descriptions{count} = real_component.print;
                    end
                    count = count + 1;
                end
            end
            
            % draw the graph
            graph = plotAdjacencyMatrix(obj, descriptions, 'final');
            
            % These are hard-coded default subtypes used in demo.
            % To make own graph coloring one should provide his own list of
            % subtypes (cell array)
            default_subtypes = {'A', 'B', 'AB'};
            updateGraphColoring(graph, default_subtypes);
            
            % store most recent node descriptions for future use
            obj.node_descriptions = descriptions;
        end        
        
        
        function Save(obj)
            timestamp = datetime('now');
            timestamp.Format = 'yyyy-MM-dd HH:mm:ss';
            timestamp = char(timestamp);
            timestamp = strrep(timestamp, '-', '_');
            timestamp = strrep(timestamp, ' ', '_');
            timestamp = strrep(timestamp, ':', '_');
            filename = sprintf('Output/RPL_problem_%s.mat', timestamp);
            
            %% ToDo: finalize the saving function                                    
            fprintf('\nRPLProblem: Saving the results to a .mat file...\n');
            % saving of sdpvar objects is not possible, therefore, to save
            % the adjacency matrix object we create its copy, in which all
            % sdpvar-s are replaced with their current numerical values.
            % This does not affect the values in the original matrix.
            adjacency_matrix = copy(obj.AdjacencyMatrix);
            adjacency_matrix.getVersionForSaving;

            %timing_analysis = obj.Analysis.last_result;
            requirement          = obj.Requirement.Operation;
            cost                 = value(obj.CostFunction);
            library              = obj.Library;
            node_descriptions    = obj.node_descriptions;
            
            problem_data.Library             = obj.Library;            
            problem_data.NumComponents       = obj.NumComponents;
            problem_data.Tags                = obj.Tags;
            problem_data.ConnectionRules     = obj.ConnectionRules;
            problem_data.Flow                = obj.Flow;
            problem_data.components_variable = obj.components_variable;
            problem_data.components_fixed    = obj.components_fixed;
            problem_data.edge_cost           = obj.edge_cost;            
            
            time_elapsed = obj.Solution.time_elapsed;
            
            % save library mappings
            mapping = containers.Map;
            for i = 1:length(obj.Flow)
                mapping(obj.Flow{i}) = ...
                    round(value(obj.LibraryMapping(obj.Flow{i})));
            end
            
            save(filename, 'adjacency_matrix', 'cost','problem_data', ...
                           'requirement', 'mapping', 'time_elapsed',  ...
                           'library', 'node_descriptions');
            
            % saving the resulting graph to a png file            
            disp('RPLProblem: Saving the architecture graph to a png file...');
            filename = sprintf('Output/graphs/RPL_problem_%s.png', timestamp);
            graph = obj.ShowResult;
            graph.fig.Visible = 'off';
            saveas(graph.fig, filename);
                                   
            % saving cplex input to a txt file
            disp('RPLProblem: Saving CPLEX input to a txt file...');
            filename = sprintf('Output/cplex/RPL_problem_%s.txt', timestamp);
            solver_in = obj.Solution.solver_data.solverinput.model;
            saveCplexInput(solver_in, filename);
            
            disp('RPLProblem: Done saving.');
        end
        
        
        function setAlgorithm(obj, new_algorithm)
            % version of generic setAlgorithm() for RPL problem
            if strcmp(new_algorithm, 'eager') && isempty(obj.AuxiliaryConstraints)                
                disp('RPLProblem: extra variables required for the "eager" algorithm. Creating...');
                aux_milp_constraints = obj.ConstraintHandler.getConstraintsMILP(obj, ...
                    obj.ConstraintHandler.Flow,...
                    obj.ConstraintHandler.Load,...
                    obj.ConstraintHandler.Timing,...
                    obj.ConstraintHandler.Mapping);                
                obj.addAuxConstraint(aux_milp_constraints); 
            end
            obj.Algorithm = new_algorithm;
        end
        
        
        function [ new_constraints ] = LearnCons(obj, result)
            % LearnCons is a part of the iterative algorithm. For the RPL
            % problem it takes the result of the timing analysis as
            % an input and using the current solution heuristically adds 
            % extra constraints to guide the solver to a better solution.
            new_constraints = obj.learning_heuristic_fast(result);
        end 
        % public methods end        
    end
    
    
    methods (Access = private)               
        function [ new_constraints ] = learning_heuristic_fast(obj, result)
            % 
        end                                     
        % private methods end
    end
end