classdef Source < Component & RPLComponentInterface
    %SOURCE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (GetAccess = public, SetAccess = private)
        InputFlowRate
    end
    
    
    methods
        % constructor
        function obj = Source(name, subtype, cost, proc_rate, input_flow)
            % calling superclass constructors
            obj@Component(name, cost, subtype);
            obj@RPLComponentInterface(proc_rate);
            if nargin > 4
                obj.InputFlowRate = input_flow;
            end
        end
        
        
        function res = print(obj)
            rows{1} = sprintf('Name: %s', obj.Name);
            rows{2} = sprintf('Type: %s', obj.Type);
            rows{3} = sprintf('Subtype: %s', obj.Subtype);
            rows{4} = sprintf('Cost: %d', obj.Cost);
            rows{5} = sprintf('Processing rate (1/sec): %d', obj.ProcessingRate);
            rows{6} = sprintf('Input flow rate (1/sec): %d', obj.InputFlowRate);
            res = sprintf('%s\n', rows{:});
        end
    end
    
end

