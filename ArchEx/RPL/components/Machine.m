classdef Machine < Component & RPLComponentInterface
    %MACHINE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (GetAccess = public, SetAccess = private)
    end
    
    
    methods
        % constructor
        function obj = Machine(name, subtype, cost, proc_rate)
            % calling superclass constructors
            obj@Component(name, cost, subtype);
            obj@RPLComponentInterface(proc_rate);
        end
        
        
        function res = print(obj)
            rows{1} = sprintf('Name: %s', obj.Name);
            rows{2} = sprintf('Type: %s', obj.Type);
            rows{3} = sprintf('Subtype: %s', obj.Subtype);
            rows{4} = sprintf('Cost: %d', obj.Cost);
            rows{5} = sprintf('Processing rate (1/sec): %d', obj.ProcessingRate);            
            res = sprintf('%s\n', rows{:});
        end
    end
    
end

