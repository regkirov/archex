classdef EPNLibrary < Library
% EPNLibrary is an implementation of a components library for the problem 
% of aircraft electrical power network (EPN) design exploration.
%
% Default EPN components are Generator (G), ACBus (B), Rectifier (R), DCBus
% (D) and Load (L). This can be extended with more types if needed. All
% components are labeled with cost, subtype (e.g. High/Low voltage) and
% failure probability. Additionally, G are labeled with power ratings, B
% and D - with power capacities and L with power requirements (all this is
% stored in the same Powers map for different types as keys).
% 
% DefaultTypes cell does not contain L, because in the original EPN problem
% formulation Loads are fixed (their final number is not a decision
% variable), while other components are variable. This can be modified in
% the Problem class if required.
%
% This class extends the abstract class Library by implementing domain
% specific versions of loadLibraryFromFile method for parsing the library
% input txt file. 
%
% createMapping method is used to formulate mapping constraints over
% library components; it is called from createLibraryMapping function in
% Helpers class.
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: December 2016
    
    properties (Constant)
        DefaultTypes = {'Generator', 'ACBus', 'Rectifier', 'DCBus'}
        DefaultName = 'Electrical power network (EPN) component library'
    end
    
    
    properties (SetAccess = private, GetAccess = public)
        Powers
        FailureProbabilities
    end
    
    
    methods (Access = public)
        function obj = EPNLibrary(filename, types)
            % initialize the components data structure with types (either
            % provided with the constructor or default).
            if nargin > 1
                obj.ComponentTypes = types;
            else
                obj.ComponentTypes = obj.DefaultTypes;
            end
                        
            obj.Powers                  = containers.Map;
            obj.FailureProbabilities    = containers.Map;
            
            % initialize the dictionary (map) structure for the library
            initComponentsMap(obj);
            % load the library from the given TXT file
            obj.loadLibraryFromFile(filename);
            % process "subtype" fields for each component type and create
            % helper structures for later associating library mappings to
            % particular subtypes (used in many constraints)
            obj.sortComponentsBySubtype;
        end
        
        
        function loadLibraryFromFile(obj, filename)
            % Load the library from a TXT file.
            % Function first finds the list of types of components, which
            % are present in the library. It then scans the file for these
            % components and their parameters (using regular expressions),
            % creates corresponding objects and adds them to the Components
            % data structure.
            
            path = './Libraries/';
            path = strcat(path, filename);
            data = fileread(path);
            
            % clear the previously existing content, if any
            if obj.Size > 0
                obj.clear();
            end
            
            pattern = '(?m)^Name:[^;]*';
            name = regexp(data, pattern, 'match');
            if isempty(name)
                warning('Library name not found. Will use the default name');
                obj.Name = obj.DefaultName;
            elseif length(name) > 1
                warning('Multiple library names found. Will use the default name');
                obj.Name = obj.DefaultName;
            else
                name = char(name);
                obj.Name = strtrim(name(6:end));
            end
            
            % find the list of component types
            pattern = '(?m)^Types:[^;]*';
            types = regexp(data, pattern, 'match');
            if isempty(types)
                error('The list of component types is not found in the library');
            elseif length(types) > 1
                error('More than 1 list of component types is found. Should be only 1');
            else
                % initialize the library structure
                types = char(types);
                types = strtrim(strsplit(types(7:end), ','));
                obj.setComponentTypes(types);
                initComponentsMap(obj);
            end
            
            for type = types
                % find rows, which represent particular components
                pattern = strcat('(?m)^', char(type), ':[^;]*');
                rows = regexp(data, pattern, 'match');
                
                % for each component type we store its subtypes
                subtypes_for_current_type = containers.Map;
                names = obj.Names(char(type));
                
                if isempty(rows)
                    warning('Components of type %s were not found in the library',...
                        char(type));
                    continue
                else
                    for row = rows
                        % if found, parse parameters for each of them
                        str_row = char(row);
                        str_row = str_row(length(char(type))+2:end);
                        vars = strsplit(str_row, ',');
                        
                        name    = strtrim(vars{1});
                        cost    = strtrim(vars{2});
                        subtype = strtrim(vars{3});
                        fprob   = strtrim(vars{4});
                        
                        % try-catch because some components (e.g. R) are
                        % not labeled with powers
                        try
                            power = strtrim(vars{5});
                        catch
                            power = '';
                        end
                        
                        cost    = str2double(cost);
                        fprob   = str2double(fprob);
                        power   = str2double(power);
                        
                        % create a component from the parsed data
                        if strcmp(char(type), 'Generator') == 1
                            new_component = Generator(name, cost, subtype, ...
                                fprob, power);
                        elseif strcmp(char(type), 'ACBus') == 1
                            maxpower = power;
                            new_component = ACBus(name, cost, subtype, fprob,...
                                maxpower);
                        elseif strcmp(char(type), 'Rectifier') == 1
                            new_component = Rectifier(name, cost, subtype, ...
                                fprob);
                        elseif strcmp(char(type), 'DCBus') == 1
                            maxpower = power;
                            new_component = DCBus(name, cost, subtype, fprob,...
                                maxpower);
                        elseif strcmp(char(type), 'Load') == 1
                            powerreq = power;
                            new_component = Load(name, cost, subtype, fprob, ...
                                powerreq);
                        % Transformator is an extra type for testing
                        elseif strcmp(char(type), 'Transformator') == 1                            
                            new_component = Transformator(name, cost, subtype, ...
                                fprob);
                        % LVACBus is an extra type for testing
                        elseif strcmp(char(type), 'LVACBus') == 1
                            new_component = LVACBus(name, cost, subtype, fprob,...
                                maxpower);
                        else
                            % some other component, for which a class is
                            % not yet implemented
                        end
                        
                        % extend existing arrays with new values
                        l = length(names);
                        names{l+1} = name;
                        obj.Costs(char(type)) = [obj.Costs(char(type)),...
                            cost];                        
                        obj.FailureProbabilities(char(type)) = ...
                            [obj.FailureProbabilities(char(type)), fprob];
                        obj.Powers(char(type)) = [obj.Powers(char(type)),...
                            power];
                        
                        % initialize the mapping for current kind
                        subtypes_for_current_type(subtype) = [];
                        
                        % add the component to the library
                        obj.addComponent(new_component);
                    end
                end
                obj.Names(char(type)) = names;
                obj.Subtypes(char(type)) = subtypes_for_current_type;
            end            
        end
        
        
        function [matrix, cons] = createMapping(obj, component_type, mapping_layer)
            % This function creates the mapping between library components
            % of a certain type and "virtual" components of the same type 
            % from the EPN problem. The mapping is represented by a matrix
            % (rows - library components, columns - virtual components) and
            % a set of constraints, which link this mapping matrix to
            % slices of the adjacency matrix used in optimization. The
            % mapping matrix consists of decision variables (binvar) and is
            % an important part of optimization result.
            % 
            % Slices of the adjacency matrix, to which the mapping matrix
            % is "associated" is the "mapping_layer". Variables from this
            % layer can show if the component is used (instantiated) or
            % not. In our formulation we consider component used if it has
            % an incoming or an outgoing edge (a connection to some other
            % node).
            %
            % Example: need to map "virtual" generators to real devices
            % present in the library. We can use connectivity matrix
            % between generators and AC buses to determine if a particular
            % generator is used (and hence has to be mapped) or not.
            
            components = obj.getComponentsByType(component_type);
            num_rows = length(components);
            cons = [];            
            num_columns = size(mapping_layer, 1);
            
            % initialize the mapping matrix (its dimensions are equal to
            % number of real and virtual devices, respectively)
            matrix = binvar(num_rows, num_columns, 'full');
            
            % constraint 1: each virtual component can be mapped to maximum
            % one real component from the library
            cons = [cons, Patterns.at_most_one_mapping(matrix)];
            
            % constraint 2: if a virtual component is connected to some
            % other component (i.e., it is instantiated) then it must be
            % mapped to a real device from the library.
            cons = [cons, Patterns.if_used_then_has_mapping(mapping_layer', matrix)];
        end
    end
    
    
    methods (Access = private)
        function initComponentsMap(obj)
            % Init each group of components with an empty vector.
            % Also init component parameters lists with empty vectors for
            % each type.
            for type = obj.ComponentTypes
                obj.Names(char(type))               = {};
                obj.Components(char(type))          = [];
                obj.Costs(char(type))               = [];
                obj.Powers(char(type))              = [];
                obj.FailureProbabilities(char(type))= [];
            end
        end
        
        
        function sortComponentsBySubtype(obj)
            % Go through the list of each component type and through
            % corresponding subtypes (different values of the "Subtype" 
            % field for a component) and for each subtype create a binary 
            % array (length is equal to number of components of this type),
            % which  has "1" if this is a component of this subtype and "0" 
            % otherwise.
            %
            % These helper structures will be used for defining constraints
            % related to subtypes of components (e.g. "High voltage
            % generators" cannot be connected to Low voltage generators")
            for type = obj.ComponentTypes
                current_subtype = obj.Subtypes(char(type));
                for subtype = keys(obj.Subtypes(char(type)))
                    mapping_to_current_subtype = zeros(1,length(obj.Components(char(type))));
                    idx = 1;
                    for component = obj.Components(char(type))
                        if strcmp(component.Subtype, char(subtype)) == 1
                            mapping_to_current_subtype(idx) = 1;
                        end
                        idx = idx + 1;
                    end                    
                    current_subtype(char(subtype)) = mapping_to_current_subtype;
                end
                obj.Subtypes(char(type)) = current_subtype;
            end
        end
    end
    % private methods end
end

