classdef Load < Component & EPNComponentInterface
    %LOAD Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (GetAccess = public, SetAccess = private)
        PowerRequirement
    end
    
    
    methods
        % constructor
        function obj = Load(name, cost, subtype, fprob, powerreq)
            % calling superclass constructors
            obj@Component(name, cost, subtype);
            obj@EPNComponentInterface(fprob);
            if nargin > 0
                obj.PowerRequirement = powerreq;
            end
        end
        
        
        function res = print(obj)
            rows{1} = sprintf('Name: %s', obj.Name);
            rows{2} = sprintf('Type: %s', obj.Type);
            rows{3} = sprintf('Subtype: %s', obj.Subtype);
            rows{4} = sprintf('Cost: %d', obj.Cost);
            rows{5} = sprintf('Failure probability: %d', obj.FailureProbability);
            rows{6} = sprintf('Power requirement: %d', obj.PowerRequirement);
            res = sprintf('%s\n', rows{:});
        end
    end
    
end

