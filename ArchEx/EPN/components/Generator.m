classdef Generator < Component & EPNComponentInterface
    %GENERATOR Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (GetAccess = public, SetAccess = private)
        PowerCapacity
    end
    
    
    methods
        % constructor
        function obj = Generator(name, cost, subtype, fprob, power)
            % calling superclass constructors
            obj@Component(name, cost, subtype);
            obj@EPNComponentInterface(fprob);
            if nargin > 0
                obj.PowerCapacity = power;
            end
        end
        
        
        function res = print(obj)
            rows{1} = sprintf('Name: %s', obj.Name);
            rows{2} = sprintf('Type: %s', obj.Type);
            rows{3} = sprintf('Subtype: %s', obj.Subtype);
            rows{4} = sprintf('Cost: %d', obj.Cost);
            rows{5} = sprintf('Failure probability: %d', obj.FailureProbability);
            rows{6} = sprintf('Power capacity: %d', obj.PowerCapacity);
            res = sprintf('%s\n', rows{:});
        end
    end
    
end

