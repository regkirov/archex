classdef Rectifier < Component & EPNComponentInterface
    %RECTIFIER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    
    methods
        % constructor
        function obj = Rectifier(name, cost, subtype, fprob)
            % calling superclass constructors
            obj@Component(name, cost, subtype);
            obj@EPNComponentInterface(fprob);
        end
        
        
        function res = print(obj)
            rows{1} = sprintf('Name: %s', obj.Name);
            rows{2} = sprintf('Type: %s', obj.Type);
            rows{3} = sprintf('Subtype: %s', obj.Subtype);
            rows{4} = sprintf('Cost: %d', obj.Cost);
            rows{5} = sprintf('Failure probability: %d', obj.FailureProbability);
            res = sprintf('%s\n', rows{:});
        end
    end
    
end

