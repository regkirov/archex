classdef EPNProblem < Problem
% EPNPROBLEM: a class for EPN (Electrical Power Network) architecture
% exploration problem. An EPN consists of a set of power sources
% (generators, batteries etc.), a set of sinks (electrical loads) and a set
% of intermediate components (AC/DC buses, rectifiers, transformers), which
% compose functional links between sinks and sources (deliver the power).
% Problem is formulated as a Mixed Integer Linear Program (MILP).
% 
% Library (implemented by the EPNLibrary class) consists of components of
% aforementioned types. They are labeled with costs, subtypes (e.g.,
% High/Low voltage), failure probabilities and power ratings/capacities.
%
% Problem statement:
% FIND 
%   system configuration and library mapping 
% SUBJECT TO 
%   interconnennection, power balance and reliability constraints
% SUCH THAT
%   overall system cost is minimized.
%
% Problem input (text files): 
% PROBLEM FILE with the following information:
%   - Functional flow (ordering of types of components starting from
%   source. For instance: Generator, AC Bus, Rectifier, DC Bus, Load;
%   - Tags. For EPN they represent spatial locations of components, for 
%   example Left and Right (a component can be located in the left or in 
%   the right part of the system/vehicle/aircraft/etc);
%   - Lists of variable and fixed components (number of instantiated
%   variable components in the solution is decided by the solver);
%   - Maximum number of components of each type;
%   - Cost of a connection (graph edge) between components. It is
%   assumed that all connections have the same cost;
%   - Failure probability of a connection (graph edge);
%   - Composition rules (which components can be connected);
%   - Problem constraints (specified using constraint patterns).
%
% LIBRARY FILE with the following information:
%   - Types of components in the library;
%   - A set of components of each type, labeled with cost, subtype, failure
%   probability and power rating/capacity.
%
% Problem output: 
%   - adjacency matrix;
%   - library mapping;
%   - reliability analysis.
%
% It is possible to vizualize the result as a graph, solve the problem
% with two different approaches (monolithic and iterative), save the 
% results to a file for future use.
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: December 2016
    
    properties (GetAccess = public, SetAccess = {?Helpers})        
        % data structures used to store the input of the problem
        edge_failure                
        
        % temporary for testing
        CostEdges
        IncidenceMatrix
        
        % misc
        solved  
%         scaling_factor
    end
    
    
    properties (Access = public)
        % debug
%         solver_in
%         solver_out
    end
    
    
    methods
        function obj = EPNProblem(name, lib_filename, input_filename, algorithm)
            obj@Problem(name, lib_filename, input_filename, algorithm);
            if nargin == 4
                str = sprintf('EPNProblem: Creating new problem instance (name = %s)', obj.Name);
                disp(str);                
                
                % load the library of components from the input file
                obj.Library = EPNLibrary(lib_filename);                                
                
                % load and process the problem input: description
                % properties (e.g. Flow, Tags) and template structure (max 
                % number of components of each type/tag, composition rules)
                obj.ProcessDescription;
                obj.ProcessTemplate;                
                
                % define data structures, which will act as input and
                % output of the optimization problem.
                obj.DefineDecisionVariables;
                
                % initialize the reliability requirement here, i.e., max
                % failure probability of each functional link between loads
                % and generators). Default value is 1 (always SAT)
                sink = obj.Flow{end};
                obj.SetRequirement(sink, '', 1);
                
                % read and process the constraints from the input file
                obj.ProcessConstraints;
                
                % create the cost function
                obj.DefineCostFunction;                                                                
                
                % initialize the analysis class (for EPN problem it is the
                % reliability analysis)
                obj.Analysis = ReliabilityAnalysis(obj);
            else
                error('EPNProblem: incorrect number of input parameters.');
            end
        end
        
        
        function ProcessDescription(obj)
            % call the same method from Problem to do common stuff
            obj.ProcessDescriptionGeneric();

            % in EPN problem edges (contactors) are also labeled with
            % failure probability
            pattern = '(?m)(?<=^(Edge failure:))[^;]*';
            res = char(regexp(obj.input.description, pattern, 'match'));
            obj.edge_failure = str2double(res);                        
        end
        
        
        function ProcessTemplate(obj)
            % call the same method from Problem to do common stuff
            obj.ProcessTemplateGeneric();
            
            % problem-specific code can be added here
            %
        end
        
        
        function ProcessConstraints(obj)            
            % call the same method from Problem to do common stuff
            obj.ProcessConstraintsGeneric();
            
            % ConstraintHandler class is responsible for parsing the input
            % file for constraints, classifying them and getting their MILP
            % formulations using patterns
            if isempty(obj.ConstraintHandler)
                obj.ConstraintHandler = ConstraintHandler(obj, obj.input.constraints);
            end
            
            % get general (e.g. max number of components), interconnection
            % and balance (e.g. "has_sufficient_power") MILP constraints;
            % they will be used both in iterative and monolithic problems
            milp_constraints = obj.ConstraintHandler.getConstraintsMILP(obj, ...
                obj.ConstraintHandler.General,...
                obj.ConstraintHandler.Interconnection,...
                obj.ConstraintHandler.Balance);
            
            obj.addConstraint(milp_constraints); 
            
            if strcmp(obj.Algorithm, 'eager')
                % reliability constraints (as well as helper "path" variables
                % and constraints) are auxiliary, they are added to formulation
                % to get a monolithic problem ("eager" approach)
                disp('EPNProblem: extra variables required for the "eager" algorithm. Creating...');
                aux_milp_constraints = obj.ConstraintHandler.getConstraintsMILP(obj, ...
                    obj.ConstraintHandler.Reliability);
                obj.addAuxConstraint(aux_milp_constraints);
            end                                                                                
        end                        
        
        
        function DefineDecisionVariables(obj)
            disp('EPNProblem: Creating decision variables and data structures...');
            
            % create the adjacency matrix
            obj.AdjacencyMatrix = AdjacencyMatrix(obj.NumComponents, ...
                obj.ConnectionRules);
            
            % add adjacency matrix constraints to ensure that all 
            % connections not mentioned in composition rules are forbidden
            adjm_constraints = obj.AdjacencyMatrix.getConstraints(0, obj.Flow);
            %% without these constraints and variables results are the same, but with much less variables
            obj.addConstraint(adjm_constraints);
            
            % generate the ordered list of matrix keys (starting from
            % source and ending with sink following the functional flow)
            ordered_keys = {};
            for i = obj.Flow
                for j = obj.Tags
                    new_key = [char(i), ' ', char(j)];
                    matching_keys = obj.AdjacencyMatrix.getMatchingKeys(new_key, new_key);
                    if isempty(matching_keys)
                        continue;
                    end
                    new_key_index = length(ordered_keys) + 1;
                    ordered_keys{new_key_index} = new_key; 
                end
            end

            % save the key order to the matrix object (it is required to
            % keep intervals of matrix indices related to different
            % component types up-to-date)
            obj.AdjacencyMatrix.setOrdering(ordered_keys);            
            
            %% test of incidence matrix
            %obj.IncidenceMatrix = IncidenceMatrix(obj.NumComponents, obj.ConnectionRules, ordered_keys);
            
            % initialize the LibraryMapping variables and add corresponding
            % mapping constraints
            [obj.LibraryMapping, mapping_constraints] = ...
                Helpers.createLibraryMapping(obj); 
            obj.addConstraint(mapping_constraints);
        end        
        
        
        function DefineCostFunction(obj)
            % Compose a cost function from existing decision variables. In
            % the current version the cost function has only the total cost
            % (price) of the system as an objective. Therefore, the function 
            % is defined as the sum of costs of components, that are
            % instantiated (used) in the problem solution.
            disp('EPNProblem: Defining the cost function...');
            for component = obj.components_variable                
                component = char(component);
                costs = obj.Library.Costs(component);
                
                % get the mapping matrix for current component type. The
                % matrix is used to determine how many "real" components
                % (from the library) are used in the solution. This is then
                % multiplied by the vector of costs.
                current_component_mapping = obj.LibraryMapping(component);
                current_component_cost = sum(costs.*sum(current_component_mapping,2)');
                
                % update the total cost function
                obj.CostFunction = obj.CostFunction + current_component_cost;
            end
            
            % include the costs of fixed components (e.g., Loads) in the
            % cost function. In the current version it is not used.
%             for component = obj.components_fixed
%                 component = char(component);
%                 costs = obj.Library.Costs(component);
%             end

            obj.CostEdges = 0;
            % edges (switches) cost
            for i = 1:length(obj.Flow)-1                
                connectivity_matrix = ...
                    obj.AdjacencyMatrix.getSubMatrixNumeric(obj.Flow{i}, obj.Flow{i+1});
                num_edges = sum(sum(connectivity_matrix));
                obj.CostFunction = ...
                    obj.CostFunction + obj.edge_cost * num_edges;
                obj.CostEdges = obj.CostEdges + obj.edge_cost * num_edges;
                
                % if horizontal connections allowed
                if Helpers.canBeConnected(obj, obj.Flow{i}, obj.Flow{i})
                    connectivity_matrix = ...
                    obj.AdjacencyMatrix.getSubMatrixNumeric(obj.Flow{i}, obj.Flow{i});
                    % we assume horizontal connections are bidirectional
                    % and divide by 2 not to count them twice
                    num_extra_edges = sum(sum(connectivity_matrix)) / 2;
                    obj.CostFunction = ...
                        obj.CostFunction + obj.edge_cost * num_extra_edges;
                    obj.CostEdges = obj.CostEdges + obj.edge_cost * num_extra_edges;
                end
            end
        end
        
        
        function SetRequirement(obj, varargin)
            % set the reliability requirement for the EPN problem
            % varargin{1} - T   (component type)
            % varargin{2} - S   (component subtype)
            % varargin{3} - val (requirement numerical value)
            %
            % CASES:
            % (1) If only T is specified and T belongs to F (functional 
            % flow), then set the value val as requirement for every 
            % component of type T.
            % (2) If T and S are specified, then set requirement only for
            % components of T having subtype S.
            % (3) If T does not belong to F and S is empty, then set the 
            % value val only for component with a particular name (T)
            T   = varargin{1};
            S   = varargin{2};
            val = varargin{3};
            
            % requirement can be set for any component in the flow F
            if any(strcmp(obj.Flow, T))
                if isempty(S)
                    % case (1)
                    sinks = obj.AdjacencyMatrix.getSubMatrixNumeric(T,T);
                    obj.Requirement.Reliability(1:length(sinks)) = val;
                else
                    % case (2)
                    S_lib = obj.Library.Subtypes(T);
                    S_indices = find(S_lib == 1);
                    for i = S_indices
                        obj.Requirement.Reliability(i) = val;                        
                    end
                end
            else
                % case (3)
                % NOTE: only Loads (obj.Flow{end}) currently supported!!
                names = obj.Library.Names(obj.Flow{end});
                T_match = strfind(names, T);
                T_index = find(not(cellfun('isempty', T_match)));
                obj.Requirement.Reliability(T_index) = val;
            end
        end
        
        
        function Solve(obj, options)
            % solve the EPN problem using a selected algorithm
            if nargin == 1
                options = [];
            end
            
            if strcmp(obj.Algorithm, 'eager') == 1
                % monolithic
                disp('EPNProblem: monolithic ("eager") algorithm selected.');
                obj.Solution = eagerAlgorithm(obj, options);                
            elseif strcmp(obj.Algorithm, 'lazy') == 1
                % iterative
                disp('EPNProblem: iterative ("lazy") algorithm selected.');
                obj.Solution = lazyAlgorithm(obj, obj.Requirement.Reliability, options);
            else
                str = sprintf('EPNProblem: the specified algorithm (%s) is not implemented', obj.Algorithm);
                disp(str);
            end            
            
            obj.solved = 1;
            str = sprintf('Elapsed time: %0.2fs', obj.Solution.time_elapsed);
            disp(str);
            fprintf('============================================================\n');
        end
        
        
        function graph = ShowResult(obj)
            % prepare the results, draw the resulting graph and calculate
            % reliabilities at each sink using the exact analysis.
            disp('EPNProblem: Drawing the graph...');
            
            % descriptions (information about mapping of each component).
            % In the graph it will appear by double clicking on a node
            %% ToDo: this is also generic?
            num_components = obj.AdjacencyMatrix.Size;
            descriptions = cell(1, num_components);
            count = 1;
            for i = 1:length(obj.Flow)
                current_mapping = value(obj.LibraryMapping(obj.Flow{i}));
                current_mapping = round(current_mapping);
                current_real_components = ...
                    obj.Library.getComponentsByType(obj.Flow{i});
                
                for j = 1:size(current_mapping, 2)
                    if sum(current_mapping(:,j)) == 0
                        % virtual component is not instantiated
                        current_description = 'NOT USED';
                        descriptions{count} = current_description;
                    else
                        library_idx = find(current_mapping(:,j),1);
                        real_component = current_real_components(library_idx);
                        descriptions{count} = real_component.print;
                    end
                    count = count + 1;
                end
            end
            
            % draw the graph
            graph = plotAdjacencyMatrix(obj, descriptions, 'final');
            
            % color graph nodes according to subtypes
            if ~isempty(obj.restrictions)
                updateGraphColoring(graph);
                pause(1);
            end
            
            % store most recent node descriptions for future use
            obj.node_descriptions = descriptions;            
        end                                 
        
        
        function Save(obj)
            % save the solved problem data into a matlab file for future
            % reference. At the moment adjacency matrix, problem input data
            % and results of the reliability analysis are saved.
            %% ToDo: save aux structures, approximate algebra values?
            timestamp = datetime('now');
            timestamp.Format = 'yyyy-MM-dd HH:mm:ss';
            timestamp = char(timestamp);
            timestamp = strrep(timestamp, '-', '_');
            timestamp = strrep(timestamp, ' ', '_');
            timestamp = strrep(timestamp, ':', '_');
            filename = sprintf('Output/EPN_problem_%s.mat', timestamp);

            fprintf('\nEPNProblem: Saving the results to a .mat file...\n');
            
            % saving of sdpvar objects is not possible, therefore, to save
            % the adjacency matrix object we create its copy, in which all
            % sdpvar-s are replaced with their current numerical values.
            % This does not affect the values in the original matrix.
            adjacency_matrix = copy(obj.AdjacencyMatrix);
            adjacency_matrix.getVersionForSaving;

            reliability_analysis = obj.Analysis.last_result;
            requirement          = obj.Requirement.Reliability;
            cost                 = value(obj.CostFunction);
            library              = obj.Library;
            node_descriptions    = obj.node_descriptions;
            
            problem_data.Library             = obj.Library;            
            problem_data.NumComponents       = obj.NumComponents;
            problem_data.Tags                = obj.Tags;
            problem_data.ConnectionRules     = obj.ConnectionRules;
            problem_data.Flow                = obj.Flow;
            problem_data.components_variable = obj.components_variable;
            problem_data.components_fixed    = obj.components_fixed;
            problem_data.edge_cost           = obj.edge_cost;
            problem_data.edge_failure        = obj.edge_failure;
            
            time_elapsed = obj.Solution.time_elapsed;
            
            % save library mappings
            mapping = containers.Map;
            for i = 1:length(obj.Flow)
                mapping(obj.Flow{i}) = ...
                    round(value(obj.LibraryMapping(obj.Flow{i})));
            end
            
            %warning off
            if strcmp(obj.Algorithm, 'eager') == 1                
                % saving also approximate reliabilities for AR
                sink = obj.Flow{end};                
                %approximate_reliabilities = value(obj.estimated_reliabilities(sink)) * max(obj.Requirement);
                approximate_reliabilities = value(obj.estimated_reliabilities(sink));
                %approximate_reliabilities = value(obj.estimated_reliabilities(sink)) .^ (1/obj.scaling_factor); % descale
                save(filename, 'adjacency_matrix', 'reliability_analysis', 'cost',... 
                    'problem_data', 'approximate_reliabilities', 'requirement',...
                    'mapping', 'time_elapsed', 'library', 'node_descriptions');
            else            
                save(filename, 'adjacency_matrix', 'reliability_analysis', 'cost', ...
                    'problem_data', 'requirement', 'mapping', 'time_elapsed',...
                    'library', 'node_descriptions');
            end
            %warning on
            
            % saving the resulting graph to a png file
            if sum(reliability_analysis) ~= 0
                disp('EPNProblem: Saving the architecture graph to a png file...');
                filename = sprintf('Output/graphs/EPN_problem_%s.png', timestamp);
                graph = obj.ShowResult;
                graph.fig.Visible = 'off';
                saveas(graph.fig, filename);
            end
            
            % saving cplex input to a txt file
            disp('EPNProblem: Saving CPLEX input to a txt file...');
            filename = sprintf('Output/cplex/EPN_problem_%s.txt', timestamp);
            solver_in = obj.Solution.solver_data.solverinput.model;
            saveCplexInput(solver_in, filename);
            
            disp('EPNProblem: Done saving.');
        end
        
        
        function setAlgorithm(obj, new_algorithm)
            % version of generic setAlgorithm() for EPN problem
            if strcmp(new_algorithm, 'eager') && isempty(obj.AuxiliaryConstraints)                
                % reliability constraints (as well as helper "path" variables
                % and constraints) are auxiliary, they are added to formulation
                % to get a monolithic problem ("eager" approach)
                disp('EPNProblem: extra variables required for the "eager" algorithm. Creating...');
                aux_milp_constraints = obj.ConstraintHandler.getConstraintsMILP(obj, ...
                    obj.ConstraintHandler.Reliability);
                obj.addAuxConstraint(aux_milp_constraints);
            end
            obj.Algorithm = new_algorithm;
        end
        
        
        function [ new_constraints ] = LearnCons(obj, result)
            % LearnCons is a part of the iterative algorithm. For the EPN
            % problem it takes the result of the reliability analysis as
            % an input and using the current solution heuristically adds 
            % extra constraints to guide the solver to a better solution.
            new_constraints = obj.learning_heuristic_fast(result);
        end                      
    end    
    
    
    methods (Access = private)                                              
        function [ new_constraints ] = learning_heuristic_fast(obj, result)
            %% Actual implementation of learning heuristics for EPN
            % It is based on estimating the number of extra source-sink 
            % paths for every functional link, which are required to
            % satisfy the reliability requirement. To add these paths the
            % function tries to perform several actions, i.e., connecting
            % existing components together and adding new components, if
            % possible. These actions result in creating extra MILP
            % constraints, adding them to existing formulation and then
            % re-running the solver.
            disp('========================== LEARNCONS ==========================');            
            [adjm, indices] = obj.AdjacencyMatrix.getMatrixNumeric;
            adjm = round(value(adjm));
            sink = obj.Flow{end};
            num_new_constraints = 0;
            new_constraints = [];
            
            % estimate max_hops (number of adj matrix multiplications)
            max_hops = 0;
            for i = 1:length(obj.Flow)-1
                current = obj.Flow{i};
                if Helpers.canBeConnected(obj, current, current)
                    % severak hops on the same level are possible
                    submatrix = obj.AdjacencyMatrix.getSubMatrixNumeric(current, sink);
                    num_current_components = size(submatrix, 1);
                    max_hops = max_hops + num_current_components;
                else
                    % only 1 current component is allowed along the path
                    max_hops = max_hops + 1;
                end
            end
            
            % do adjacency matrix multiplications (up to maximum number of 
            % hops) to compute the walk indicator matrix
            %% ToDo: turn into a generic helper function?
            walk_indicator_matrix = zeros(size(adjm,1));
            for i = 1:max_hops
                product = adjm;
                for j = 1:i-1
                    % logical AND (multiplication)
                    product = product * adjm;
                    product = product > 0;
                end
                % logical OR (addition)
                walk_indicator_matrix = walk_indicator_matrix + product;
                % keep the values binary
                walk_indicator_matrix = walk_indicator_matrix > 0;              
            end
            
            % get degrees of redundancy from the walk indicator matrix
            degrees_list = containers.Map;
            for i = 1:length(obj.Flow)-1
                current = obj.Flow{i};
                current_indices_row = indices(current);
                current_indices_col = indices(sink);
                current_all_paths_matrix = ...
                    walk_indicator_matrix(current_indices_row(1):current_indices_row(2), ...
                        current_indices_col(1):current_indices_col(2));
                current_degree_of_redundancy = sum(current_all_paths_matrix,1);
                degrees_list(current) = current_degree_of_redundancy;
            end
            
            % estimate the worst failure probability of a functional link
            prob_path_failure = 0;
            prob_edge_failure = obj.edge_failure;
            for i = 1:length(obj.Flow)-1
                current = obj.Flow{i};
                fprobs = obj.Library.FailureProbabilities(current);
                % take the worst fprob (we estimate the worst case)
                prob_component_failure = max(fprobs);
                prob_path_failure = prob_path_failure + prob_component_failure + prob_edge_failure;                               
            end
            
            % find functional links with worst reliability
            diff = abs((obj.Requirement.Reliability - result) ./ obj.Requirement.Reliability);
            worst_unsat_sink_idx = find(diff == max(diff));
            
            % iterate over all components starting from closest to sink
            for i = length(obj.Flow)-1:-1:1                
                current = obj.Flow{i};
                % get current degree of redundancy
                red_degree = degrees_list(current);                                                
                
                if i == length(obj.Flow)-1
                    str = sprintf('CURRENT LEVEL: %s.', current);
                else
                    str = sprintf('\nCURRENT LEVEL: %s.', current);
                end
                disp(str);
                
                % get current number of instantiated components
                current_mapping = obj.LibraryMapping(current);
                current_mapping_values = round(value(current_mapping));
                current_num_components = sum(sum(current_mapping_values));
                max_components = size(current_mapping,2);
                paths_still_required = zeros(1,length(result));
                
                % freeze the mapping (mapping decided on previous iteration
                % cannot be changed on future ones);
                % Otherwise next results might get even worse than current,
                % more iterations will be needed to converge, final
                % architecture might be over-designed)
                for i = 1:size(current_mapping,2)
                    if sum(current_mapping_values(:,i),1) >= 1
                        new_constraints = [new_constraints, ...
                            current_mapping(:,i) == current_mapping_values(:,i)];
                    end
                end
                
                % analyze each sink separately (they might have different
                % reliability requirements; hence, different actions
                % might need to be taken)
                for j = 1:length(result)                                        
                    % first estimate required number of extra paths (k)
                    k = max(floor(log(obj.Requirement.Reliability(j)/result(j))/log(prob_path_failure)));
                    
                    if result(j) <= obj.Requirement.Reliability(j)
                        % already SAT, no actions needed
                        continue;
                    end
                    
                    % set k = 1 to do the fine tuning part in the same
                    % for-loop (otherwise another very similar loop
                    % will be required)
                    if k == 0                        
                        k = 1;
                    end
                    
                    paths_still_required(j) = k;
                    
                    if Helpers.canBeConnected(obj, current, current)
                        % if horizontal connections are allowed
                        current_current_matrix = ...
                            obj.AdjacencyMatrix.getSubMatrixNumeric(current, current);
                        current_current_matrix_values = round(value(current_current_matrix));
                                                                       
                        current_indices_row = indices(current);
                        current_indices_col = indices(sink);
                        current_all_paths_matrix = ...
                            walk_indicator_matrix(current_indices_row(1):current_indices_row(2), ...
                                current_indices_col(1):current_indices_col(2));
                                                
                        % find index of component of the current level
                        % connected to sink j 
                        current_sink_conn_idx = ...
                            find(current_all_paths_matrix(:,j) == 1);
                        if isempty(current_sink_conn_idx)
                            paths_still_required(j) = 0;
                            continue;
                        end
                        % if there are several, take the first one
                        current_sink_conn_idx = current_sink_conn_idx(1);
                        
                        % add horizontal connections to existing components
                        for l = 1:size(current_current_matrix,1)
                            curr_idx_global = current_sink_conn_idx + current_indices_row(1) - 1;
                            l_global = l + current_indices_row(1) - 1;
                            if sum(current_mapping_values(:,l)) == 1 ...
                                    && current_sink_conn_idx ~= l ...
                                    && current_current_matrix_values(current_sink_conn_idx, l) ~= 1 ...
                                    && current_num_components ~= red_degree(j)...
                                    && Helpers.canBeConnectedBySubtype(obj, curr_idx_global, l_global)
                                % if component l is instantiated 
                                % AND it is not current
                                % AND it is not yet connected to current                                
                                % AND red_degree is less than num_component
                                % AND subtypes of current and l are compatible
                                % THEN add a horizontal connection
                                new_constraints = [new_constraints, ...
                                    current_current_matrix(current_sink_conn_idx,l) >= 1];
                                num_new_constraints = num_new_constraints + 1;
                                % adding a connection implies adding a path.
                                % we now need one path less.
                                paths_still_required(j) = paths_still_required(j) - 1;
                                
                                if paths_still_required(j) == 0
                                    % already OK, no more connections need
                                    break;
                                end                            
                            end
                        end                        
                        
                        if num_new_constraints > 0
                            str = sprintf('%s %d: Added %d horizontal connection(s) to existing components', ...
                                sink, j, k - paths_still_required(j));
                            disp(str);
                        end                                                
                        
                        % if more paths are needed then add a constraint to
                        % horizontally connect current component to k other
                        % components(some of them are not yet instantiated)
                        %% We currently force connections to new components only for DC buses (closest to sink)
                        if (current_num_components < max_components) && ...
                                (strcmp(current, obj.Flow{end-1}) == 1) %...
                                % && (any(current_all_paths_matrix(current_sink_conn_idx, worst_unsat_sink_idx)))
                            max_connections = max_components - 1;
                            current_connections = sum(current_current_matrix_values(current_sink_conn_idx,:));
                            extra_paths = min(k, max_connections - current_connections);
                            if extra_paths > num_new_constraints
                                % connecting to more components than
                                % currently instantiated will force the
                                % optimizer to add new ones
                                new_constraints = [new_constraints, ...
                                    sum(current_current_matrix(current_sink_conn_idx,:)) >= ...
                                        sum(current_current_matrix_values(current_sink_conn_idx,:)) + extra_paths];
                                if num_new_constraints == 0
                                    str = sprintf('%s %d: Connecting existing components is not currently possible. Added %d extra connections (extra components are required)',...
                                        sink, j, extra_paths);
                                    disp(str);
                                else
                                    str = sprintf('%s %d: Connecting existing components is not enough. Added %d extra connections (extra components are required)',...
                                        sink, j, extra_paths - num_new_constraints);
                                    disp(str);
                                end
                                num_new_constraints = num_new_constraints + 1;
                            end
                        end
                    else
                        % no horizontal connections allowed
                        paths_still_required(j) = red_degree(j);
                        %paths_still_required(j) = k + red_degree(j);
                    end                    
                    num_new_constraints = 0;
                end
                
                % if more paths are needed (could not get the proper degree
                % of redundancy only by connecting existing components
                % together) then add more components.
                % NOTE: adding new components is already forced with 
                % constraints specified above, here we enforce at least one
                % new component to be of a particular subtype if required.
                if Helpers.canBeConnected(obj, current, current) ...
                        && any(strcmp(keys(obj.restrictions), current))
                    
                    extra_components = max(paths_still_required);                                          
                    for i = worst_unsat_sink_idx
                        
                        % we currently specify desired subtypes for new
                        % components only for those closest to sink (e.g. 
                        % DC buses). All other levels are expected to work
                        % correctly based on already existing constraints.
                        if (extra_components == 0) || (strcmp(current, obj.Flow{end-1}) ~= 1)
                            break;
                        end

                        % look for a component subtype that we need to add
                        % (subtype of the sink, which has the worst unsat
                        % result)
                        worst_current_idx = ...
                            find(current_all_paths_matrix(:,i) == 1);
                        worst_current_idx = worst_current_idx(1);
                        subtypes_for_current_type = obj.Library.Subtypes(current);
                        mapping = current_mapping_values(:, worst_current_idx);
                        for key = keys(subtypes_for_current_type)
                            current_subtype = subtypes_for_current_type(char(key));
                            if find(mapping == 1) == find(current_subtype == 1)
                                worst_unsat_subtype = char(key);
                                break;
                            end
                        end
                        
                        % components currently mapped to this worst subtype
                        worst_unsat_subtype_current_mapping = ...
                            current_subtype * current_mapping;
                        worst_unsat_subtype_current_mapping_values = ...
                            round(value(worst_unsat_subtype_current_mapping));

                        % add a constraint
                        new_constraints = [new_constraints, ...
                            sum(worst_unsat_subtype_current_mapping) >=...
                            sum(worst_unsat_subtype_current_mapping_values) + 1];
                        str = sprintf('Added 1 new component with a subtype "%s"', ...
                            worst_unsat_subtype);
                        disp(str);
                        extra_components = extra_components - 1;
                    end

                    extra_components = max(paths_still_required);
                    str = sprintf('Added %d new component(s) in total', ...
                            extra_components);
                    disp(str);
                else                    
                    % if horizontal connections are NOT allowed
                    if max(paths_still_required) <= max_components
                        if (max(paths_still_required) - current_num_components) > 0
                            % new constraint not needed here, should be
                            % forced by already existing constraints
%                             new_constraints = [new_constraints,...
%                                 sum(sum(current_mapping)) >= max(paths_still_required)];
                            str = sprintf('Horizontal connections not allowed. Will add %d new component(s)', ...
                                max(paths_still_required) - current_num_components);
                            disp(str);
                        else
                            str = sprintf('No need to add extra components');
                            disp(str);
                        end
                    else
                        str = sprintf('Cannot add extra components (maximum amount reached)');
                        disp(str);
                    end
                end
                
                % last attempt to tune: if new horizontal connections on
                % previously checked levels will no more increase the
                % redundancy AND adding new components on any level does
                % not help as well (we already have max components) THEN
                % try to add several more horizontal connections on current
                % level
                %
                % EXAMPLE: already achieved maximum components and degree
                % of redundancy for DC Buses, still might improve by
                % connecting some AC buses together
                if Helpers.canBeConnected(obj, current, current) ...
                       && max(sum(current_current_matrix_values)) ~= max_components - 1 ...
                       && length(new_constraints) == 0
                           new_constraints = [new_constraints, ...
                               sum(sum(current_current_matrix)) >= ...
                               sum(sum(current_current_matrix_values)) + 1];                           
                           str = sprintf('Fine tuning: Added 1 horizontal connection');
                           disp(str);
                end                
            end
            disp('========================== LEARNCONS END ==========================');
        end                        
        
        
        function [min_red_type, min_indices] = findMinRedType(obj)
            % find a component type (and its indices in the adjacency
            % matrix) with minimal degree of redundancy
            sink = obj.Flow{end};
            min_red_type = obj.Flow{1};
            min_degree_of_redundancy = 1000; % !!hard coded
            for i = 1:length(obj.Flow)-1
                current = obj.Flow{i};
                curr_all_paths = obj.allPathsLayer(current, sink);
                curr_all_paths = curr_all_paths.Matrix;
                curr_all_paths = curr_all_paths(:,:,1);
                curr_all_paths_values = round(value(curr_all_paths));
                curr_all_paths_values = sum(curr_all_paths_values, 1);
                curr_min_degree_of_redundancy = min(curr_all_paths_values);
                if curr_min_degree_of_redundancy <= min_degree_of_redundancy
                    min_degree_of_redundancy = curr_min_degree_of_redundancy;
                    min_indices = find(curr_all_paths_values == min_degree_of_redundancy);
                    min_red_type = obj.Flow{i};
                end                
            end
        end  
        % private methods end
    end
    
    
    methods (Access = public)
        function res = getFailureProbabilitiesAsVector(obj)
            % get an ordered vector of failure probabilities (number of
            % values = number of components). This vector will be used for
            % exact reliability analysis.
            res = [];
            if obj.solved == 5  % not supported yet                
%                 % get failure probabilities for each component from mapping
%                 % matrices (each virtual component can now be associated
%                 % with real from the library)
%                 for i = 1:length(obj.Flow)
%                     current_mapping = value(obj.LibraryMapping(obj.Flow{i}));
%                     current_mapping = round(current_mapping);
%                     current_real_components = ...
%                     obj.Library.getComponentsByType(obj.Flow{i});
%                 
%                     for j = 1:size(current_mapping,2)
%                         library_idx = find(current_mapping(:,j), 1);
%                         if ~isempty(library_idx)
%                             % if the component is mapped
%                             real_component = current_real_components(library_idx);
%                             fprob = real_component.FailureProbability;
%                         else
%                             fprob = 0;
%                         end
%                         res = [res fprob];
%                     end
%                 end
            else
                % if problem is not yet solved (variables are not decided
                % yet) then it is not yet possible to get failure
                % probabilities from mappings. Instead for each component
                % type we use the first value of failure probability for a
                % corresponding type in the library.
                % This means we currently assume same failure probability
                % for all components of particular type.
                for i = 1:length(obj.Flow)
                    num_of_current_components = size(obj.LibraryMapping(obj.Flow{i}), 2);
                    fprobs_current_type = obj.Library.FailureProbabilities(obj.Flow{i});
                    fprob = fprobs_current_type(1);
                    res = [res fprob * ones(1, num_of_current_components)];
                end
            end
        end
    end
end

%
%% Some references
%
%  - D. Kirov et al. "ArchEx: An Extensible Framework for the Exploration
%    of Cyber-Physical System Architectures, Proc. DAC, Jun. 2017.
%  - P. Nuzzo, et al. "A contract-based methodology for aircraft electric 
%    power system design," IEEE Access, vol. 2, pp. 1-25, 2014.
%  - N. Bajaj, et al. "Optimized Selection of Reliable and Cost-Effective
%    Cyber-Physical System Architectures," Proc. DATE, Mar. 2015.