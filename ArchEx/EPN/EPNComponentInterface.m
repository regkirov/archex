classdef (HandleCompatible) EPNComponentInterface
% An interface which adds an extra label to all components of the EPN
% library - failure probability. This label is not a default one provided
% with a generic Library class.

% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: December 2016

    properties
        FailureProbability         % component reliability
    end
    
    
    methods
        % interface constructor
        function obj = EPNComponentInterface(fprob)
            if nargin > 0
                obj.FailureProbability = fprob;
            else
                obj.FailureProbability = 0;
            end
        end
    end
    
end

