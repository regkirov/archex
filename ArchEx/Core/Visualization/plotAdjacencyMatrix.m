function graph = plotAdjacencyMatrix( prob, descriptions, mode )
% PLOTADJACENCYMATRIX function draws a graph for provided adjacency matrix
% using GraphViz4Matlab. Node descriptions are provided as second argument.
% They can be seen by double-clicking on a node of the graph. In ArchEx
% information about library mapping of a component is shown in the
% description.

% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: March 2017

% the adjacency matrix here might be a set of decision variables, therefore, 
% "value" is used
adjacency_matrix = prob.AdjacencyMatrix;
adjm_numeric = value(adjacency_matrix.getMatrixNumeric);
adjm_numeric = round(adjm_numeric);
total_components = size(adjm_numeric, 1);

ordered_keys = adjacency_matrix.Ordering;
node_labels = cell(1, total_components);
prev_key = ordered_keys{1};
level_indices = 1;
node_count = 0;

% generate the ordered list of node (component) labels
for key = ordered_keys
    if ~ismember(key, adjacency_matrix.Row_keys)
        continue
    end
    
    key_parts = strsplit(char(key));
    key_part1 = key_parts{1};
    key_part2 = key_parts{end};
    prev_key_parts = strsplit(prev_key);
    prev_key_part1 = prev_key_parts{1};
    prev_key_part2 = prev_key_parts{end};
    
    if strcmp(prev_key_part1, key_part1) == 0
        level_indices = [level_indices, node_count+1];
    end
    
    submatrix = adjacency_matrix.getSubMatrixNumeric(char(key), char(key));
    num_components = size(submatrix, 1);

    for i = 1:num_components
        %% need to make node naming problem-specific somehow
        node_count = node_count + 1;
        if isa(prob, 'RPLProblem')
            if strcmp(key_part1, 'Source')
                new_label = sprintf('Src%s', key_part2(1));
            elseif strcmp(key_part1, 'Sink')
                new_label = sprintf('Snk%s', key_part2(1));
            else
                new_label = [key_part1(1), key_part1(end), key_part2(1), num2str(i)];
            end
        elseif isa(prob, 'WirelessProblem')
            if strcmp(key_part1, 'Sink')
                new_label = sprintf('Snk%s', num2str(i));
            else
                new_label = [key_part1(1:3), num2str(i)];
            end
        else     
            % EPN
            new_label = [key_part2(1), key_part1(1), num2str(i)];
        end
        if strcmp(mode, 'template')
            % add node number for template mode
            new_label = sprintf('%s\n%d', new_label, node_count);
        end
        
        node_labels{node_count} = new_label;
    end
        
    prev_key = char(key);
end

colors_list = {'g', 'c', 'r', 'y', 'b', 'm', 'w'};
node_colors = cell(1, total_components);
color_idx = 1;
% generate the list of node colors (according to component types)
for i = 1:node_count
    if ismember(i, level_indices(2:end))
        color_idx = color_idx + 1;
        if color_idx > length(colors_list)  % ToDo: test more
            color_idx = 1;
        end
    end
    
    node_colors{i} = colors_list{color_idx};
end

% test descriptions
% descr = cell(1,node_count);
% for i = 1:node_count
%     descr{i} = ['Bla ', num2str(i)];
% end

% draw the graph
if isa(prob, 'WirelessProblem')
    layout  = NetworkLayout('NetworkLayout', prob.candidate_locations, prob.Map);
    layout2 = LayeredLayout('LayeredLayout', level_indices);
else
    layout  = LayeredLayout('LayeredLayout', level_indices);
    layout2 = [];
end

if strcmp(mode, 'template')
    % calculate the adjacency matrix of the template from incidence matrix.
    % The resulting graph will show all possible connections (edges) of
    % the reconfigurable graph.
    I = prob.IncidenceMatrix.getMatrix;
    id = eye(size(I,1));
    adjm_template = I * I' - 2 * id;
    adjm_template = adjm_template ~= 0; % make binary
    
    % remove self loops (set diagonal entries to zero)
    for i = 1:size(adjm_template,1)
        adjm_template(i,i) = 0;
        %descriptions{end+1} = text;
    end
    
    % plot
    graph=graphViz4Matlab('-adjMat', adjm_template, '-layout', layout, ...
                          '-nodeLabels', node_labels, '-nodeColors', node_colors, ...
                          '-nodeDescriptions', descriptions);
else
    % normal plotting (show graph of the decided configuration)
    graph=graphViz4Matlab('-adjMat', adjm_numeric, '-layout', layout, ...
                          '-nodeLabels', node_labels, '-nodeColors', node_colors, ...
                          '-nodeDescriptions', descriptions);
end

if ~isempty(layout2)
    graph.addLayout(layout2);
end

end