function updateGraphColoring( graph, subtypes_list, colors_list )
% This function updates the coloring of the graph. It takes as input the
% graph handle, list of component kinds and list of colors (cells). It loops over
% the nodes of the graph, checks the field "Kind" in their descriptions and
% colors each node with a particular kind from kinds_list with a particular
% color from colors_list (there is a default colors list in case it is not
% provided by the user).
%
% Current usage of this function is limited with coloring graphs for the
% EPS problem (e.g. color High/Low voltage component differently for better
% visibility/distinction), but in general it can be applied to resulting
% graphs of other problems solved by ArchEx (in the future versions)
%
% Author: Dmitrii Kirov
% Last modified: October 2016

%default_colors = {[0,1,0], [0.75,0.75,0], [1,0,0]};
default_colors = {[0,1,0], [1,1,0], [1,0,0]};
default_subtypes = {'High voltage', 'Low voltage', 'TRU'};

if nargin == 3
    colors = colors_list;
    subtypes = subtypes_list;
elseif nargin == 2
    subtypes = subtypes_list;
    colors = default_colors;
else
    subtypes = default_subtypes;
    colors = default_colors;
end
pattern = '(?m)(?<=^Subtype: )[A-Za-z0-9 ]*\n';

nodes = graph.nodeArray;

% iterate through all graph nodes
for node = nodes % "not used" nodes will have white color
    descr = node.description;
    if strcmp(descr, 'NOT USED') == 1
        node.shadedColor = 'None';
        node.textColor = 'None';
        node.lineStyle = '--';
        node.fontSize = 10;
        node.lineColor = 'None';
    end
    
    % get the node (component) kind from the description
    subtype = regexp(descr, pattern, 'match');
    subtype = strtrim(subtype);
    
    for i = 1:length(subtypes)
        if strcmp(subtype, subtypes{i}) == 1
            % update the node color if its kind is present in kinds_list
            % (those which are not present will stay unchanged)
            node.shadedColor = colors{i};
            break;
        end
    end
end

graph.redraw;

% % hide nodes that are not used (this should be done after redrawing)
% for node = nodes
%     descr = node.description;
%     if strcmp(descr, 'NOT USED') == 1
%         node.erase;
%     end
% end

end

