classdef (Abstract) Component < handle
% This class is a representation of a generic component for a variety
% of design exploration problems. It has fields, which are common for
% different problem, namely:
%
% - Name of the component
% - Type of the component (e.g. "Source"; used to classify components
% within the library)
% - Cost (often used in optimization as a part of the cost function)
% - Subtype (additional field for sub-classifying components. For
% instance, a component of a type "Source" can be a wireless sensor
% network transmitter, a WiFi access point, an ultrasound transmitter
% and so on)
%
% One must inherit from this abstract class when creating concrete
% component classes for a particular problem.
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: May 2016
    
    properties (SetAccess = protected, GetAccess = public)
        Name
        Type
        Cost
        Subtype
    end
    
    methods (Abstract)
        print(obj)
    end
    
    methods
        function obj = Component(name, cost, subtype)
            if nargin == 0
                obj.Name = 'Noname component';
                obj.Cost = 0;
                obj.Subtype = 0;
            elseif nargin == 1
                obj.Name = name;
                obj.Cost = 0;
                obj.Subtype = 0;
            elseif nargin == 2
                obj.Name = name;
                obj.Cost = cost;
                obj.Subtype = 0;
            elseif nargin == 3
                obj.Name = name;
                obj.Cost = cost;
                obj.Subtype = subtype;
            end
            obj.Type = class(obj);
        end
        
    end
    
end

