classdef TimingAnalysis < handle
    %TIMINGANALYSIS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = private)
    end
    
    methods (Access = public)
        function obj = TimingAnalysis(prob)
            % constructor
            disp(sprintf('%s: Timing analysis created', class(prob)));
        end
        
        function res = Run(obj)
            %disp('Timing analysis: not implemented yet');
            res = -1;
        end
    end
    
end

