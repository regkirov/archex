classdef ReliabilityAnalysis < handle
% Implementation of the reliability analysis class. Using the data from the
% adjacency matrix the class is able to calculate the exact reliability of
% the topology template.
%
% INPUT: 
%   - problem object (adjacency matrix, functional flow and failure
%     probabilities will be taken from there)
%
% The public Run() method is used to run the reliability analysis whenever 
% needed. Adjacency matrix is first backed up (not to lose actual decision
% variables), then during preprocessing all horizontal connections between
% components of the same type are resolved to vertical connections (current
% and previous/next components in the functional flow) and then the
% recursive reliability computation routine (recReliability) is called.
% After getting the result, original adjacency matrix is restored (it might
% still be used, e.g., for next iterations of the iterative algorithm).
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: December 2016
    
    properties (GetAccess = public, SetAccess = private)
        failure_probabilities
        adjacency_matrix
        num_components
        flow
        prob
        
        num_sources
        num_sinks
        
        % most recent result will be stored in a property to avoid extra
        % calls to the analysis (it might run very long)
        last_result
    end
    
    
    methods (Access = public)
        function obj = ReliabilityAnalysis(prob)
            if nargin < 1
                error('ReliabilityAnalysis: not enough arguments.');
            else
                obj.prob                     = prob;
                obj.adjacency_matrix         = prob.AdjacencyMatrix;
                obj.num_components           = obj.adjacency_matrix.Size;
                obj.flow                     = prob.Flow;                                
                
                fprob.prob_component_failure = prob.getFailureProbabilitiesAsVector;
                fprob.prob_edge_failure      = prob.edge_failure;
                obj.failure_probabilities    = fprob;
                
                temp_matrix = ...
                    obj.adjacency_matrix.getSubMatrixNumeric(obj.flow{1}, obj.flow{end});
                obj.num_sources = size(temp_matrix, 1);
                obj.num_sinks   = size(temp_matrix, 2);
            end
        end
        
        
        function res = Run(obj)
            str = sprintf('%s: Running reliability analysis...', class(obj.prob));
            disp(str);
            
            first_sink_idx = obj.num_components - obj.num_sinks + 1;
            sink_indices = first_sink_idx:obj.num_components;
            source_indices = 1:obj.num_sources;
            
            % backup the values of the adjacency matrix (they will be
            % modified during horizontal connections resolution)
            obj.adjacency_matrix.Backup;
            
            obj.resolveHorizontalConnections;
            adjm_numeric = value(obj.adjacency_matrix.getMatrixNumeric);
            adjm_numeric = round(adjm_numeric);
            
            res=zeros(1,length(sink_indices));            
            for i=1:length(sink_indices)
                % call the recursive reliability computation routine
                res(i)=sum(recReliability(obj.failure_probabilities, ...
                        source_indices, sink_indices(i), [], adjm_numeric'));
            end 
            
            % restore original values of the adjacency matrix
            obj.adjacency_matrix.Restore;
            
            % store the last result in a property for future use
            obj.last_result = res;
        end
        
        function SetFailureProbabilities(obj, fprobs)
            obj.failure_probabilities = fprobs;
        end
    end
    
    
    methods (Access = private)
        function resolveHorizontalConnections(obj)
            % preprocess the adjacency matrix before running the rel
            % analysis. In the resulting matrix there should be no
            % connections between components of same type ("horizontal");
            % they should be replaced with connections between these
            % components and preceding/succeeding components in the
            % functional flow ("vertical").
            %
            % first compute the horizontal closure            
            for i = 1:length(obj.flow) - 1
                current_type = obj.flow{i};
                next_type = obj.flow{i+1};
                
                % if horizontal connections are possible on this level
                if Helpers.canBeConnected(obj.prob, current_type, current_type)                    
                    submatrix = obj.adjacency_matrix.getSubMatrixNumeric(current_type, current_type);
                    submatrix_values = round(value(submatrix));
                    
                    for j = 1:size(submatrix_values,1)
                        % compute the horizontal closure: A = A+A*A (up to
                        % max number of hops, i.e., number of components on
                        % current level)
                        submatrix_values = ...
                            submatrix_values + submatrix_values * submatrix_values;
                        submatrix_values = double(submatrix_values > 0);
                    end
                    
                    % make values binary
                    %submatrix_values = submatrix_values > 0;
                    
                    % update the submatrix (the adjacency matrix will also
                    % be updated, because its values are binvar-s, which
                    % are handle classes in matlab).
                    assign(submatrix, submatrix_values);
                    
                    % now horizontal connections are not needed
                    % change them to vertical
                    submatrix_next = obj.adjacency_matrix.getSubMatrixNumeric(current_type, next_type);
                    current_current = round(value(submatrix)');
                    current_next = round(value(submatrix_next)');                        
                    for j = 1:size(current_current,1)
                        for k = 1:size(current_current,2)
                            if current_current(j,k) == 1
                                current_current(j,k) = 0;
                                for l = 1:size(current_next,1)
                                    if current_next(l,k) == 1
                                        current_next(l,j) = 1;
                                    end
                                    if current_next(l,j) == 1
                                        current_next(l,k) = 1;
                                    end
                                end
                            end                            
                        end
                    end
                    
                    assign(submatrix, current_current');
                    assign(submatrix_next, current_next');
                end
            end                                    
        end
        % private methods end
    end
end

