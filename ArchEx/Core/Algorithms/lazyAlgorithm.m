function [ solution ] = lazyAlgorithm( problem, requirement, options )
% LAZYALGORITHM: Implementation of the generic iterative algorithm for 
% solving architecture exploration problems. Each iteration is a 
% combination of  running the solver, analyzing the results and learning 
% new constraints using a heuristic function. The latter are added to the 
% problem formulation. In such a way the solver is guided towards a better
% solution. Compared with the monolithic optimization, the iterative one is 
% significantly faster (the overall performance depends on the learning 
% function), but the result might not be a global optimum. This is because 
% the whole problem with all constraints alltogether is never solved.
% 
% If requirement is not SAT, the learning function is called. It returns
% the list of extra constraints to add. If it is empty, then the problem is
% considered UNFEASIBLE and the algorithm terminates. Otherwise, the solver
% is called again with extended constraints list.
% 
% INPUT: 
%   - the problem object (the corresponding class MUST implement a
%     learning function and have an accessible Analysis object)
%   - system requirement that should be compared to analysis results
%   - solver options (e.g., number of threads, verbose level etc.)
%
% OUTPUT: 
%   - elapsed time (sec)
%   - solver data (e.g., MILP constraints from solver input)
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: December 2016
    
    if isempty(options)
        opt = cplex_options('lazy');
    else
        opt = options;
    end
        
    % Flag is zero until the requirement is met and algorithm can terminate
    flag = 0;
    
    % "unfeasible" flag
    unfeasible = 0;
    
    % Number of iterations
    count = 0;
    
    cons = problem.Constraints;
    
    % turn off some unnecessary warnings
    warning( 'off', 'MATLAB:lang:badlyScopedReturnValue' );
    
    [h,m,s] = hms(datetime('now'));
    str = sprintf('Start time is %d:%d:%0.0f.', h, m, s);
    disp(str);
    
    tic    
    while flag == 0
        % increase the iteration counter
        count = count + 1;        
        
        str = sprintf('Solving the optimization problem (Iteration #%d)...', count);
        disp(str);
        
        % Call Yalmip/CPLEX to solve the problem
        solution.solver_data = optimize(cons, problem.CostFunction, opt);
        
        str = sprintf('Done (Iteration #%d)\n', count);
        disp(str);
        
        solution.graph = problem.ShowResult;        
        % !TEMP (for demo)
        %updateGraphColoring(solution.graph);
        %pause(1);
        
        % run the analysis and check if the requirement is satisfied        
        res_analysis = problem.Analysis.Run;
        flag = min(res_analysis < requirement); % what about <=, >=, >, ==, != ??
        
        % if unfeasible, terminate (all values are "0")
        if sum(res_analysis) == 0
            unfeasible = 1;
            break;
        end                    
        
        % print every iteration results to console
        str = sprintf('\n%s after iteration #%d:', class(problem.Analysis), count);
        disp(str);
        disp(res_analysis);                        
        
        if ~flag
            % requirement not SAT, run LearnCons            
            fprintf('\nRequirement is not SAT. Running LearnCons()\n');
            new_cons = problem.LearnCons(res_analysis);
            if isempty(new_cons)
                % LearnCons returns 0 extra constraints (i.e. it does not
                % know what to add to solve the problem, that is, the
                % problem is unfeasible)
                disp('Unfeasible (no more extra constraints to add).');
                break;
            else
                str = sprintf('Number of new MILP constraints learned: %d\n', length(new_cons));
                cons = [cons, new_cons];
                disp(str);
            end
        end                                       
    end
    
    solution.time_elapsed = toc;
    if ~unfeasible
        disp('Requirement is SAT.');
    end
    disp('Done solving.');
    
    % print results to console
    fprintf('\n=======================  RESULTS:  =========================\n');
    str = sprintf('Total number of iterations: %d', count);
    disp(str);
    if ~unfeasible        
        fprintf('%s (final):\n', class(problem.Analysis));
        disp(res_analysis);
    else
        close all;
        disp('Final result: UNFEASIBLE.');
    end           
end

