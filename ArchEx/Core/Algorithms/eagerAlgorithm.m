function [ solution ] = eagerAlgorithm( problem, options )
% EAGERALGORITHM: Implementation of a monolithic optimization routine,
% which composes the problem using all provided constraints and tries to
% solve it in one shot ("eagerly").
%
% INPUT: 
%   - the problem object 
%   - solver options (e.g., number of threads, verbose level etc.)
% 
% OUTPUT: 
%   - elapsed time (sec)
%   - solver data (e.g., MILP constraints from solver input)
%
% The execution time of the function may be slow because it tries to solve
% the full problem. The advantage is that it returns the globally optimal 
% solution (maybe with respect to a certain approximation - this depends on
% a particular problem), The downside is the rather slow timing.
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: December 2016

    if isempty(options)
        opt = cplex_options('eager');
    else
        opt = options;
    end
    
    % merge the primary set of constraints with additional constraints
    % related to a certain viewpoint (e.g., reliability) to formulate a
    % monolithic problem.
    cons = [problem.Constraints, problem.AuxiliaryConstraints];
    
    % turn off some unnecessary warnings
    warning( 'off', 'MATLAB:lang:badlyScopedReturnValue' );    
    
    [h,m,s] = hms(datetime('now'));
    str = sprintf('Start time is %d:%d:%0.0f.', h, m, s);
    disp(str);
    disp('Solving the optimization problem...');
    tic
    
    % Call Yalmip/CPLEX to solve the problem
    %solvesdp(cons, problem.CostFunction, options);
    solution.solver_data = optimize(cons, problem.CostFunction, opt);
    
    disp('Done solving.');
    solution.time_elapsed = toc;
    
    % check if feasible solution was found or not
    feasible = solution.solver_data.solveroutput.exitflag > 0;    
    
    if feasible
        % draw the resulting graph
        solution.graph = problem.ShowResult;
        pause(1);
        
        % store the graph in the base workspace
        assignin('base', 'graph', solution.graph);
        
        % run the analysis and show result
        res_analysis = problem.Analysis.Run;
        fprintf('\n=======================  RESULTS:  =========================\n');
        if ~(length(res_analysis) == 1 && res_analysis == -1)
            % if the analysis is implemented
            fprintf('%s:\n', class(problem.Analysis));
            disp(res_analysis);
        end
    else
        fprintf('\n=======================  RESULTS:  =========================\n');
        fprintf('%s: UNFEASIBLE.\n', class(problem));
        solution.graph = [];
        assignin('base', 'graph', solution.graph);
    end             
end

