classdef Patterns
% The class PATTERNS contains the implementation of the patterns available
% to specify the requirements (optimization constraints) for the
% architectire exploration (optimization) problem.
%
% The class is implemented as a static class, since a "state" is not needed. 
% Every method returns the set of MILP constraints over the binary 
% variables taken from matrices of decision variables (e.g. adjacency
% matrix, mapping matrix). All patterns (except simple low-level ones) can
% be called using a similar interface, which includes following arguments:
% the problem object (prob), types and/or subtypes of components (T,S) and
% a possible numerical value, if required. In such a way pattern methods 
% can be called both from the ConstraintHandler class, which parses the
% input file, and directly by the users to add additional constraints to
% their problem by calling a particular pattern.

% First, required decision variables are fetched for required types/subtypes 
% using helper functions (e.g. getConnectivityMatrix). Next, these variables 
% are used to formulate the constraint. 
%
% The YALMIP framework is currently used to formulate MILP constraints.
%
% NOTATION:
% E - adjacency matrix
% C - connectivity matrix between two component types (i.e., submatrix
%     of the adjacency matrix E related to these types)
% Q - quantity matrix (matrix of size same to E, which represents labeling
%     of edges with some quantity value; example: flow rate)
% M - mapping matrix (rows - "real" components from library, columns -
%     "virtual" components of type T)
% T - type of a component
% S - subtype of a component
% val - numerical value (e.g., maximum number of allowed connections
%       between components of types T1 and T2)
% '(prime) - input argument is optional (e.g. S')
%
% REFERENCES:
% [1] D. Kirov, P. Nuzzo, R. Passerone and A. Sangiovanni-Vincentelli. 
%     "ArchEx 2.0: An Extensible Framework for the Exploration of
%     Cyber-Physical System Architectures".
% [2] N. Bajaj, P.Nuzzo et al. "Optimized Selection of Reliable and Cost-Effective
%     Cyber-Physical System Architectures," Proc. DATE, Mar. 2015.
%
% Authors: Dmitrii Kirov and Michele Lora and Pierluigi Nuzzo
% Last modified: December 2016
    
    methods(Static = true, Access = public)
        
        %% Simple patterns (mostly for internal use)
        %  Express basic algebraic relations
                
        function [ milp_constraints ] = atMost( op1, op2 )
            % OP1 is smaller than or equal to OP2
            % Function ATMOST takes two parameters OP1 and OP2 and returns
            % the YALMIP inequality OP1 <= OP2.
            milp_constraints = op1 <= op2;
            return
        end

        
        function [ milp_constraints ] = atLeast( op1, op2 )
            % OP1 is greater than or equal to OP2
            % Function ATLEAST takes two parameters OP1 and OP2 and returns
            % the YALMIP inequality OP1 >= OP2.
            milp_constraints = op1 >= op2;
            return;
        end
        
        
        function [ milp_constraints ] = equal( op1, op2 )
            % Function EQUAL takes two parameters OP1 and OP2 and returns
            % the YALMIP equation OP1 = OP2.
            % In this implementation, the constraint is the conjunction of 
            % the constraints obtained from the ATLEAST and ATMOST patterns
            milp_constraints = Patterns.atMost(op1, op2);
            milp_constraints = [milp_constraints, Patterns.atLeast(op1, op2)];
            return;
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Following patterns are suggested to be used for defining
        % architecture exploration problems that ArchEx currently supports.
        % They are classified into several groups (e.g., interconnection,
        % timing, reliability etc.) within the ConstraintHandler class so
        % it can store them separately and then flexibly join to formulate
        % optimization problems. For each pattern a short english
        % description is provided, that describes the related requirement.
        % More details on their functionality can be found in the ArchEx 
        % documentation.
        %
        % New patterns for extending ArchEx should be added here.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %% GENERAL PATTERNS
        % 
        %% at_least_N_components( T, S', val )
        function [ milp_constraint ] = at_least_N_components( prob, T, S, val )
            % There must be at least <val> component(s) of type T with
            % subtype S.
            M = Helpers.getMappingMatrix(prob, T);
            if ~isempty(S)
                T_lib = prob.Library.findRelatedType(T);
                subtypes_of_T = prob.Library.Subtypes(T_lib);
                milp_constraint = Patterns.atLeast(sum(subtypes_of_T(S) * M), val);
            else
                milp_constraint = Patterns.atLeast(sum(max(T,[],1)), val);
            end
        end
        
        
        %% at_most_N_components( T, S', val )
        function [ milp_constraint ] = at_most_N_components( prob, T, S, val )
            % There must be at most <val> component(s) of type T with
            % subtype S.
            M = Helpers.getMappingMatrix(prob, T);
            if ~isempty(S)
                T_lib = prob.Library.findRelatedType(T);
                subtypes_of_T = prob.Library.Subtypes(T_lib);
                milp_constraint = Patterns.atMost(sum(subtypes_of_T(S) * M), val);
            else
                milp_constraint = Patterns.atMost(sum(max(T,[],1)), val);
            end
        end
        
        
        %% exactly_N_components( T, S', val )
        function [ milp_constraint ] = exactly_N_components( prob, T, S, val )
            % There must be exactly <val> component(s) of type T with
            % subtype S.
            M = Helpers.getMappingMatrix(prob, T);
            if ~isempty(S)
                T_lib = prob.Library.findRelatedType(T);
                subtypes_of_T = prob.Library.Subtypes(T_lib);
                milp_constraint = Patterns.equal(sum(subtypes_of_T(S) * M), val);
            else
                milp_constraint = Patterns.equal(sum(max(T,[],1)), val);
            end
        end
           
        
        %% INTERCONNECTION PATTERNS
        %(DO WE NEED THIS?)  These patterns are used to express a relation ~ among components 
        %  of an architecture. Given an adjacency matrix M of size |I|x|J|,
        %  describing the interconnections between the elements of set I = 
        %  {1,...,|I|} and J = {1,...,|J|}, and a scalar S, the resulting 
        %  set of constraints impose that the number of connections of any 
        %  element j in J (j in [1:|J|]) with an element in I is in the 
        %  relation ~ with S. In other words, the sum of the row elements 
        %  of all columns in M is in the relation ~ with the scalar, i.e.,
        %  for all j in [1:|J|]: M(j) = M(1,j) + M(2,j) + ... + M(I,j) ~ S
        
        %% at_least_N_connections( T1, S1', T2, S2', val )
        function [ milp_constraint ] = at_least_N_connections( prob, T1, S1, T2, S2, val )
            % Every componnent of type T1 with subtype S1 must have at
            % least <val> connection(s) to a component of type T2 with
            % subtype S2.
            % NOTE: S1 and S2 are optional.
            C = Helpers.getConnectivityMatrix(prob, T1, T2);
            if ~isempty(S1) && ~isempty(S2)
                % S1 and S2 arguments not yet supported
            else            
                s = size(C,2);
                const(1:s) = val;
                milp_constraint = Patterns.atLeast(sum(C,1), const);
            end
        end
        
        
        %% at_most_N_connections( T1, S1', T2, S2', val )
        function [ milp_constraint ] = at_most_N_connections( prob, T1, S1, T2, S2, val )
            % Every componnent of type T1 with subtype S1 must have at
            % most <val> connection(s) to a component of type T2 with
            % subtype S2.
            % NOTE: S1 and S2 are optional.
            C = Helpers.getConnectivityMatrix(prob, T1, T2);
            if ~isempty(S1) && ~isempty(S2)
                % S1 and S2 arguments not yet supported
            else            
                s = size(C,2);
                const(1:s) = val;
                milp_constraint = Patterns.atMost(sum(C,1), const);
            end
        end
        
        
        %% exactly_N_connections( T1, S1', T2, S2', val )
        function [ milp_constraint ] = exactly_N_connections( prob, T1, S1, T2, S2, val )
            % Every componnent of type T1 with subtype S1 must have exactly
            % <val> connection(s) to a component of type T2 with subtype S2.
            % NOTE: S1 and S2 are optional.
            C = Helpers.getConnectivityMatrix(prob, T1, T2);
            if ~isempty(S1) && ~isempty(S2)
                % S1 and S2 arguments not yet supported
            else            
                s = size(C,2);
                const(1:s) = val;
                milp_constraint = Patterns.equal(sum(C,1), const);
            end
        end
        
        
        %% at_least_N_connections_if_used( T1, S1', T2, S2', val )
        function [ milp_constraint ] = at_least_N_connections_if_used( prob, T1, S1, T2, S2, val )
            % Every componnent of type T1 with subtype S1, which is used
            % (instantiated) must have at least <val> connection(s) to a 
            % component of type T2 with subtype S2.
            % NOTE: S1 and S2 are optional.
            C = Helpers.getConnectivityMatrix(prob, T1, T2);
            M = Helpers.getMappingMatrix(prob, T1, T2);                        
            milp_constraint = [];
            if ~isempty(S1) && ~isempty(S2)
                % S1 and S2 arguments not yet supported
            else                            
                for i = 1:size(C,2)
                    milp_constraint = [milp_constraint, implies(max(M(:,i),[],1), ...
                        Patterns.atLeast(sum(C(:,i),1), val), 1e-2)];
                end
            end
        end
        
        
        %% at_most_N_connections_if_used( T1, S1', T2, S2', val )
        function [ milp_constraint ] = at_most_N_connections_if_used( prob, T1, S1, T2, S2, val )
            % Every componnent of type T1 with subtype S1, which is used
            % (instantiated) must have at most <val> connection(s) to a 
            % component of type T2 with subtype S2.
            % NOTE: S1 and S2 are optional.
            C = Helpers.getConnectivityMatrix(prob, T1, T2);            
            M = Helpers.getMappingMatrix(prob, T1, T2);             
            milp_constraint = [];
            if ~isempty(S1) && ~isempty(S2)
                % S1 and S2 arguments not yet supported
            else                            
                for i = 1:size(C,2)
                    milp_constraint = [milp_constraint, implies(max(M(:,i),[],1), ...
                        Patterns.atMost(sum(C(:,i),1), val), 1e-2)];
                end
            end
        end
        
        
        %% exactly_N_connections_if_used( T1, S1', T2, S2', val )
        function [ milp_constraint ] = exactly_N_connections_if_used( prob, T1, S1, T2, S2, val )
            % Every componnent of type T1 with subtype S1, which is used
            % (instantiated) must have exactly <val> connection(s) to a 
            % component of type T2 with subtype S2.
            % NOTE: S1 and S2 are optional.
            C = Helpers.getConnectivityMatrix(prob, T1, T2);            
            M = Helpers.getMappingMatrix(prob, T1, T2);             
            milp_constraint = [];
            if ~isempty(S1) && ~isempty(S2)
                % S1 and S2 arguments not yet supported
            else                            
                for i = 1:size(C,2)
                    milp_constraint = [milp_constraint, implies(max(M(:,i),[],1), ...
                        Patterns.equal(sum(C(:,i),1), val), 1e-2)];
                end
            end
        end                                
        
        % Following patterns define "if-then" connectivity relations
        % between components. T_in and T_out are, respectively, types,
        % from/to which type T must (or must not) have incoming/outgoing
        % connections. C_in and C_out are, respectively, connectivity
        % matrices between T_in (rows) and T (columns), and T (rows) and
        % T_out (columns). All following constraints are specified using
        % C_in and C_out using either (in)equality operator or implies().
        %
        %% in_conn_implies_out_conn( T, T_in, T_out )
        function [ milp_constraint ] = in_conn_implies_out_conn( prob, T, T_in, T_out )
            % For every component of type T: if it has an incoming
            % connection from a component of type T_in, then it must also
            % have an outgoing connection to a component of type T_out.
            E = prob.AdjacencyMatrix;
            [C_in, C_out] = Helpers.getIncomingOutgoing(prob, E, T, T_in, T_out);
            milp_constraint = max(C_in,[],1) <= max(C_out,[],1);
        end
        
        
        %% no_in_conn_implies_out_conn( T, T_in, T_out )
        function [ milp_constraint ] = no_in_conn_implies_out_conn( prob, T, T_in, T_out )
            % For every component of type T: if it has no incoming
            % connections from components of type T_in, then it must
            % have an outgoing connection to a component of type T_out.
            E = prob.AdjacencyMatrix;
            [C_in, C_out] = Helpers.getIncomingOutgoing(prob, E, T, T_in, T_out);
            has_incoming = max(C_in,[],1);
            has_outgoing = max(C_out,[],1);
            milp_constraint = [];
            for i = 1:length(has_incoming)
                milp_constraint = [milp_constraint, ...
                    implies(has_incoming(i) == 0, has_outgoing(i) == 1, 1e-2)];
            end
        end
        
        
        %% in_conn_implies_no_out_conn( T, T_in, T_out )
        function [ milp_constraint ] = in_conn_implies_no_out_conn( prob, T, T_in, T_out )
            % For every component of type T: if it has an incoming
            % connection from a component of type T_in, then it must 
            % have no outgoing connections to components of type T_out.
            E = prob.AdjacencyMatrix;
            [C_in, C_out] = Helpers.getIncomingOutgoing(prob, E, T, T_in, T_out);
            has_incoming = max(C_in,[],1);
            has_outgoing = max(C_out,[],1);
            milp_constraint = [];
            for i = 1:length(has_incoming)
                milp_constraint = [milp_constraint, ...
                    implies(has_incoming(i) == 1, has_outgoing(i) == 0, 1e-2)];
            end
        end
        
        
        %% no_in_conn_implies_no_out_conn( T, T_in, T_out )
        function [ milp_constraint ] = no_in_conn_implies_no_out_conn( prob, T, T_in, T_out )
            % For every component of type T: if it has no incoming
            % connections from components of type T_in, then it must also
            % have no outgoing connections to components of type T_out.
            E = prob.AdjacencyMatrix;
            [C_in, C_out] = Helpers.getIncomingOutgoing(prob, E, T, T_in, T_out);
            milp_constraint = max(C_in,[],1) >= max(C_out,[],1);
        end
        
        
        %% out_conn_implies_in_conn( T, T_out, T_in )
        function [ milp_constraint ] = out_conn_implies_in_conn( prob, T, T_out, T_in )
            % For every component of type T: if it has an outgoing
            % connection to a component of type T_out, then it must also
            % have an incoming connection from a component of type T_in.
            E = prob.AdjacencyMatrix;
            [C_in, C_out] = Helpers.getIncomingOutgoing(prob, E, T, T_in, T_out);
            milp_constraint = max(C_out,[],1) <= max(C_in,[],1);
        end
        
        
        %% no_out_conn_implies_in_conn( T, T_out, T_in )
        function [ milp_constraint ] = no_out_conn_implies_in_conn( prob, T, T_out, T_in )
            % For every component of type T: if it has no outgoing
            % connections to components of type T_out, then it must
            % have an incoming connection from a component of type T_in.
            E = prob.AdjacencyMatrix;
            [C_in, C_out] = Helpers.getIncomingOutgoing(prob, E, T, T_in, T_out);
            has_outgoing = max(C_out,[],1);
            has_incoming = max(C_in,[],1);            
            milp_constraint = [];
            for i = 1:length(has_outgoing)
                milp_constraint = [milp_constraint, ...
                    implies(has_outgoing(i) == 0, has_incoming(i) == 1, 1e-2)];
            end
        end
        
        
        %% out_conn_implies_no_in_conn( T, T_out, T_in )
        function [ milp_constraint ] = out_conn_implies_no_in_conn( prob, T, T_out, T_in )
            % For every component of type T: if it has an outgoing
            % connection to a component of type T_out, then it must
            % have no incoming connections from components of type T_in.
            E = prob.AdjacencyMatrix;
            [C_in, C_out] = Helpers.getIncomingOutgoing(prob, E, T, T_in, T_out);
            has_outgoing = max(C_out,[],1);
            has_incoming = max(C_in,[],1);            
            milp_constraint = [];
            for i = 1:length(has_outgoing)
                milp_constraint = [milp_constraint, ...
                    implies(has_outgoing(i) == 1, has_incoming(i) == 0, 1e-2)];
            end
        end
        
        
        %% no_out_conn_implies_no_in_conn( T, T_out, T_in )
        function [ milp_constraint ] = no_out_conn_implies_no_in_conn( prob, T, T_out, T_in )
            % For every component of type T: if it has no outgoing
            % connections to components of type T_out, then it must also
            % have no incoming connections from components of type T_in.
            E = prob.AdjacencyMatrix;
            [C_in, C_out] = Helpers.getIncomingOutgoing(prob, E, T, T_in, T_out);
            milp_constraint = max(C_out,[],1) >= max(C_in,[],1);
        end
        
        
        %% bidirectional_connection( T1, T2 )
        function [ milp_constraint ] = bidirectional_connection( prob, T1, T2 )
            % Connections between components of types T1 and T2 must be
            % bidirectional.
            % This is done by constraining the connectivity matrix C
            % between T1 and T2 to be equal to its transpose, i.e., if
            % there is an outgoing edge from T1(i) to T2(j) then there must
            % be also an incoming edge to T1(i) from T2(j).
            C1 = Helpers.getConnectivityMatrix(prob, T1, T2);
            C2 = Helpers.getConnectivityMatrix(prob, T2, T1);
            milp_constraint = Patterns.equal(C1, C2');
        end
        
        
        %% no_bidirectional_connection( T1, T2 )
        function [ milp_constraint ] = no_bidirectional_connection( prob, T1, T2 )
            % Connections between components of types T1 and T2 must NOT be
            % bidirectional.
            % NOTE: the pattern is supposed to be used only if T1 = T2. Our
            % formulation is based on the assumption that there are no 
            % "backward" connections in the functional flow (it is already
            % restricted using the adjacency matrix constraints). So if
            % T1 ~= T2, this pattern is not required.
%             C = Helpers.getConnectivityMatrix(prob, T1, T2);
%             for i = size(C,1)
%                 for j = size(C,2)
%                     if i == j
%                         continue
%                     end
%                     milp_constraint = [milp_constraint, ...
%                         implies(C(i,j) == 1, C(j,i) == 0, 1e-2)];
%                 end
%             end
            C1 = Helpers.getConnectivityMatrix(prob, T1, T2);
            C2 = Helpers.getConnectivityMatrix(prob, T2, T1);
            for i = size(C1,1)
                for j = size(C2,2)
                    if i == j
                        continue
                    end
                    milp_constraint = [milp_constraint, ...
                        implies(C1(i,j) == 1, C2(j,i) == 0, 1e-2)];
                end
            end
        end
        
        
        %% no_self_loops( T )
        function [ milp_constraint ] = no_self_loops( prob, T )
            % Components of type T must not be connected to themselves
            % (this means a self-loop).
            C = Helpers.getConnectivityMatrix(prob, T, T);
            milp_constraint = [];
            for i = 1:size(C,1)
                milp_constraint = [milp_constraint, C(i,i) == 0];
            end
        end
        
        
        %% cannot_connect( T1, S1, T2, S2 )
        function [ milp_constraint ] = cannot_connect( prob, T1, S1, T2, S2 )
            % Components of type T1 with subtype S1 must not be connected
            % to components of type T2 with subtype S2.
            %            
            % get vectors that store mapping of each virtual component to 
            % the subtypes from the constraint input ("1" if current 
            % component is mapped to the required subtype, "0" otherwise)
            T1_mapping_to_S1 = Helpers.getMappingToSubtype(prob, T1, S1);
            T2_mapping_to_S2 = Helpers.getMappingToSubtype(prob, T2, S2);

            % get connectivity matrix
            C = prob.AdjacencyMatrix.getSubMatrixNumeric(T1, T2);
            rows = T1_mapping_to_S1;
            cols = T2_mapping_to_S2;
            %connectivity_matrix = connectivity_matrix';                                

            if ~any(isnan(value(C)))
                C = prob.AdjacencyMatrix.getSubMatrixNumeric(T2, T1);     
                rows = T2_mapping_to_S2;
                cols = T1_mapping_to_S1;
            end                                   

            % add an "implies" constraint for all possible mappings of
            % T1 and T2: IF T1 and T2 are mapped to
            % incompatible subtypes THEN they cannot be connected.
            milp_constraint = [];
            for i = 1:size(C,2)
                for j = 1:size(C,1)
                    milp_constraint = [milp_constraint,...
                        implies(cols(i) & rows(j), C(j,i) == 0, 1e-2)];
                end
            end

            % store pairs of restricted type-subtype mappings in a
            % structure. This information may be required during
            % solving (e.g. in learning)
            if ~any(strcmp(keys(prob.restrictions), T1))
                % if such type is not yet present in restrictions
                prob.restrictions(T1) = containers.Map;
            end
            if ~any(strcmp(keys(prob.restrictions), T2))
                prob.restrictions(T2) = containers.Map;
            end

            % store restricted pairs of mappings
            new_restriction = prob.restrictions(T1);
            new = {T1_mapping_to_S1, T2_mapping_to_S2};
            if ~isKey(new_restriction, T2)
                new_restriction(T2) = {new};                  
            else
                new_restriction(T2) = [new_restriction(T2), {new}];                    
            end
            prob.restrictions(T1) = new_restriction;

            % obj.restrictions has certain redundancy: the same
            % restriction will be stored twice (for T1 with T2
            % and for T2 with T1) in order to handle any ordering
            % of inputs when the structure is used.
            new_restriction = prob.restrictions(T2);
            new = {T2_mapping_to_S2, T1_mapping_to_S1};
            if ~isKey(new_restriction, T1)                    
                new_restriction(T1) = {new};                        
            else
                new_restriction(T1) = [new_restriction(T1), {new}];                        
            end
            prob.restrictions(T2) = new_restriction;
        end
        
        
        %% PATH PATTERNS
        %
        %% at_least_N_paths( T1, S1', T2, S2', val )
        function [ milp_constraint ] = at_least_N_paths( prob, T1, S1, T2, S2, val )
            % Every component of type T1 with subtype S1 must have at least
            % <val> paths to components of type T2 with subtype S2.
            % NOTE: S1 and S2 are optional.
            %
            % Notation: P - path matrix(represents paths between T1 and T2)
            milp_constraint = [];
            T1_idx = find(ismember(prob.Flow, T1));
            T2_idx = find(ismember(prob.Flow, T2));
            try
                if T1_idx < T2_idx
                    P = prob.allPathsLayer(T1, T2);
                elseif T1_idx > T2_idx
                    P = prob.allPathsLayer(T2, T1);
                end
            catch
                warning(sprintf('Patterns::at_least_N_paths: path matrix between %s and %s does not exist', T1, T2));
                return
            end
            
            s = size(P,2);
            const(1:s) = val;
            % NOTE: so far this constraint assumes that every "destination"
            % component (end of path) is instantiated. 
            %% ToDo: check whether component is used or not
            milp_constraint = Patterns.atLeast(sum(P,1), const);                
        end
        
        
        %% at_most_N_paths( T1, S1', T2, S2', val )
        function [ milp_constraint ] = at_most_N_paths( prob, T1, S1, T2, S2, val )
            % Every component of type T1 with subtype S1 must have at most
            % <val> paths to components of type T2 with subtype S2.
            % NOTE: S1 and S2 are optional.
            %
            % Notation: P - path matrix(represents paths between T1 and T2)
            milp_constraint = [];
            T1_idx = find(ismember(prob.Flow, T1));
            T2_idx = find(ismember(prob.Flow, T2));
            try
                if T1_idx < T2_idx
                    P = prob.allPathsLayer(T1, T2);
                elseif T1_idx > T2_idx
                    P = prob.allPathsLayer(T2, T1);
                end
            catch
                warning(sprintf('Patterns::at_least_N_paths: path matrix between %s and %s does not exist', T1, T2));
                return
            end
            
            s = size(P,2);
            const(1:s) = val;
            % NOTE: so far this constraint assumes that every "destination"
            % component (end of path) is instantiated. 
            %% ToDo: check whether component is used or not
            milp_constraint = Patterns.atMost(sum(P,1), const);
        end
        
        
        %% exactly_N_paths( T1, S1', T2, S2', val )
        function [ milp_constraint ] = exactly_N_paths( prob, T1, S1, T2, S2, val )
            % Every component of type T1 with subtype S1 must have exactly
            % <val> paths to components of type T2 with subtype S2.
            % NOTE: S1 and S2 are optional.
            %
            % Notation: P - path matrix(represents paths between T1 and T2)
            milp_constraint = [];
            T1_idx = find(ismember(prob.Flow, T1));
            T2_idx = find(ismember(prob.Flow, T2));
            try
                if T1_idx < T2_idx
                    P = prob.allPathsLayer(T1, T2);
                elseif T1_idx > T2_idx
                    P = prob.allPathsLayer(T2, T1);
                end
            catch
                warning(sprintf('Patterns::at_least_N_paths: path matrix between %s and %s does not exist', T1, T2));
                return
            end
            
            s = size(P,2);
            const(1:s) = val;
            % NOTE: so far this constraint assumes that every "destination"
            % component (end of path) is instantiated. 
            %% ToDo: check whether component is used or not
            milp_constraint = Patterns.equal(sum(P,1), const);
        end
        
        
        %% FLOW PATTERNS
        %
        %% flow_balance( T, S' )
        function [ milp_constraint ] = flow_balance( prob, T, S )
            % Input flow to every component of type T with subtype S must
            % be equal to the output flow from this component.
            % (this is a balance equation)
            milp_constraint = [];
            for op_mode = prob.Requirement.Operation
                op_mode = op_mode{1};
                Q_set = prob.flowRateMatrices(char(op_mode.mode));
                
                for tag = keys(op_mode.map)
                    % skip flow matrices of all zeros (no flow)
                    if op_mode.map(char(tag)) == 0
                        continue;
                    end
                    
                    Q = Q_set(char(tag));
                    % get submatrices of Q representing input and output flows
                    % to/from component T
                    [Q_in, Q_out] = Helpers.getIncomingOutgoing(prob, Q, T);                

                    if ~isempty(S)
                        % if S argument is specified, constrain the flow
                        % balance only for components T of subtype S
                        M     = Helpers.getMappingMatrix(prob, T);
                        T_lib = prob.Library.findRelatedType(T);
                        S_lib = prob.Library.Subtypes(T_lib);
                        S_mapped = S_lib(S) * M;
                        for i = 1:length(S_mapped)
                            milp_constraint = [milp_constraint, ...
                                implies(S_mapped(i), ...
                                Patterns.equal(sum(Q_in(:,i)), sum(Q_out(:,i))), 1e-2)];
                        end                     
                    else
                        % otherwise - balance equation for all components of T
                        milp_constraint = [milp_constraint, ...
                            Patterns.equal(sum(Q_in,1),sum(Q_out,1))];
                    end

                    % store input rates
                    if any(strcmp(T, keys(prob.inputRates)))
                        prob.inputRates(T) = [prob.inputRates(T), sum(Q_in,1)];
                    else
                        prob.inputRates(T) = sum(Q_in,1);
                    end
                end
            end
        end
        
        
        %% BALANCE PATTERNS
        %
        %% has_sufficient_power( T1, S1', T2, S2', val )
        function [ milp_constraint ] = has_sufficient_power( prob, T1, S1, T2, S2 )
            % Components of type T1 with subtype S1 (alltogether) must be
            % able to power components of type T2 with subtype S2
            % (alltogether).
            %
            % This is a domain specific pattern currently applied in EPS.
            % To use it, components of T1 and T2 must be labeled with power
            % reqirements or capacities in the library (Powers array must
            % be present).
            
            % separate component type from tag (e.g., we need "Generator" 
            % instead of "Generator Left" to get a proper submatrix from 
            % adjacency matrix).
            T1 = strsplit(T1,'+');
            T2 = strsplit(T2,'+');
            
            T1_general = strsplit(strtrim(T1{1}));
            T2_general = strsplit(strtrim(T2{1}));
            T1_general = T1_general{1};
            T2_general = T2_general{1};
            T1_mapping = Helpers.getMappingMatrix(prob, T1);
            T2_mapping = Helpers.getMappingMatrix(prob, T2);

            % Add the power constraint, which states that the sum of
            % powers of components of type A, which are used, should be
            % at least as big as power requirements of components B.
            % Components that are instantiated (used) can be seen from
            % the mapping matrix (if a virtual component is mapped to
            % a real one from the library then it is used - this
            % constraint is put during the mapping).
            milp_constraint = ...
                Patterns.atLeast(sum(prob.Library.Powers(T1_general).*sum(T1_mapping,2)'), ...
                    sum(prob.Library.Powers(T2_general).*sum(T2_mapping,2)'));            
        end
        
        
        %% has_sufficient_power_if_connected( T1, S1', T2, S2', val )
        function [ milp_constraint ] = has_sufficient_power_if_connected( prob, T1, S1, T2, S2 )
            % Every component of type T1 with subtype S1 must have
            % sufficient power for all components of type T2 with subtype
            % S2 that are connected to it.
            % NOTE: S1 and S2 are optional.
            %
            % This is also a domain-specific pattern. It currently supports
            % only direct connections (e.g. a DC bus must be able to power
            % all loads directly connected to it, i.e. in single hop).
            % NOTE: more generalized version would require having auxiliary
            % path varibles/matrices in the problem formulation.
            
            % separate component type from tag (e.g., we need "Generator" 
            % instead of "Generator Left" to get a proper submatrix from 
            % adjacency matrix).
            T1 = strsplit(T1,'+');
            T2 = strsplit(T2,'+');           
            
            T1_general = strsplit(strtrim(T1{1}));
            T2_general = strsplit(strtrim(T2{1}));
            T1_general = T1_general{1};
            T2_general = T2_general{1};
            T1_mapping = Helpers.getMappingMatrix(prob, T1);
            T2_mapping = Helpers.getMappingMatrix(prob, T2);

            % compose the connectivity matrix from sub-parts (e.g. if
            % we need a matrix DCBus, Load Left + Load Right; this
            % cannot be done with a single query in current
            % implementation of the AdjacencyMatrix class)
            %% ToDo: improve the AdjacencyMatrix interface?
            %% ToDo2: handle the reverted input!!!
            C = [];
            for i = T1
                new_row = [];
                i = strtrim(i);
                for j = T2
                    j = strtrim(j);
                    piece = prob.AdjacencyMatrix.getSubMatrixNumeric(char(i), char(j));
                    new_row = [new_row, piece];
                end
                C = [C; new_row];
            end
            C = C';

            milp_constraint = ...
                Patterns.atLeast(prob.Library.Powers(T1_general) * T1_mapping,...
                    (prob.Library.Powers(T2_general) * T2_mapping) * C);                
        end                                      
        
        
        %% WORKLOAD PATTERNS
        %
        %% no_overloads( T, S' )
        function [ milp_constraint ] = no_overloads( prob, T, S )
            % Components of type T with subtype S must not be overloaded,
            % i.e. the input flow rate must be less than or equal to
            % component's processing rate. Otherwise component won't be
            % able to handle the input traffic and the queue will grow
            % infinitely.
            milp_constraint = [];
            T_lib = prob.Library.findRelatedType(T);
            T_mapping = prob.LibraryMapping(T);
            processing_rates = prob.Library.ProcessingRates(T_lib);            
            mapped_proc_rates = processing_rates * T_mapping;
            
            for op_mode = prob.Requirement.Operation 
                op_mode = op_mode{1};
                Q_set = prob.flowRateMatrices(op_mode.mode);                
                    
                % calculate the input rates for T for current op_mode
                input_sum = [];
                for tag = keys(op_mode.map)
                    % skip flow matrices of all zeros (no flow)
                    if op_mode.map(char(tag)) == 0
                        continue;
                    end
                    Q = Q_set(char(tag));
                    % get submatrix of Q representing input flow to T (Q_in)
                    Q_in = Helpers.getIncomingOutgoing(prob, Q, T);
                    if ~isempty(input_sum)
                        input_sum = input_sum + sum(Q_in,1);
                    else
                        input_sum = sum(Q_in,1);
                    end
                end

                if ~isempty(S)
                    % if S argument is specified, put the load constraint
                    % only for components T of subtype S
                    M     = Helpers.getMappingMatrix(prob, T);
                    T_lib = prob.Library.findRelatedType(T);
                    S_lib = prob.Library.Subtypes(T_lib);
                    S_mapped = S_lib(S) * M;
                    for i = 1:length(S_mapped)
                        milp_constraint = [milp_constraint, ...
                            implies(S_mapped(i), ...
                            Patterns.atLeast(input_sum(i), mapped_proc_rates(i)), 1e-2)];
                    end
                else
                    % load constraint for all components of type T
                    milp_constraint = [milp_constraint, ...
                        Patterns.atMost(input_sum, mapped_proc_rates)];
                end
            end  
            % store processing rates
            prob.procRates(T)  = mapped_proc_rates;
        end
            
        
        %% TIMING PATTERNS
        %
        %% max_cycle_time( T, S', val )
        function [ milp_constraint ] = max_cycle_time( prob, T, S, val )
            % The cycle time at component of type T with subtype S must be
            % less than or equal to <val>.
            %
            % Cycle time is the time an entity/signal (e.g. a product, a
            % detail) is being process in the system starting from source
            % and ending at component T. The requirement can be also read
            % as "the message must reach component T within <val> time units"
            %
            %% ToDo: generalized version (possibility of constraining the cycle time between T1 and T2             
        end
        
        
        %% max_idle_rate_sum( T, S', val )
        function [ milp_constraint ] = max_idle_rate_sum( prob, T, S, val )
            % Total idle rate of components of type T with sybtype S must
            % be less than or equal to <val>.
            %
            % Idle rate is the rate of a component/machine of doing no job.
            % It is the difference between processing rate u and input flow
            % rate l. For instance, if a component processes 10 messages
            % every second (u = 10 msg/sec) and the input message flow rate
            % l = 8 msg/sec, then idle rate is 2 msg/sec (the component is
            % able to process 2 extra messages every second, but has to
            % remain idle as input flow is smaller).
            % NOTE: S is optional
            milp_constraint = [];  
            total_idle_rate = 0;
            if strcmp(T, 'TOTAL') == 1
                types = prob.machines;
            else
                types = T;
            end
            
            for T = types
                T = char(T);
                T_lib = prob.Library.findRelatedType(T);
                T_mapping = prob.LibraryMapping(T);
                processing_rates = prob.Library.ProcessingRates(T_lib);            
                mapped_proc_rates = processing_rates * T_mapping;
                T_idle_rate = 0;
                idle_rate_vect = zeros(1, length(mapped_proc_rates));
                for op_mode = prob.Requirement.Operation
                    op_mode = op_mode{1};
                    Q_set = prob.flowRateMatrices(char(op_mode.mode));
                    
                    % calculate the input rates for T for current op_mode
                    input_sum = [];
                    for tag = keys(op_mode.map)
                        Q = Q_set(char(tag));
                        % get submatrix of Q representing input flow to T (Q_in)
                        Q_in = Helpers.getIncomingOutgoing(prob, Q, T);
                        if ~isempty(input_sum)
                            input_sum = input_sum + sum(Q_in,1);
                        else
                            input_sum = sum(Q_in,1);
                        end
                    end     
                    
                    % update idle rates for every component of T
                    idle_rate_vect = idle_rate_vect + (mapped_proc_rates - input_sum);
                    
                    % idle rate for T accumulates from idle rates of T for
                    % every existing operation mode
                    %T_idle_rate = T_idle_rate + sum(idle_rate_vect);                                        
                end
                % total idle rate accumulates from idle rates for every T
                total_idle_rate = total_idle_rate + sum(idle_rate_vect);
                
                % store idle rates for T
                if ~any(strcmp(T, keys(prob.idleRates)))
                    prob.idleRates(T) = idle_rate_vect;
                else
                    prob.idleRates(T) = prob.idleRates(T) + idle_rate_vect;
                end
            end
            
            % limiting the total idle rate
            prob.total_idle = total_idle_rate;
            milp_constraint = [milp_constraint, total_idle_rate <= val];                        
        end
        
        
        %% RELIABILITY PATTERNS
        %
        %% min_redundant_components( T, S', val )
        function [ milp_constraint ] = min_redundant_components( prob, T, S, val )
            % Components of type T with subtype S must have at least <val>
            % degree of redundancy.
            % NOTE: S is optional
            %
            % Notation: P - path matrix (a.k.a. walk indicator matrix)
            % More details about degrees of redundancy can be found in [1].
            sink = prob.Flow{end};
            try
                P = prob.allPathsLayer(T, sink);
            catch
                warning(sprintf('Patterns::min_red_degree: path matrix between %s and %s is not created', T, sink));
                return
            end
            
            if ~isempty(S)
                % if S argument is specified, put the red_degree constraint
                % only for components T of subtype S
                M     = Helpers.getMappingMatrix(prob, T);
                T_lib = prob.Library.findRelatedType(T);
                S_lib = prob.Library.Subtypes(T_lib);
                S_mapped = S_lib(S) * M;
                for i = 1:length(S_mapped)
                    milp_constraint = [milp_constraint, ...
                        implies(S_mapped(i), Patterns.atLeast(sum(P(:,i)), val), 1e-2)];
                end
            else
                s = size(P,2);
                const(1:s) = val;
                % currently this constraint is same as at_least_N_paths with
                % the exception that T2 is the sink.
                milp_constraint = Patterns.atLeast(sum(P,1), const);
            end
        end
        
        
         %% max_failprob_of_connection( T, S', val )
        function [ milp_constraint ] = max_failprob_of_connection( prob, T, S, val )
            % The maximum failure probability of a functional link, which
            % starts at sources and ends at a component of type T with
            % subtype S, must be <val>.
            %
            % This pattern can be used to constrain several functional
            % links simultaneously (by specifying the type-subtype T-S) as
            % well as a particular functional link ending at a certain
            % component of T. 
            %
            % NOTE: in current version T must be equal to the last
            % component type in the functional flow (e.g. Load)
            % NOTE2: S is optional
            milp_constraint = [];
            reliabilities = prob.estimated_reliabilities(prob.Flow{end});            
                
            if strcmp( T, prob.Flow{end} ) == 1
                % we currently consider a functional link is a link
                % connected to a sink                                
                if isempty(S)
                    milp_constraint = [milp_constraint, reliabilities(1:end) <= val];
                else
                    S_lib = prob.Library.Subtypes(prob.Flow{end});
                    S_indices = find(S_lib == 1);
                    for i = S_indices
                        milp_constraint = [milp_constraint, reliabilities(i) <= val];
                    end
                end
            else
                % name of a particular sink is provided as input
                names = prob.Library.Names(prob.Flow{end});
                T_match = strfind(names, T);
                T_index = find(not(cellfun('isempty', T_match)));
                milp_constraint = [milp_constraint, reliabilities(T_index) <= val];
            end
        end
        
        
        %% OPERATION PATTERNS
        % has_operation_mode( T )
        function [ milp_constraint ] = has_operation_mode( prob, T )
            % The system must support the operation mode T.
            % (T is a string)
            %
            % This pattern is useful for systems that process some kinds of
            % messages (e.g. processing lines, which assemble/package
            % details). T is in the form "A1x1+A2x2+...+Anxn", where x1..xn
            % are flow rates of products of type x1..xn (can be represented
            % in ArchEx using tag extensions for component types), and
            % A1..An are constants.
            %
            % Example: operation mode "1A+2B" means that the system should
            % be able to simultaneously produce product A normally and
            % product B at a double rate.
            %
            % This pattern currently just creates a structure for storing
            % information about the operation mode and returns no MILP
            % constraints.
            new_req.mode = T;
            new_req.map  = containers.Map;
            value = strsplit(T, '+');
            for product_type = prob.Tags
                product_type = char(product_type);
                l = length(product_type);
                for i = value
                    i = char(i);
                    if ~isempty(strfind(i, product_type))
                        val = i(1:length(i) - l);
                        new_req.map(product_type) = str2double(val);
                    end
                end
            end            
            prob.SetRequirement(new_req);
            
            % no MILP constraints currently exist for this pattern
            milp_constraint = [];
        end
        
        
        %% TYPE PATTERNS
        %  These patterns specify type properties of nodes or node
        %  variables. 
        function [ constraints ] = isProbability( obj )
            % The pattern ISPROBABILITY takes a parameter OBJ, a
            % matrix of YALMIP variables or a single YALMIP variable. The
            % set of constraints returned by the pattern impose
            % that variables in OBJ are probabilities (i.e., their values 
            % are in the 0-to-1 range).
            constraints = (0 <= obj <= 1);
            return;
        end
        
        
        %% MAPPING PATTERNS
        %  These patterns are used to enforce the correctness of mapping
        %  rules between "virtual" and "real" (library) components.
        function [ milp_constraint ] = at_most_one_mapping( M )
            % every virtual component can be mapped to at most one real
            % component. Input - mapping matrix M of binary decision
            % variables (M(i,j) = 1 if virtual component j is mappet to
            % library component i)
            s = size(M,2);
            const(1:s) = 1;
            milp_constraint = Patterns.atMost(sum(M,1), const);
        end
        
        
        function [ milp_constraint ] = if_used_then_has_mapping( condition, target )
            % if a virtual component is connected to some
            % other component (i.e., it is instantiated) then it must be
            % mapped to a real device from the library.
            milp_constraint = max(condition,[],1) == max(target,[],1);
        end
        
        
        %% in_flow_implies_mapping_to( T, Tag, S )
        function [ milp_constraint ] = in_flow_implies_mapping_to( prob, T, Tag, S )
            %
            
            milp_constraint = [];
            % get vectors that store mapping of each virtual component to 
            % the subtypes from the constraint input ("1" if current 
            % component is mapped to the required subtype, "0" otherwise)
            T_mapping_to_S = Helpers.getMappingToSubtype(prob, T, S);
            
            for op_mode = prob.Requirement.Operation
                op_mode = op_mode{1};
                Q_set = prob.flowRateMatrices(char(op_mode.mode));
                Q     = Q_set(Tag);                
                Q_in  = Helpers.getIncomingOutgoing(prob, Q, T);
                
                if op_mode.map(Tag) == 0
                    % otherwise we can get constraints like if 0 >= 1e-2 ..
                    continue;
                end
                
                % IF input flow (sum of a column k of Q_in) is greater than
                % zero (>= 1e-2 is used/hard coded, because strict
                % inequalities are not supported by YALMIP) THEN mapping of
                % component k must be 1.
                for k = 1:size(Q_in,2)
                    milp_constraint = [milp_constraint, ...
                        implies(sum(Q_in(:,k)) >= 1e-2, T_mapping_to_S(k) == 1, 1e-2)];
                end
            end
        end
        
        
        %% incoming_then_not_mapped_to( T, Tag, S )
        function [ milp_constraint ] = incoming_then_not_mapped_to( prob, T, Tag, S )
            %
            
            milp_constraint = [];
            % get vectors that store mapping of each virtual component to 
            % the subtypes from the constraint input ("1" if current 
            % component is mapped to the required subtype, "0" otherwise)
            T_mapping_to_S = Helpers.getMappingToSubtype(prob, T, S);
            
            for op_mode = prob.Requirement.Operation
                op_mode = op_mode{1};
                Q_set = prob.flowRateMatrices(char(op_mode.mode));
                Q     = Q_set(Tag);                
                Q_in  = Helpers.getIncomingOutgoing(prob, Q, T);
                
                % IF input flow (sum of a column k of Q_in) is greater than
                % zero (>= 1e-2 is used/hard coded, because strict
                % inequalities are not supported by YALMIP) THEN mapping of
                % component k must be 1.
                for k = 1:size(Q_in,2)
                    milp_constraint = [milp_constraint, ...
                        implies(sum(Q_in(:,k)) >= 1e-2, T_mapping_to_S(k) == 0, 1e-2)];
                end
            end
        end
        
        
        %%  Higher-level patterns 
        %   This family of patterns specify "higher-level" properties,
        %   closer to the language of the requirements.
        %%  Example: "Availability"
        function [ constraints ] = maximumRectifierFailuresAllowed(binVar, n)
            % The pattern MAXIMUMRECTIFIERFAILURESALLOWED imposes that, even
            % if N Rectifier Units fail, all the loads are still powered.
            % BINVAR is the whole set of the binary variables used in 
            % ArchEx, including the adjancency sub-matrices and their 
            % powers/products; N is the number of failures allowed.
            % For all loads there must be at least n+1 connected rectifiers
            constraints = min(binVar.RL_vector) >= n + 1;
            return;
        end    
    
        
        function res = getMILPvariables(constraint)
            % get necessary decision variables for the MILP formulation.
            % The function is used to reduce the repeating code in multiple
            % patterns.
            %
            % NOT IMPLEMENTED YET
            %
        end        
    end
end

