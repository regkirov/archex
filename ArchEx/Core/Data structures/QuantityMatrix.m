classdef QuantityMatrix < Matrix
% TODO: finilize description
%
% The matrix is organized as a multi-dimensional map (MapN object) and 
% is built from a bunch of smaller sub-matrices, which represent 
% the connectivity between components of type 1 (rows) and 
% components of type 2 (columns). The size of each sub-matrix depends 
% on the number of components of a particular type.
%
% To construct the matrix two helper structures are used:
% 1. num_components (a vector, which provides the amount of components
% of each particular type)
% 2. rules (a map that provides interconnection rules between
% components). 
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: January 2017
    
    properties (Access = private)
        matrix_backup
    end
    
    
    properties (GetAccess = public, SetAccess = private)
        %
    end
    
    
    methods (Access = public)
        function obj = QuantityMatrix(num_components, rules)
            % parent constructor
            obj@Matrix(num_components, rules);
            
            if nargin == 2
                % initialize the matrix
                obj.M = MapN;
                obj.I = containers.Map;
                obj.Size = 0;
                
                % we need the keys of the helper matrix (e.g., 'ACBus Left',
                % 'Generator Right' etc.). The adjacency matrix will have the
                % same keys (maybe not all of them).
                [rows_rules, cols_rules] = obj.getKeys(rules);
                
                % Iterate over the matrix of connection rules and construct
                % the adjacency matrix step by step.                
                for row = rows_rules
                    row = char(row);
                    % num_type1 is the number of components of type 1
                    % (these will be rows of a particular sub-matrix).
                    % For instance, for the Generator-Bus sub-matrix (GB)
                    % num_type1 will be the number of Generators (G).
                    num_type1 = num_components(row);
                    if num_type1 == 0
                        % If there are no components of this type then we
                        % do not need this sub-matrix at all in the
                        % resulting adjacency matrix, so we skip it.
                        continue;
                    end
                    for col = cols_rules
                        col = char(col);
                        % num_type2 is the number of components of type 2.
                        num_type2 = num_components(col);
                        if num_type2 == 0
                            % if there are no components of this type - skip
                            continue;
                        elseif rules(row, col) == 1
                            % If the rules say that these components can be
                            % connected (type 1 and type 2, for instance,
                            % Generators and Buses) then declare this
                            % sub-matrix as a bunch of symbolic binary decision
                            % variables (binvar)
                            bvar = sdpvar(num_type1, num_type2, 'full');
                            obj.M(row, col) = bvar;
                        elseif rules(row, col) == 0
                            % otherwise matrix entries should be marked
                            % with zeros to constrain them further on
                            %bvar = sdpvar(num_type1, num_type2, 'full');
                            %assign(bvar, zeros(num_type1, num_type2));
                            obj.M(row, col) = zeros(num_type1, num_type2);
                        end                        
                    end
                    obj.Size = obj.Size + num_type1;                                        
                end
                [obj.Row_keys, obj.Col_keys] = obj.getKeys(obj.M);  
                
                % default key ordering
                obj.Ordering = obj.Row_keys;
                
                % initialize global indices
                obj.updateGlobalIndices;
            else
                error('AdjacencyMatrix: not enough parameters');
            end
        end
        
        
        function [ milp_constraints ] = getConstraints(obj)
            % get connection rules of the adjacency matrix in the form of
            % MILP constraints. These constraints restrict all connections
            % in the graph, which were marked with "0" value during the
            % matrix initialization, i.e., connections between non-adjacent
            % layers in the functional flow (e.g. generators cannot be 
            % directly connected to loads) and backward connections (in our
            % notation components can be connected only along the flow,
            % generator G can be connected to bus B with an edge GB, while
            % edge BG must not be present)
            qntm = obj.getMatrixNumeric;
            milp_constraints = [];
            for i = 1:size(qntm,1)
                for j = 1:size(qntm,2)
                    % add constraint only if qntm(i,j) is a decision
                    % variable (sdpvar) and is marked with "0"
                    if value(qntm(i,j)) == 0 && isa(qntm(i,j), 'sdpvar')
                        milp_constraints = [milp_constraints, qntm(i,j) == 0];
                    end
                end
            end
        end                            
    end
    
    
    methods (Access = private)
        %
    end
end

