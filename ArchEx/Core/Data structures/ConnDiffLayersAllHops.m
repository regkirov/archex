classdef ConnDiffLayersAllHops < handle
    % This class represents the connectivity between different layers of
    % components within all hops (up to a specified maximum). A "layer" is
    % a matrix, describing the one-hop connectivity between two types of
    % components (types can be different or same).
    %
    % If matrix A and B represent the connectivity between components, then
    % A*B is the connectivity between the rows of A and columns of B. For
    % example if GB is the connectivity between components of type G and
    % B and BL is the connectivity between components of type B and L, then
    % GB * BL is the connectivity between G and L (through B). In such a 
    % way connections between different layer components can be determined 
    % in many problems.
    %
    % Results of multiplying two matrices of decision variables is
    % non-linear and, therefore, cannot be added to the problem as a linear
    % constraint. In this data structure we use a well-known linearization
    % technique (described below), which replaces the multiplication of two
    % variables by multiplication of each one by a scalar and their sum.
    % The cost of using this technique is introducing of an extra dimension
    % to the data structure (here - 3rd dimension). The lower level of this
    % dimension (indexed by 1) represents the multiplication results.
    %
    % Dimension 4 of this data structure represents the connectivity in
    % different number of hops. For instance, the slice indexed by 1
    % represents connectivity in 1 hop, slice 2 shows if there is a
    % connection in 2 OR 1 hop and so on.
    %
    % Authors: Dmitrii Kirov and Pierluigi Nuzzo
    % Last modified: May 2016
    
    properties (GetAccess = public, SetAccess = private)
        Matrix
        Layer1
        Layer2
    end
    
    
    methods (Access = public)
        function obj = ConnDiffLayersAllHops(layer1, layer2, max_hops)
            % here the constructor expects that layer1 and layer2 are of
            % ConnOneHop type, i.e., they represent the one hop
            % connectivity between components A-B for layer1 and B-C for
            % layer2.
            if nargin == 3
                layer1_size = size(layer1.Matrix);
                layer2_size = size(layer2.Matrix);

                if layer1_size(1) ~= layer2_size(2)
                    error('Input layers have incompatible dimensions.');
                end

                % initialize the 4-dimensional matrix of decision
                % variables.
                % Summary of dimensions (given that layer1 is the 1 hop
                % connectivity between components A and B and layer2 -
                % between components B and C):
                % [1] - components of type A
                % [2] - components of type C
                % [3] - only the slice with index "1" is of interest - it
                % represents the multiplication result between layer1 and
                % layer2
                % [4] - how many hops (1 - one hope, 2 - two hops OR one
                % hop, end - all hops up to max_hops)
                obj.Matrix = binvar(layer2_size(1), layer1_size(2),...
                    layer2_size(2) + 1, max_hops, 'full');
                obj.Layer1 = layer1.Matrix;
                obj.Layer2 = layer2.Matrix;
            else
                error('ConnDiffLayersAllHops: Not enough parameters');
            end
        end
        
        
        function [cons] = getConstraints(obj, cons)
            % get the consistency constraints that relate this data structure
            % to input structures. They represent some rules that must hold
            % in order to correctly represent the connectivity between
            % input layers within all hops.
            cons =[cons, obj.Matrix(:,:,1,1) == obj.Layer1];
            
            dimension = size(obj.Matrix);
            % The linearization trick.
            % Matrix multiplication is O(n^3), where each matrix is nxn. Therefore n
            % multiplications require O(n^4) operations.
            % 
            % Note : There are two for-loops inside because of the linearization.
            % Consider A = XY defined as follows: 
            % A(1,1) = Logical_OR(X(1,i)*Y(i,1))
            % For this we need to create n dummy variables (Bi's) such that
            % Bi <= X(1,i); Bi <= Y(1,i); Bi >= X(1,i)+Y(1,i)-1 
            % and then implement A(1,1) as the logical-OR of all variables Bi's
            for i=2:dimension(4)
                for j=1:dimension(1)
                    for k=1:dimension(2)

                        %%%%%% Implement the logical AND
                        for l=1:dimension(3)-1

                            cons =[cons, obj.Matrix(j,k,l+1,i)<=...
                                obj.Matrix(l,k,1,i-1)];
                            cons =[cons, obj.Matrix(j,k,l+1,i)<=obj.Layer2(j,l)];
                            cons =[cons, obj.Matrix(j,k,l+1,i)>=...
                                obj.Matrix(l,k,1,i-1)+obj.Layer2(j,l)-1];
                        end

                        %%%%%% Implement the logical OR
                        for l=1:dimension(3)-1
                            cons =[cons, obj.Matrix(j,k,1,i)>=obj.Matrix(j,k,l+1,i)];
                        end

                        cons =[cons, obj.Matrix(j,k,1,i)>=obj.Matrix(j,k,1,i-1)];
                        cons =[cons, obj.Matrix(j,k,1,i)<=sum(...
                            obj.Matrix(j,k,2:dimension(3),i))+...
                            obj.Matrix(j,k,1,i-1)];
                    end
                end
            end
        end
        
    end
    
end

