classdef ConnAllPaths < handle
% This data structure represent the existence of a path between
% components represented by rows and columns (possibly through other
% components of same/different types). The input to the structure
% constructor are one or two layers, which can represent either the one-hop 
% connectivity between different components or multiple-hop  connectivity 
% or the existence of paths between different components (possibly via 
% other components).
%
% If matrices A and B represent the existence of a path between
% components of type 1-2 and 2-3 respectively, then A*B shows if there
% is a path between 1 and 3. In such a way connections between different 
% layer components can be determined in many problems.
%
% Results of multiplying two matrices of decision variables is
% non-linear and, therefore, cannot be added to the problem as a linear
% constraint. In this data structure we use a well-known linearization
% technique (described below), which replaces the multiplication of two
% variables by multiplication of each one by a scalar and their sum.
% The cost of using this technique is introducing of an extra dimension
% to the data structure (here - 3rd dimension). The lower level of this
% dimension (indexed by 1) represents the multiplication results.
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: May 2016
    
    properties (GetAccess = public, SetAccess = private)
        Matrix
        Layer1
        Layer2
    end
    
    
    properties (GetAccess = public, SetAccess = private)
        Adjacent
    end
    
    
    methods (Access = public)
        function obj = ConnAllPaths(layer1, layer2)
            % The constructor accepts either one or two inputs (layer1 and
            % layer2). If there are two then they can be either ConnOneHop
            % or ConnAllPaths structures. If there is one, then it is of
            % ConnDiffLayersAllHops data structure.
            if nargin == 2
                % non-adjacent layers (e.g. RL = RD + DL)
                layer1_size = size(layer1.Matrix);
                layer2_size = size(layer2.Matrix);

                if layer1_size(2) ~= layer2_size(1)
                    error('Input layers have incompatible dimensions.');
                end

                obj.Matrix = binvar(layer1_size(1), layer2_size(2),...
                    layer1_size(2) + 1, 'full');
                obj.Layer1 = layer1.Matrix;
                obj.Layer2 = layer2.Matrix;
                obj.Adjacent = 0;
                
            elseif nargin == 1
                % adjacent layers
                layer1_size = size(layer1.Matrix);
                
                obj.Matrix = binvar(layer1_size(1), layer1_size(2), 'full');
                obj.Layer1 = layer1.Matrix;
                obj.Adjacent = 1;
                
            else
                error('ConnAllPaths: Not enough parameters');
            end
        end
        
        
        function [cons] = getConstraints(obj, cons)            
            if obj.Adjacent == 1
                % if layers are adjacent (e.g. RD)
                dimension = size(obj.Layer1);
                if length(dimension) == 4
                    cons = [cons, obj.Matrix == obj.Layer1(:,:,1,dimension(4))];
                elseif length(dimension) == 2
                    cons = [cons, obj.Matrix == obj.Layer1];
                end
                
            elseif obj.Adjacent == 0
                % if layers not adjacent (e.g. RL)
                dimension = size(obj.Matrix);
                for j=1:dimension(1)
                    for k=1:dimension(2)
                        for l=1:dimension(3)-1
                            cons =[cons, obj.Matrix(j,k,l+1)<=obj.Layer1(j,l)];
                            if length(size(obj.Layer2)) == 2
                                cons =[cons, obj.Matrix(j,k,l+1)<=obj.Layer2(l,k)];
                            else
                                cons =[cons, obj.Matrix(j,k,l+1)<=obj.Layer2(l,k,1)];
                            end
                            cons =[cons, obj.Matrix(j,k,l+1)>=obj.Layer1(j,l)+...
                                obj.Layer2(l,k)-1];
                        end
                        for l=1:dimension(3)-1
                            cons =[cons, obj.Matrix(j,k,1)>=obj.Matrix(j,k,l+1)];
                        end
                        cons =[cons, obj.Matrix(j,k,1)<=sum(...
                            obj.Matrix(j,k,2:dimension(3)))];
                    end
                end
            end
        end
        
    end
    
end

