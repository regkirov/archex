classdef Matrix < matlab.mixin.Copyable
% MATRIX: Parent class for Matrix objects in ArchEx.
% The class contains common functionalities to handle matrices. In ArchEx
% matrices are represented as MapN (multi-key dictionary) structures to
% easily fetch sub-matrices, which refer to particular component types. 
% If M is a Matrix object, then M(A,B) is a submatrix, entries of which 
% correspond to types A (rows) and B (columns).
%
% Example: E is the adjacency matrix of a graph G, which represents a power
% network with components of types G (generator), B (bus) and L (load). To
% get the information about the connectivity between buses and loads (a
% submatrix of E), one should do E(B,L). Matrix class provides dedicated
% methods that are responsible for getting its submatrices, constructing
% the full matrix from pieces, getting local and global indices of
% components.
%
% Several matrix data structures exist in ArchEx: adjacency matrix,
% incidence matrix, quantity matrices. They all have Matrix as their parent
% class.
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: January 2017
    
    properties (Access = protected)
        % matrix (MapN)
        M
        % matrix indices storage (containers.Map)
        I
    end
    
    properties (GetAccess = public, SetAccess = protected)
        % lists of matrix row and column keys (cell arrays of char)
        Row_keys
        Col_keys
        
        % matrix size
        Size
        
        % list of ordered matrix keys (useful in certain situations). User
        % is able to set any ordering. A typical one is ordering the keys
        % (which are component types + tags) according to functional flow.
        Ordering
        
        % a structure that stores number of components according to their
        % types (or types + tags) - containers.Map
        NumComponents
        
        % ConnectionRules(A,B) = 1 (0) means that components of type A can
        % (cannot) be connected to components of type B - MapN.
        ConnectionRules
    end
    
    methods (Access = public)
        function obj = Matrix(num_components, rules)
            % initialize matrix, indices and size
            obj.M = MapN;
            obj.I = containers.Map;
            obj.Size = 0;
            
            % store number of components and connection rules
            obj.NumComponents = num_components;
            obj.ConnectionRules = rules;
        end
        
        
        function [matrix, map] = getSubMatrixNumeric(obj, row, column)
            % Get a particular slice of the adjacency matrix as a numeric
            % matrix.
            % INPUT:
            % Row and column names (char) related to a particular group of
            % numeric rows/columns
            % OUTPUT:
            % 1. A numeric matrix (built from matrices corresponding to all
            % combinations of the keys requested)
            % 2. A map that associates these key combinations to particular
            % rows and columns of the matrix.                      
            matrix = [];
            map = containers.Map;
            
            if any(strcmp(obj.Row_keys, row)) && any(strcmp(obj.Col_keys, column))
                % if specified keys are exactly present in the matrix, then
                % return a particular sub-matrix (without concatenating
                % different combinations).
                matrix = obj.M(row, column);
                dims = size(matrix);
                rows = dims(1);
                cols = dims(2);                
                map(row) = [1,rows];
                map(column) = [1,cols];
            else
                % otherwise return a sub-matrix, which consists of all
                % combinations of requested keys
                [matching_keys_row, matching_keys_column] = ...
                    obj.getMatchingKeys(row, column);
                curr_idx_row = 1;            % current last row (required for mapping)
                previous_key_part1_row = ''; % for merging
                for i = matching_keys_row
                    new_row = [];
                    curr_idx_col = 1;           % current last column (for mapping)
                    previous_key_part1_col = '';% for merging
                    for j = matching_keys_column
                        % fetch new piece of the adjacency matrix
                        piece = obj.M(char(i),char(j));
                        
                        % get its dimensions
                        dims = size(piece);
                        rows_in_curr_piece = dims(1);
                        cols_in_curr_piece = dims(2);                        
                                                 
                        % mappings for rows and columns are stored in the
                        % same map, because adjacency matrices are
                        % symmetric - no need to store them separately
                        %map(char(i)) = [curr_idx_row, curr_idx_row + rows_in_curr_piece - 1];
                        curr_indices_col = [curr_idx_col, curr_idx_col + cols_in_curr_piece - 1];
                        map(char(j)) = curr_indices_col;
                        curr_idx_col = curr_idx_col + cols_in_curr_piece;
                        
                        % NOTE: AdjacencyMatrix class is expected to be used 
                        % for matrices, which keys consist of either 1 or 2
                        % words (which mean general type and some subclass;
                        % for instance "DCBus Left" is the key, "DCBus" is
                        % a component type, "Left" is a spatial class,
                        % which divides buses into left and right parts).
                        % In the mapping of keys to numeric indices we also
                        % provide mapping of a general type (e.g. for bus
                        % the mapping will consist of "DCBus Left", "DCBus
                        % Right" and "DCBus"). The latter is created by merging
                        % of indices (better to have this functionality in
                        % one place)
                        
                        % if the key consists of 2 parts, fetch the general
                        % type (which is the first word/part)
                        key_part1_col = strsplit(char(j));
                        key_part1_col = key_part1_col{1};
                        
                        % COLUMNS: merge indices to have also a mapping for general
                        % type (stored in the first word of the key)
                        if strcmp(key_part1_col, previous_key_part1_col) == 1
                            extra_indices = curr_indices_col;
                            if extra_indices(1) > merged_indices_col(2)
                                merged_indices_col(2) = extra_indices(2);
                            elseif extra_indices(1) >= merged_indices_col(1) ...
                                    && extra_indices(2) <= merged_indices_col(2)
                                % extra indices are already there
                                continue;
                            else
                                merged_indices_col(1) = extra_indices(1);
                            end
                        else
                            merged_indices_col = curr_indices_col;
                        end

                        map(key_part1_col) = merged_indices_col;                    
                        previous_key_part1_col = key_part1_col;

                        % append the piece to the row of resulting matrix
                        new_row = [new_row, piece];
                    end
                    
                    curr_indices_row = [curr_idx_row, curr_idx_row + rows_in_curr_piece - 1];
                    map(char(i)) = curr_indices_row;
                    
                    key_part1_row = strsplit(char(i));
                    key_part1_row = key_part1_row{1};
                    
                    % ROWS: merge indices to have also a mapping for general
                    % type (stored in the first word of the key)
                    if strcmp(key_part1_row, previous_key_part1_row) == 1
                        extra_indices = curr_indices_row;
                        if extra_indices(1) > merged_indices_row(2)
                            merged_indices_row(2) = extra_indices(2);
                        elseif extra_indices(1) >= merged_indices_row(1) ...
                                && extra_indices(2) <= merged_indices_row(2)
                            % extra indices are already there
                            continue;
                        else
                            merged_indices_row(1) = extra_indices(1);
                        end
                    else
                        merged_indices_row = curr_indices_row;
                    end

                    map(key_part1_row) = merged_indices_row;                    
                    previous_key_part1_row = key_part1_row;
                    
                    % append the new row to the matrix
                    matrix = [matrix;new_row];
                    curr_idx_row = curr_idx_row + rows_in_curr_piece;
                end                
            end
        end                        
        
        
        function [matrix, map] = getMatrixNumeric(obj, ordered_keys)
            % get the whole adjacency matrix in a numeric form. The
            % function also returns the mapping of row/column keys to
            % numeric indices of the matrix.
            matrix = [];
            map = containers.Map;
            if nargin == 2
                % order of keys is provided as input - use it
                ordering = ordered_keys;
            else
                % otherwise use current ordering
                ordering = obj.Ordering;
            end
            
            curr_idx_row = 1;        % current last row (required for mapping)
            previous_key_part1 = ''; % for merging
            for i = ordering
                new_row = [];
                if ~ismember(i, obj.Row_keys)
                    continue
                end
                
                curr_idx_col = 1;    % current last column (for mapping)
                for j = ordering
                    if ~ismember(j, obj.Col_keys)
                        continue
                    end
                    % fetch new piece of the adjacency matrix
                    piece = obj.M(char(i), char(j));                    
                    
                    % get its dimensions
                    dims = size(piece);
                    rows_in_curr_piece = dims(1);
                    cols_in_curr_piece = dims(2);
                    
                    % if the key consists of 2 parts, fetch the general
                    % type (which is the first word/part)
                    key_part1 = strsplit(char(j));
                    key_part1 = key_part1{1};
                    
                    % calculate new mapping and add it to the map
                    curr_indices = [curr_idx_col, curr_idx_col + cols_in_curr_piece - 1];
                    map(char(j)) = curr_indices;
                    curr_idx_col = curr_idx_col + cols_in_curr_piece;
                     
                    % merge indices to have also a mapping for general
                    % type (stored in the first word of the key)
                    if strcmp(key_part1, previous_key_part1) == 1
                        extra_indices = curr_indices;
                        if extra_indices(1) > merged_indices(2)
                            merged_indices(2) = extra_indices(2);
                        elseif extra_indices(1) > merged_indices(1) ...
                                && extra_indices(2) < merged_indices(2)
                            % extra indices are already there
                            continue;
                        else
                            merged_indices(1) = extra_indices(1);
                        end
                    else
                        merged_indices = curr_indices;
                    end
                    
                    map(key_part1) = merged_indices;                    
                    previous_key_part1 = key_part1;
                    
                    % append the piece to the row of resulting matrix
                    new_row = [new_row, piece];
                end
                % add the new row to the resulting matrix
                matrix = [matrix;new_row];
                curr_idx_row = curr_idx_row + rows_in_curr_piece;
            end
        end
        
        
        function res = getSubMatrixMap(obj, row, column)
            % get a particular slice of the adjacency matrix (e.g.,
            % Generator-Bus) as a MapN structure.
            res = MapN;
            if any(strcmp(obj.Row_keys, row)) && any(strcmp(obj.Col_keys, column))
                % if specified keys are exactly present in the matrix, then
                % return a particular sub-matrix (without concatenating
                % different combinations).
                res(row, column) = obj.M(row, column);
            else
                % otherwise return a sub-matrix, which consists of all
                % combinations of requested keys.
                [matching_keys_row, matching_keys_column] = ...
                    obj.getMatchingKeys(row, column);
                for i = matching_keys_row
                    for j = matching_keys_column
                        res(char(i),char(j)) = obj.M(char(i),char(j));
                    end
                end
            end
        end
        
        
        function res = getMatrixMap(obj)
            res = obj.M;
        end
        
        
        function setOrdering(obj, ordered_keys)
            % set new ordering of keys
            obj.Ordering = ordered_keys;
            obj.updateGlobalIndices;
        end
        
        
        function [row_keys,col_keys] = getKeys(obj, matr)
            %% ToDo: make a better interface for this
            % Helper function to get keys for rows and columns of the MapN
            % structure (which we use to represent the adjacency matrix).
            % The MapN object doesn't have a built-in method for this.
            i = 1;
            for key_pair = keys(matr)
                row_keys{i} = key_pair{1}{1};
                col_keys{i} = key_pair{1}{2};
                i = i + 1;
            end
            row_keys = unique(row_keys);
            col_keys = unique(col_keys);
        end
        
        
        function [row_keys, col_keys] = getMatchingKeys(obj, row, column)
            % return the list of all keys of the adjacency matrix, which
            % (partly) match the input. For example, if 'Bus' is in the
            % input then 'Bus Left', 'Bus Right' etc. should be returned.
            
            %% ToDo: modify interface to accept also one input (not always row AND column)
            
            % check which row keys include <row> as their part.
            indexc = strfind(obj.Row_keys, row);
            
            % get their indices
            matching_indices = find(not(cellfun('isempty', indexc)));
            
            if isempty(matching_indices)
                %error('There are no matching subsets of the adjacency matrix.');
                row_keys = [];
                col_keys = [];
                return;
            end
            
            row_keys = {};
            j = 1;
            for i = 1:length(matching_indices)
                % FIX: filter the keys that should not be there (e.g., if
                % we look for all keys containing "ACBus" we should not
                % include those who are named "LVACBus").
                new_key = obj.Row_keys{matching_indices(i)};
                new_key_parts = strsplit(new_key);
                if strcmp(new_key_parts{1}, row) == 0 ...
                        && strcmp(new_key, row) == 0
                    continue;
                end
                % return the list of matching keys generated from indices
                row_keys{j} = new_key;
                j = j + 1;
            end
            
            if strcmp(row, column) == 1
                col_keys = row_keys;
            else
                % if <row> and <column> are different then also check for
                % matching column keys
                indexc = strfind(obj.Col_keys, column);
                matching_indices = find(not(cellfun('isempty', indexc)));
                
                if isempty(matching_indices)
                    error('There are no matching subsets of the adjacency matrix.');
                end
                
                col_keys = {};
                j = 1;
                for i = 1:length(matching_indices)
                    % FIX: filter the keys that should not be there (e.g., if
                    % we look for all keys containing "ACBus" we should not
                    % include those who are named "LVACBus").
                    new_key = obj.Col_keys{matching_indices(i)};
                    new_key_parts = strsplit(new_key);
                    if strcmp(new_key_parts{1}, column) == 0 ...
                            && strcmp(new_key, column) == 0
                        continue;
                    end
                    % return the list of matching keys generated from indices
                    col_keys{j} = new_key;
                    j = j + 1;
                end
            end
        end
        
        
        function indices = getGlobalIndices(obj, key)
            % return global indices of a component in the adjacency matrix
            if any(strcmp(obj.Ordering, key))
                % if the key is present, just return corresponding indices
                indices = obj.I(key);
            else
                % otherwise merge indices for several keys (e.g. when there
                % are keys "Source A" and "Source B" and the key input is
                % "Source" (global type)
                matching_keys = obj.getMatchingKeys(key, key);
                indices = obj.getGlobalIndices(matching_keys{1});
                for i = matching_keys
                    % a bit of recursion %)
                    idx = obj.getGlobalIndices(char(i));
                    
                    %% We assume that all to-be-merged parts are one after another with no breaks (otherwise this may not work)
                    if idx(1) > indices(2)
                        indices(2) = idx(2);
                    else
                        indices(1) = idx(1);
                    end
                end
            end
        end
    end
    
    methods (Access = protected)
        function updateGlobalIndices(obj)
            % create/update global adjacency matrix indices. This function
            % is called at init and/or when key ordering is changed.           
            start_idx = 1;
            for i = 1:length(obj.Ordering)
                matrix = obj.M(obj.Ordering{i}, obj.Ordering{i});
                matrix_size = size(matrix,1);
                obj.I(obj.Ordering{i}) = [start_idx, start_idx + matrix_size - 1];
                start_idx = start_idx + matrix_size;
            end
        end
    end
    
end

