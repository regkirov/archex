classdef AdjacencyMatrix < Matrix
% This class is a container for a generic adjacency matrix for several
% graph interconnection problems (e.g., electrical power system). It is
% the primary data structure for these problems. It will be the input
% for the optimizer (it will contain the symbolic variables to be
% decided by the optimization tool) as well as its output (after the
% decision the symbolic variables will have concrete values, which is
% the optimization result).
%
% The matrix is organized as a multi-dimensional map (MapN object) and 
% is built from a bunch of smaller sub-matrices, which represent 
% the connectivity between components of type 1 (rows) and 
% components of type 2 (columns). The size of each sub-matrix depends 
% on the number of components of a particular type.
%
% To construct the matrix two helper structures are used:
% 1. num_components (a vector, which provides the amount of components
% of each particular type)
% 2. rules (a map that provides interconnection rules between
% components). 
%
% The values of the matrix are either zeros (if components cannot be
% connected a priori) or symbolic binary decision variables, which
% after the optimization are either 0 or 1.
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: January 2017
    
    properties (Access = private)
        matrix_backup
    end
    
    
    properties (GetAccess = public, SetAccess = private)
        %
    end
    
    
    methods (Access = public)
        function obj = AdjacencyMatrix(num_components, rules)
            % parent constructor
            obj@Matrix(num_components, rules);
            if nargin == 2                
                % we need the keys of the helper matrix (e.g., 'ACBus Left',
                % 'Generator Right' etc.). The adjacency matrix will have the
                % same keys (maybe not all of them).
                [rows_rules, cols_rules] = obj.getKeys(rules);
                
                % Iterate over the matrix of connection rules and construct
                % the adjacency matrix step by step.                
                for row = rows_rules
                    row = char(row);
                    % num_type1 is the number of components of type 1
                    % (these will be rows of a particular sub-matrix).
                    % For instance, for the Generator-Bus sub-matrix (GB)
                    % num_type1 will be the number of Generators (G).
                    num_type1 = num_components(row);
                    if num_type1 == 0
                        % If there are no components of this type then we
                        % do not need this sub-matrix at all in the
                        % resulting adjacency matrix, so we skip it.
                        continue;
                    end
                    for col = cols_rules
                        col = char(col);
                        % num_type2 is the number of components of type 2.
                        num_type2 = num_components(col);
                        if num_type2 == 0
                            % if there are no components of this type - skip
                            continue;
                        elseif rules(row, col) == 1
                            % If the rules say that these components can be
                            % connected (type 1 and type 2, for instance,
                            % Generators and Buses) then declare this
                            % sub-matrix as a bunch of symbolic binary decision
                            % variables (binvar)
                            bvar = binvar(num_type1, num_type2, 'full');
%                             if strcmp(row, col) == 1
%                                 % connectivity of components of the same
%                                 % type (and spatial type, e.g., "ACBus
%                                 % Left") is represented by a symmetric
%                                 % matrix.
%                                 bvar = binvar(num_type1, num_type2);
%                             else
%                                 % other connections are represented by a
%                                 % 'full' matrix (non-symmetric)
%                                 bvar = binvar(num_type1, num_type2, 'full');
%                             end
                            obj.M(row, col) = bvar;
                        elseif rules(row, col) == 0
                            % otherwise matrix entries should be marked
                            % with zeros to constrain them further on
                            bvar = binvar(num_type1, num_type2, 'full');
                            assign(bvar, zeros(num_type1, num_type2));
                            obj.M(row, col) = bvar;
                        end
                        
                    end
                    obj.Size = obj.Size + num_type1;                    
                    
                end
                [obj.Row_keys, obj.Col_keys] = obj.getKeys(obj.M);  
                
                % default key ordering
                obj.Ordering = obj.Row_keys;
                
                % initialize global indices
                obj.updateGlobalIndices;
            else
                error('AdjacencyMatrix: not enough parameters');
            end
        end
        
        
        function [ milp_constraints ] = getConstraints(obj, varargin)
            % get connection rules of the adjacency matrix in the form of
            % MILP constraints. These constraints restrict all connections
            % in the graph, which were marked with "0" value during the
            % matrix initialization, i.e., connections between non-adjacent
            % layers in the functional flow (e.g. generators cannot be 
            % directly connected to loads) and backward connections (in our
            % notation components can be connected only along the flow,
            % generator G can be connected to bus B with an edge GB, while
            % edge BG must not be present)
            if length(varargin) == 1
                full = varargin{1};
            elseif length(varargin) == 2
                full = varargin{1};
                flow = varargin{2};
            else
                warning('AdjacencyMatrix: incorrect number of parameters in getConstraints()');
                return;
            end
            
            milp_constraints = [];
            if full
                % need all possible constraints (restrict backward
                % connections etc.)
                adjm = obj.getMatrixNumeric;                
                for i = 1:size(adjm,1)
                    for j = 1:size(adjm,2)
                        if value(adjm(i,j)) == 0
                            milp_constraints = [milp_constraints, adjm(i,j) == 0];
                        end
                    end
                end
            else
                % otherwise constrain only those entries, which according
                % to the flow can be connected, but are restricted by
                % composition (template) rules
                for i = 1:length(flow)-1
                    current = flow{i};
                    next    = flow{i+1};
                    submatrix = obj.getSubMatrixNumeric(current, next);
                    for i = 1:size(submatrix,1)
                        for j = 1:size(submatrix,2)
                            if value(submatrix(i,j)) == 0
                                milp_constraints = [milp_constraints, submatrix(i,j) == 0];
                            end
                        end
                    end
                    if obj.canBeConnected(current, current)
                        submatrix = obj.getSubMatrixNumeric(current, current);
                        for i = 1:size(submatrix,1)
                            for j = 1:size(submatrix,2)
                                if value(submatrix(i,j)) == 0
                                    milp_constraints = [milp_constraints, submatrix(i,j) == 0];
                                end
                            end
                        end
                    end
                end                
            end
        end
               
        
        function Backup(obj)
            % backup the adjacency matrix (values of decision variables).
            % The matrix might be affected by some analysis, i.e. some its
            % values might be modified, because each its value is an
            % spdvar, which is a handle object. This function can be
            % called before running the analysis in order to ensure that
            % correct values (returned by the optimizer) are saved.
            matrix_values = obj.getMatrixNumeric;
            obj.matrix_backup = round(value(matrix_values));
        end
        
        
        function Restore(obj)
            % update matrix values from backup
            current_matrix = obj.getMatrixNumeric;
            assign(current_matrix, obj.matrix_backup);
        end
            
        
        function getVersionForSaving(obj)
            % create a copy of the adjacency matrix (new object), which
            % does not contain decision variables (sdpvar-s). This copy is
            % required for saving the matrix for future use (e.g. plotting
            % the graph from saved data), because YALMIP objects cannot be
            % saved.
            obj.M = copy(obj.M);
            for row = obj.Row_keys
                for col = obj.Col_keys
                    submatrix_numeric = obj.getSubMatrixNumeric(char(row), char(col));
                    obj.M(char(row),char(col)) = round(value(submatrix_numeric));
                end
            end
        end                 
    end
    
    
    methods (Access = private)
        function res = canBeConnected( obj, T1, T2 )
            % check if two component types can be connected by definition.
            % If the corresponding submatrix of the adjacency matrix has
            % only zeros, then components cannot be connected.
            [row_keys, col_keys] = ...
                obj.getMatchingKeys(T1, T2);
            for i = row_keys
                for j = col_keys
                    if obj.ConnectionRules(char(i),char(j)) == 1
                        res = 1;
                        return;
                    end
                end
            end
            res = 0;
        end
    end
end

