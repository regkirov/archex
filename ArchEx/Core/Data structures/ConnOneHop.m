classdef ConnOneHop < handle
% This class is a wrapper for the particular slice of the adjacency
% matrix, which represents all one-hop connections between components
% of particular types. It stores a sub-matrix in the form of a numeric
% matrix and a map that associates string keys with particular rows and
% columns of the matrix. This numeric matrix is not a complete slice of
% the adjacency matrix for given types, i.e., it has only components of
% the 1st type as rows and only components of the 2nd type as columns.
%
% Example for the EPS problem:
% ConnOneHop('Generator', 'ACBus', adjmatrix) will be storing a sub-matrix 
% constructed from all pieces of the adjacency matrix related to one-hop 
% connectivity of generators and AC buses (possibly including extension 
% tags, e.g. left/right). Each row will be related to a generator, each 
% column - to an AC bus.
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: December 2016
    
    properties (GetAccess = public, SetAccess = private)
        Matrix
        Map
    end
    
    
    methods (Access = public)
        function obj = ConnOneHop(type1, type2, adjmatrix)
            if nargin == 3
                % the AdjacencyMatrix class has a specific method to get a
                % numeric sub-matrix with the mapping structure.
                [obj.Matrix, obj.Map] = adjmatrix.getSubMatrixNumeric(type1, type2);
            else
                error('ConnOneHop: Not enough parametets');
            end
        end
        
        
        function [cons] = getConstraints(obj, cons)
            % So far no constraints to be added here
            for i = 1:size(obj.Matrix,1)
                for j = 1:size(obj.Matrix,2)
                    if value(obj.Matrix(i,j)) == 0
                        cons = [cons, obj.Matrix(i,j) == 0];
                    end
                end
            end
        end
        
    end
    
end

