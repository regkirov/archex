classdef (Abstract) Library < handle
% Library is an abstract class representing the generic library of
% components for a range of design exploration problems. It includes
% the data structure for storing the components data as well as methods
% for manipulating it (adding new components, deletion, getting the
% components list etc.)
%
% Common library properties/structures include:
% - Name of the library
% - Size (number of components in the library)
% - List of component types (e.g., generator, bus, rectifier...)
% - A dictionary/map for storing lists of components according to their
% type.
%
% A library class for a particular problem (e.g., electrical power
% system design or wireless network infrastructure synthesis) must
% inherit this abstract class. It can overload some of existing methods
% if needed and also it must implement abstract methods from this
% class, namely:
%
% - loadLibraryFromFile(): a problem specific parser is required. Here we 
% do not even specify, which kind of file will store the library (e.g., TXT
% or XML). It can be decided separately for each distinct problem.
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: December 2016
    
    properties (SetAccess = protected, GetAccess = protected)
        Components        
    end
    
    
    properties (SetAccess = protected, GetAccess = public)
        % for library
        Name
        ComponentTypes
        Size        
        % for components
        Names
        Costs
        Subtypes
    end
    
    
    methods (Abstract)
        loadLibraryFromFile     (obj, filename)
        createMapping           (obj)
        %addComponent            (obj, component)
        %removeComponent         (obj, name)
        %getFullComponentList    (obj)
        %getComponentListByKind  (obj, kind)
    end
    
    
    methods (Access = public)
        function obj = Library()
%             if nargin > 0
%                 obj.Name = name;
%             else
%                 error('Not enough parameters.');
%             end
            
            % storage for component objects (organized as a dictionary/map)
            obj.Components = containers.Map;
            % initial size of the library is zero
            obj.Size = 0;
            
            obj.Names                   = containers.Map;
            obj.Costs                   = containers.Map;
            obj.Subtypes                = containers.Map;
        end  
        
        
        function addComponent(obj, component)
            key = class(component);
            % if we have such component type in this library
            if ismember(key, obj.ComponentTypes)
                % then add this new component
                obj.Components(key) = [obj.Components(key), component];
                obj.Size = obj.Size + 1;
            end
        end
        
        
        function removeComponentByName(obj, name)
            % delete components with a given name from the library
            for type = obj.ComponentTypes
                group = obj.Components(char(type));
                for i = 1:length(group)
                    if strcmp(group(i).Name, name) == 1
                        group(i) = [];
                        %break
                    end
                end
                obj.Components(char(type)) = group;
            end
        end
        
        
        function removeComponentsByType(obj, type)
            % delete all components of a given type from the library
            if isKey(obj.Components. type)
                num_removed = length(obj.Components(type));
                remove(obj.Components, type);
                obj.Size = obj.Size - num_removed;
            else
                warning('The type "%s" is not present in the library. Cannot delete.', ...
                    type);
            end
        end
        
        
        function clear(obj)
            % delete everything from the library
            remove(obj.Components, obj.ComponentTypes);
            obj.Size = 0;
        end
        
        
        function component = getComponentByName(obj, name)
            % return a list of components with matching name
            for type = obj.ComponentTypes
                for item = obj.Components(char(type))
                    if strcmp(item.Name, name) == 1
                        component = item;
                        return;
                    end
                end
            end
            component = [];
        end
        
        
        function list = getComponentsByType(obj, type)
            % return a list of components with matching type
            if isKey(obj.Components, type)
                list = obj.Components(type);
            else
                error('Specified component type is not present in the library');
            end
        end
        
        
        function list = getFullComponentList(obj)
            % get all components
            list = obj.Components;
        end
        
        
        function setComponentTypes(obj, types)
            obj.ComponentTypes = types;
        end
        
        
        function res = findRelatedType(obj, type)
            % find a related component type in the library, if any
            % return [] otherwise
            lib_types = obj.ComponentTypes;
            res = [];
            for i = lib_types
                if ~isempty(strfind(type, char(i)))
                    res = char(i);
                    return;
                end
            end
        end
    end
    % public methods end
end

