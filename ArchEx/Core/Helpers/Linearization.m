classdef Linearization
% LINEARIZATION class provides helper functions, which return linear 
% formulations for certain operations (e.g., division by an integer
% decision variable, product of two binary variables etc.)
%
% Some non-linear operations, for instance min and max, are not linearized
% here as it can be handled by 3rd party preprocessing tools currently used
% with ArchEx.
%
% Author: Dmitrii Kirov
% Last modified: December 2016
    
    methods (Static)
        function [ linear_factor, cons ] = linearizeDenominator(prob, denominator)
            % this function provides a linearized version of an integer
            % denominator (Division of decision variables is nonlinear).
            % 
            % Input: 
            % - integer decision variable or sum of binary variables
            %
            % Output: 
            % - linear factor: sum of integer constants multiplied by aux 
            %   binary variables, which is a linearized version of input. 
            %   It can be seen as a linearized version of 1 / denominator.
            % - set of constraints that associate aux variables to already
            %   existing variables from the problem (i.e. adjacency matrix)
            %
            % This function can create auxiliary data structures and
            % corresponding constraints in certain cases.                      
            linear_factor = [];
            cons = [];
            
            if ~isinteger(denominator)
                % input is a sum of binary variables, which has to be
                % replaced with an auxiliary integer variable first
                int = intvar(1,1);
                
                % bound on the value of int
                cons = [cons, 0 <= int <= length(denominator)];
                
                % value of int must be equal to the value of sum of
                % denominator
                cons = [cons, int == sum(denominator)];
            end
            
            % init the aux data structure, which "unrolls" the integer
            % variable I to a vector B of binary variables. Item B(n) = 1
            % iff I = n-1.
            int_index = binvar(length(denominator), 1);
            
            for i = 0:size(int_index,1)-1
                cons = [cons, implies(int == i, int_index(i+1) == 1, 1e-2)];
            end
            
            % one and only one variable of int_index can be 1
            cons = [cons, sum(int_index) == 1];
            
            % linearization: linear_factor is replaces the 1 / denominator
            % with a sum(1/x) * B, where x are integer constants (possible
            % values of denominator) and B are binary variables, which
            % "activate" only one of these products.
            for i = 2:size(int_index,1)                
                linear_factor = [linear_factor, (1/(i-1)) * int_index(i)];
                
                % older versions
                %res{i-1} = (1/(i-1)) * current_index(i,idx);
                %res = res + (1/i-1) * current_index(i,idx);
                %res = [res, (1/(i-1)) * current_index(i,idx)];
            end
        end
        
        
        function [res, cons] = linearizeProduct(lhs, rhs)
            % linearization of product of continuous variable and binary
            % variable (lhs is continuous, rhs is binary) using the big M
            % approach
            %
            % Input:
            % - continuous variable(s) (lhs)
            % - binary variable(s) (rhs)
            %
            % Output:
            % - a vector of binvars, which, when summed, represent the 
            %   product of lhs and rhs (linearized version). These variables
            %   are auxiliary and are initialized in the function.
            % - set of constraints that associate aux variables to already
            %   existing variables from the problem (i.e. adjacency matrix)
            cons = [];
            if ~any(isa(lhs, 'sdpvar')) || ~any(isa(rhs, 'sdpvar'))
                % special case when lhs or rhs is just a double
                lhs = sum(lhs);
                res = lhs * rhs;
            else
                res = sdpvar(1, length(lhs) * length(rhs), 'full');
                idx = 1;
                big_M = 10000; % hard coded!
                for i = 1:length(lhs)
                    for j = 1:length(rhs)
                        rhs_base = getbase(rhs(j));
                        rhs_base = rhs_base(2);
                        %res(idx) = res(idx) * rhs_base;
                        
                        % sdpvar * binvar product linearization (z = A*x)
                        % (A - sdpvar, x = binvar)
                        % need to add 4 linearization constraints:
                        % 1) z <= big_M * x
                        A = lhs(i);
                        x = rhs(j)/rhs_base;
                        z = res(idx);
                        cons = [cons, z/rhs_base <= big_M * x];
                        
                        % 2) z <= A
                        cons = [cons, z/rhs_base <= A];
                        
                        % 3) z >= A - (1-x)*big_M
                        cons = [cons, z/rhs_base >= A - (1-x)*big_M];
                        
                        % 4) z >= 0;
                        cons = [cons, z/rhs_base >= 0];
                        
                        idx = idx + 1;
                    end                    
                end
            end
        end
        
        
        function [res, cons] = linearizeBinProduct(lhs, rhs)
            % linearize the product of two binary variables
            % Input: 
            % - left and right hand sides of product (lhs and rhs) - binary
            % Output: 
            % - a vector of binvars, which, when summed, represent the 
            %   product of lhs and rhs (linearized version). These variables
            %   are auxiliary and are initialized in the function.
            % - set of constraints that associate aux variables to already
            %   existing variables from the problem (i.e. adjacency matrix)
            cons = [];
            if ~any(isa(lhs, 'sdpvar')) || ~any(isa(rhs, 'sdpvar'))
                % special case when lhs or rhs is just a double
                lhs = sum(lhs);
                res = lhs * rhs;
            else
                res = binvar(1, length(lhs) * length(rhs), 'full');
                idx = 1;
                for i = 1:length(lhs)
                    lhs_base = getbase(lhs(i));
                    lhs_base = lhs_base(2);
                    for j = 1:length(rhs)                        
                        rhs_base = getbase(rhs(j));
                        rhs_base = rhs_base(2);
                        res(idx) = res(idx) * lhs_base * rhs_base;
                        
                        % binvar product linearization (X = A*B)
                        % need to add 3 linearization constraints:
                        % 1) X <= A
                        cons = [cons, res(idx)/(lhs_base*rhs_base) <= lhs(i)/lhs_base];
                        
                        % 2) X <= B
                        cons = [cons, res(idx)/(lhs_base*rhs_base) <= rhs(j)/rhs_base];
                        
                        % 3) X >= A + B - 1
                        cons = [cons, res(idx)/(lhs_base*rhs_base) >= ...
                            lhs(i)/lhs_base + rhs(j)/rhs_base - 1];
                        
                        idx = idx + 1;
                    end
                end
            end
        end
    end  
end

