function [reliability] = recReliability(failure_probabilities, ...
    source_indices, comp_index, prev_comp_index, adjacency_matrix )
% [RELIABILITY]=RECRELIABILITY(TEMPLATEPARAMETERS,PROB_COMPONENT_FAILURE, 
% COMP_INDEX, PREV_COMP_INDEX, ADJACENCY_MATRIX) computes the failure 
% probability at a critical load (or set of loads) indexed by COMP_INDEX in 
% a directed graph using a recursive algorithm to traverse the whole graph 
% (a modified depth-first search).
% 
% See [Nuzzo, et al., IEEE Access, 2014] for further details.
%
% RELIABILITY is the vector of the reliability values (at the nodes of
% interest). 
% TEMPLATEPARAMETERS includes the number and attributes of the components 
% in the template.
% ADJACENCY_MATRIX is the adjacency matrix of the architecture graph.
% PROB_COMPONENT_FAILURE is a vector of failure probabilities for all the 
% template components ordered as in COMPUTE_RELIABILITY().
% COMP_INDEX is the index set of the components at which the failure
% probability is currently computed.
% PREV_COMP_INDEX is the index set of the direct predecessors of the nodes
% in COMP_INDEX.
% 
% RECPRELIABILITY stores all the neighbor nodes of COMP_INDEX that have not
% been visited yet. Then, it generates all possible combinations of failure
% events due to such components and compute their probability, by
% multiplying the contributions due to independent components and summing
% up the contributions due to disjoint events. Whenever one (or more)
% components are healthy, RECRELIABILITY is recursively called by using
% such healthy components as current nodes. Recursion stops when either a
% healthy generator is found or all components have been visited. In the
% first case, there exists a path of healthy components from a generator to
% a critical load and, therefore, its failure probability is zero.
% Otherwise, a healthy path is found which does not include a
% generator; its contribution to the overall failure probability is then
% irrelevant and RECRELIABILITY returns the neutral element 1.
% 
% Authors: Nikunj Bajaj and Pierluigi Nuzzo and Dmitrii Kirov
% Last modified: June 2016

num_components       = size(adjacency_matrix, 1);      % Total number of components

prob_component_failure  = failure_probabilities.prob_component_failure;
prob_edge_failure       = failure_probabilities.prob_edge_failure;

%%% Local Variables
ind=[];                                                                     % Keep track of indices of components to traverse the graph at the next call

buf_checksource = zeros(num_components);
%% List of all elements to be traversed across the tree
for j=1:size(comp_index,2)
    % Vector of the elements connected to elements in the vector IND
    adjacency_matrix_vector = adjacency_matrix(comp_index(1,j),:);          % For each element of COMP_INDEX adjacency vector is to be considered

    buf_checksource(j)= isempty(find(adjacency_matrix_vector,1));        % Check whether the current component is a source

    for i=1:size(prev_comp_index,2)
        adjacency_matrix_vector(prev_comp_index(i))=0;                      % All those elements which are considerd in previous calls shall not be considered again
    end
    ind = [ind find(adjacency_matrix_vector)];                              % Update IND vector with the indices of elements to be considered in current call
end
ind =unique(ind);                                                           % Remove repetitive element indices from the vector IND
prev_comp_index=[prev_comp_index comp_index ind];                           % Update the PREV_COMPONENT_INDEX vector for the next call
prev_comp_index=unique(prev_comp_index);                                    % Remove repetitive element indices from the vector PREV_COMPONENT_INDEX


size_ind = size(ind,2);                                                     % Number of elements that will be considered in this call



%% Computation and Recursive Call
if numel(ind)==0                                                            % Case when recursion reaches the generators or APUs or a case where it cannot find any further connected nodes
      if ismember(1,buf_checksource)
        reliability=0;                                                          
      else
        reliability=1;    
      end
else
    reliability=zeros(1,2^size_ind);
    for i=1:2^size_ind                                                     % Generating 2^n cases of survival and failure of the n components as 2^n binary numbers
        bin_i = dec2bin(i-1)-48;                                           % Storing different digits of the 2^n binary numbers as an array in every iterattion.
        bin_aux = zeros(1,size(ind,2)-size(bin_i,2));                      % (i-1) is being considered since n elements can generate numbers from 0-(2^n-1). 48 is subtracted for the ascii value.
        bin_i1= [bin_aux bin_i];                                           % Appending zeros on MSB side to make all numbers consist of the same number of bits. As in binary number one is read as 00001 when 5 components are to be considered
        x=1;
        prob_component_failure_copy = prob_component_failure ;              % Copy of component failure probability to be transferred to the next function call

        for j =1:size_ind
            if bin_i1(1,j)==1
                prob_component_failure_copy(ind(j))=0;                      % If the element has not failed now its probability of failure to be considered as 0 for the next call
            else
                prob_component_failure_copy(ind(j))=1;                      % If the element has failed now its probability of failure to be considered as 1
            end
        end
        
        failure_probabilities.prob_component_failure = prob_component_failure_copy;

        for j =1:size_ind                                                   % For loop running for SIZE_IND times so as to consider all elements of IND vector

            %%% Note : Generators/ APUs are considered separately  
            %%% - if the recursion reaches other elements then further cases are
            %%% to be explored if the element itself does not fail;
            %%% - if the recursion reaches the generator or APU and it does not
            %%% fail then for sure the system has found a working path and
            %%% probability of failure then becomes 0. Please note, here
            %%% reliability is being used in the sense of "probability of
            %%% failure" and not "survival probability".

            if ismember(ind(j), source_indices)
                if bin_i1(1,j)==0
                    x= x*(prob_component_failure(ind(j))+prob_edge_failure-prob_edge_failure*prob_component_failure(ind(j)));      % Update X with the current failure probability of the series between component and switch
                else
                    x=0;
                    comp_index= [comp_index ind(j)];                                                                                   % If this element did not fail then tree has to be traversed upward through this link so COMP_INDEX is updated
                end
            else 
                if bin_i1(1,j)==0
                    x= x*(prob_component_failure(ind(j))+prob_edge_failure-prob_edge_failure*prob_component_failure(ind(j)));      % Update X with the current failure probability of the series of component and switch
                else
                    x= x*((1-prob_component_failure(ind(j)))*(1-prob_edge_failure));                                                 % Update the variable x with the current survival probability of the series of component and the switch
                    comp_index= [comp_index ind(j)];                                                                                   % If this element did not fail then tree has to be traversed upward through this link so COMP_INDEX is updated
                end
            end
        end
        if sum(bin_i1)>0                                                                                                               % If at least one element from IND does not fail (which means bin_i1>0) then the function is
            x=x*sum(recReliability(failure_probabilities, source_indices, ...
                comp_index, prev_comp_index, adjacency_matrix));     % again called recursively; otherwise the probability of all failures is multiplied
        end      

        reliability(i) = x;  % update reliability vector 
        comp_index=[];
    end
end
end
