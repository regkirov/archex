classdef Helpers
% Helpers is a static class that encompasses generic functions that can be 
% used in different problems. Every method of this class takes the problem
% object as a first argument so that it can use data from a particular
% problem.
%
% In general ArchEx helper functions can include routines for creating
% auxiliary variables/structures applicable to different problems,
% formulating some types of constraints (e.g. reliability; also possible to
% use within different problems) and querying existing data structures for
% certain variables.
%
% PROVIDED METHODS:
% - getConnectivityMatrix
% - getIncomingOutgoing
% - getMappingMatrix
% - createLibraryMapping
% - createPathMatrices
% - canBeConnectedBySubtype
% - createReliabilityConstraints
% - createTimingConstraints
% - createFlowRateMatrices
% - removeZeros
% (the list is to be updated when new methods are added; descriptions of
% each method is provided inside their implementations)
%
% NOTATION:
% E - adjacency matrix
% F - functional flow
% C - connectivity matrix between two component types (i.e., submatrix
%     of the adjacency matrix E related to these types)
% M - mapping matrix (rows - "real" components from library, columns -
%     "virtual" components of type T)
% T - type of a component
% S - subtype of a component
%
% We write "T1 >(<) T2" if T1 is the preceding (succeeding) component 
% type in F.
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: December 2016
    
    methods (Static)
        function C = getConnectivityMatrix( prob, T1, T2 )
            % get the submatrix C of E related to the connectivity between
            % components of types T1 and T2. Following cases are possible:
            % 
            % T1 > T2: rows of C represent inputs to T2, columns - outputs
            % from T1
            % T1 < T2: rows of C represent inputs to T1, columns - outputs
            % from T2
            % T1 = T2 (T1 and T2 are same type T): rows of C represent
            % outputs from T, columns - inputs to T
            %
            % Matrix C is used in MILP constraints on number of connections
            % (incoming or outgoing) between T1 and T2. If T1 >(<) T2 then
            % C can be used to express "There must be at_least|at_most|exactly
            % N outgoing (incoming) connections from (to) T1 to (from) T2".
            C = prob.AdjacencyMatrix.getSubMatrixNumeric(T1, T2);
            if strcmp(T1,T2) ~= 1
                %% NOTE: for T1=T2 we currently support only incoming connections (matrix is not transposed)
                C = C';
            end
            if ~any(isnan(value(C)))
                C = prob.AdjacencyMatrix.getSubMatrixNumeric(T2, T1);
            end
        end

        
        function [ C_in, C_out ] = getIncomingOutgoing( prob, A, T, T_in, T_out )
            % get a submatrix of A representing incoming connections/quantities 
            % to T (C_in) and a submatrix representing outgoing connections 
            % or quantities from T (C_out). A can be adjacency matrix, flow
            % rate matrix or other.
            %
            % T must be a single component type (e.g., "AC Bus"), while
            % T_in and T_out can be combinations of several types separated
            % by "|" (e.g., "Generator|AC Bus"). Adjacency matrix E is
            % queried for each of these types and C_in/C_out is combined
            % from pieces.
            %
            % If arguments T_in and T_out are not provided, C_in and C_out
            % will be submatrices representing, respectively, ALL incoming
            % and outgoing connections/quantities to/from T. In both C_in
            % and C_out columns represent T, rows - T_in and T_out.                        
            if nargin == 5
                C_in  = [];
                C_out = [];
                for in = strsplit(T_in, '|')
                    in = strtrim(in);
                    m = A.getSubMatrixNumeric(T, char(in));
                    if strcmp(T, char(in)) ~= 1
                        % transpose only if T ~= T_in (otherwise columns of m
                        % will mistakenly represent OUTGOING connections from
                        % T)
                        m = m';
                    end
                    if ~any(isnan(value(m)))
                        m = A.getSubMatrixNumeric(char(in), T);
                    end
                    C_in = [C_in; m];
                end

                for out = strsplit(T_out, '|')
                    out = strtrim(out);
                    m = A.getSubMatrixNumeric(char(out), T);
                    if strcmp(T, char(out)) == 1
                        % transpose only if T = T_out (otherwise columns of m
                        % will mistakenly represent INCOMING connections from
                        % T)
                        m = m';
                    end
                    if ~any(isnan(value(m)))
                        m = A.getSubMatrixNumeric(T, char(out))';
                    end
                    C_out = [C_out; m]; % not sure, maybe ','
                end
            elseif nargin == 3
                C_in  = [];
                C_out = [];
                T_flow = strsplit(T);
                T_flow = T_flow{1};
                T_idx = find(ismember(prob.Flow, T_flow));
                if T_idx ~= 1
                    % this is not the first type in F
                    T_prev = prob.Flow{T_idx-1};
                    C_in = A.getSubMatrixNumeric(T_prev, T);
                    if Helpers.canBeConnected(prob, T, T)
                        C_in = [C_in; A.getSubMatrixNumeric(T, T)];
                    end
                end
                if T_idx ~= length(prob.Flow)
                    % this is not the last type in F
                    T_next = prob.Flow{T_idx+1};
                    C_out = A.getSubMatrixNumeric(T, T_next)';
                    if Helpers.canBeConnected(prob, T, T)
                        C_out = [C_out; A.getSubMatrixNumeric(T, T)'];
                    end
                end               
            else
                error('Helpers::getIncomingOutgoing: Incorrect number of arguments.');
            end
        end
        
        
        function M = getMappingMatrix( prob, varargin )
            % get the mapping matrix for specified input(s). Function can
            % have 3 possible inputs in varargin:
            %
            % 1) Type T (possibly with extension tag): function returns the
            % mapping matrix for T (e.g. "Generator" or "ACBus Left");
            % 2) Types T1 and T2: if T1 > T2, return mapping matrix for T1,
            % otherwise for T2;
            % 3) List of types T1..Tn (cell, single argument): function 
            % returns the mapping matrix composed from matrices for each of
            % T (type for T1..Tn must be same, tags can be different,
            % e.g. "Generator Left" and "Generator Right")
            nVarargs = length(varargin);
            if nVarargs == 1
                if isa(varargin{1}, 'cell')
                    % case 3
                    T_list = varargin{1};
                    % separate component type from tag (e.g., we need
                    % "Generator" instead of "Generator Left" to get a proper
                    % submatrix from adjacency matrix).
                    T_general = strsplit(strtrim(T_list{1}));                
                    T_general = T_general{1};               

                    % get a submatrix related to 2 types (e.g. Generator and
                    % Load) and find the columns, which refer to the types
                    % specified in the power constraint (e.g. which rows for
                    % Generator refer to Generator Left etc.)
                    [matrix, mapping_indices] = ...
                        prob.AdjacencyMatrix.getSubMatrixNumeric(T_general, T_general);
                    T_indices = [];                
                    for i = T_list
                        i = strtrim(i);
                        if length(strsplit(char(i))) > 1
                            % if current type from the constraint has a spatial
                            % class (e.g. "Generator Left")
                            T_indices = [T_indices, mapping_indices(char(i))];
                        end
                    end              

                    % Get mapping matrices for general types (e.g. generators).
                    % Mapping matrices are those which map virtual components
                    % from the problem to real components from the library.
                    general_mapping = prob.LibraryMapping(T_general);

                    % get slices of these mapping matrices related to types
                    % specified in the power constraint (e.g. part of the
                    % mapping matrix which maps ONLY left generators to real
                    % generators from the library)
                    M = [];
                    for i = 1:length(T_indices)/2
                        M = [M, general_mapping(:, T_indices(2*i-1):T_indices(2*i))];
                    end

                    % use general mapping, if no spatial classes are used (i.e.
                    % we are operating only with component types)
                    if isempty(M)
                        M = general_mapping;
                    end
                else
                    % case 1
                    T = varargin{1};
                    M = prob.LibraryMapping(T);
                end
            elseif nVarargs == 2
                % case 2
                T1 = varargin{1};
                T2 = varargin{2};
                matr = prob.AdjacencyMatrix.getSubMatrixNumeric(T1,T2);
                if any(isnan(value(matr)))
                    M = prob.LibraryMapping(T1);
                else
                    M = prob.LibraryMapping(T2);
                end
            end
        end
        
        
        function [lib_mapping, cons] = createLibraryMapping( prob )
            %% Mapping of "virtual" components to library components.
            % Define "mapping layers" for each component type. These are 
            % matrices, looking at which one can conclude if the particular 
            % "virtual" component is instantiated. It is related to parts of
            % the adjacency matrix. If the component has a connection to
            % a component of preceding and/or subsequent type (in the flow)
            % then it is instantiated (used in the solution). Such
            % components must be mapped to library components (real).
            mapping_layer = containers.Map;
            
            % create mapping matrices from virtual components (columns) to
            % real (rows). These matrices also consist of decision
            % variables. Additional constraints to link these variables to
            % those previously defined for virtual components are created
            % inside the createMapping function of the library and are added
            % to the list of problem constraints.
            lib_mapping = containers.Map;
            
            cons = [];
            for i = 1:length(prob.Flow)
                % check if current component type is variable (number of
                % components is not fixed and can be decided by the optimizer)
                % Fixed components are mapped separately 1-to-1.
                current = prob.Flow{i};
                last    = prob.Flow{end};
                is_variable_component = 0;
                for j = prob.components_variable
                   j = char(j);
                   if ~isempty(strfind(current, j))
                       is_variable_component = 1;
                       library_type_of_component = j;
                   end
                end
                
                if is_variable_component && length(prob.Flow) > 1
                    if i == 1
                        next = prob.Flow{i+1};
                        mapping_layer(current) = ...
                            prob.AdjacencyMatrix.getSubMatrixNumeric(current, next);
                    elseif i == length(prob.Flow)
                        previous = prob.Flow{i-1};
                        mapping_layer(current) = ...
                            prob.AdjacencyMatrix.getSubMatrixNumeric(previous, last);
                    else
                        previous = prob.Flow{i-1};
                        next     = prob.Flow{i+1};
                        part1    = prob.AdjacencyMatrix.getSubMatrixNumeric(previous, current);
                        part2    = prob.AdjacencyMatrix.getSubMatrixNumeric(current, next);
                        mapping_layer(current) = [part1',part2];
                    end
                    
%                     if Helpers.canBeConnected(prob, current, current)
%                         % add incoming/outgoing horizontal connections to
%                         % the mapping layer if applicable
%                         horizontal = prob.AdjacencyMatrix.getSubMatrixNumeric(current, current);
%                         %mapping_layer(current) = [mapping_layer(current), horizontal, horizontal'];
%                         mapping_layer(current) = [mapping_layer(current), horizontal];
%                     end
                    
                    [lib_mapping(current), mapping_constraints] = ...
                        prob.Library.createMapping(library_type_of_component, ...
                        mapping_layer(current));
                    cons = [cons, mapping_constraints];
%                     prob.addConstraint(cons);
                else
                    % mapping of fixed components
                    % This procedure is slightly different because this mapping is
                    % pre-defined (not an optimizer decision). It can be seen as a
                    % one-to-one mapping.
                    % The one-to-one mapping is just an identity matrix.
                    % First, get its size (how many "virtual" components of this
                    % type are there)
                    submatrix = prob.AdjacencyMatrix.getSubMatrixNumeric(current, current);
                    num_virtual_components = size(submatrix, 1);
                    num_library_components = length(prob.Library.getComponentsByType(current));
                    % then create the identity matrix of corresponding size
                    lib_mapping(current) = eye(num_library_components,...
                        num_virtual_components);
                end
            end                                                         
            
            % parts of mapping layer
            for i = 1:length(prob.Flow)
                current = prob.Flow{i};
                [matrix, indices] = prob.AdjacencyMatrix.getSubMatrixNumeric(current, current);
                current_component_mapping = lib_mapping(current);
                for key = keys(indices)
                    current_idx = indices(char(key));                    
                    lib_mapping(char(key)) = current_component_mapping(:, current_idx(1):current_idx(2));
                end
            end
        end
        
        
        function [ milp_constraints ] = createPathMatrices( prob )
            % create auxiliary data structures for reasoning about paths
            % (i.e., connOneHop, connDiffLayersAllHops and connAllPaths).
            % They consist of binary decision variables. This function
            % creates all necessary structures according to a particular
            % problem (e.g. its functional flow etc.) and returns a set of
            % MILP constraints, which relate these variables to primary
            % decision variables from the adjacency matrix and ensure that
            % they are consistent. Each aux data structure class has a
            % getConstraints() method, which defines these constraints and
            % we can add them to the list.           
            % The fact that each sdpvar/binvar variable is a handle object 
            % is actively leveraged (each change to a binvar will impact all 
            % places where it used).  
            str = sprintf('%s: Creating auxiliary data structures and variables (path)...', class(prob));
            disp(str);
            
            prob.oneHopLayer     = MapN;
            prob.allHopsLayer    = MapN;
            prob.allPathsLayer   = MapN;
            milp_constraints     = [];
            milp = [];
            
            % oneHop data structures: connectivity in one hop either 
            % between neighboring component types (according to the flow) or
            % between components of the same type (if horizontal connections 
            % are allowed)
            for i = 1:length(prob.Flow)-1
                current = prob.Flow{i};
                next    = prob.Flow{i+1};
                if Helpers.canBeConnected(prob, current, next)
                    prob.oneHopLayer(current, next) = ...
                        ConnOneHop(current, next, prob.AdjacencyMatrix);
%                     new_layer = prob.oneHopLayer(current, next);
%                     milp = new_layer.getConstraints(milp);  
%                     prob.addConstraint(milp);
                end
                
                if Helpers.canBeConnected(prob, current, current)
                    % if components of current type can be connected also
                    % horizontally (e.g., buses)
                    prob.oneHopLayer(current, current) = ...
                        ConnOneHop(current, current, prob.AdjacencyMatrix);
%                     new_layer = prob.oneHopLayer(current, current);
%                     milp = new_layer.getConstraints(milp);  
%                     prob.addConstraint(milp);
                end
            end                                                           
            
            % diffLayersAllHops structures: connectivity between neighboring 
            % component types (according to the flow) in all hops (up to
            % maximum number of hops, which is typically equal to the maximum
            % number of components on a particular layer).
            %
            % The loop starts from sink. A new structure is created only
            % if components of preceding type (in the flow) can be
            % connected horizontally. That is, diffLayersAllHops structures
            % are created from oneHop structure for current and preceding
            % component types connectivity and oneHop structure for
            % horizontal connectivity between preceding type components.
            %
            % !!NOTE: pay attention to the order of input layers. For
            % instance, for DDL_layer_new first input should be DDL_layer,
            % otherwise there will be incorrect results).
            
            % go up from sink to source
            for i = length(prob.Flow):-1:2
                current  = prob.Flow{i};
                previous = prob.Flow{i-1};
                if Helpers.canBeConnected(prob, previous, previous)
                    % get max number of hops as a number of buses
                    bus2bus_matrix = prob.oneHopLayer(previous, previous);
                    max_hops = size(bus2bus_matrix.Matrix,1);
                    prob.allHopsLayer(previous, current) = ...
                        ConnDiffLayersAllHops(prob.oneHopLayer(previous, current),  ...
                                              prob.oneHopLayer(previous, previous),...
                                              max_hops);
                    
                    % add constraints related to the new layer
                    new_layer = prob.allHopsLayer(previous, current);
                    milp_constraints = ...
                        new_layer.getConstraints(milp_constraints);
                end
            end                       
            
            % allPaths structures: connectivity between components of
            % different types, i.e., presence/absence of a path (possibly
            % through other components) between components A and B.
            %
            % These structures are created in a cascade manner starting
            % from the sink. This generic implementation ensures that all
            % required all-paths structures are created and added to the
            % problem (creating structures from each to each is redundant
            % as some of them are not needed; this will introduce a heavy
            % performance overhead when solving the problem).
            %
            % The main purpose is to have allPaths structures from all
            % component types to the sink (e.g., DCBus to Load, Rectifier
            % to Load, Generator to Load etc.) Next allPaths structure
            % typically takes previously created allPaths as one of the
            % arguments, i.e., a cascade way is used.
            for i = length(prob.Flow):-1:2
                current  = prob.Flow{i};
                previous = prob.Flow{i-1};
                last     = prob.Flow{end};
                if i == length(prob.Flow)
                    % this is first pair of components (special case)
                    if Helpers.canBeConnected(prob, previous, previous)                   
                        % if preceding layer has horizontal connectivity,
                        % then use allHops layer
                        prob.allPathsLayer(previous, current) = ...
                            ConnAllPaths(prob.allHopsLayer(previous, current));
                    else
                        % otherwise all paths matrix is equal to one hop
                        % connectivity between components
                        prob.allPathsLayer(previous, current) = ...
                            ConnAllPaths(prob.oneHopLayer(previous, current));
                    end
                    
                    % add constraints
                    new_layer = prob.allPathsLayer(previous, current);
                    milp_constraints = ...
                        new_layer.getConstraints(milp_constraints);
                else
                    if Helpers.canBeConnected(prob, previous, previous)
                        
                        % all paths between components of current and
                        % preceding types (created from allHops)
                        prob.allPathsLayer(previous, current) = ...
                            ConnAllPaths(prob.allHopsLayer(previous, current));
                        
                        % don't forget these constraints!
                        extra_layer = prob.allPathsLayer(previous, current);                        
                        milp_constraints = ...
                            extra_layer.getConstraints(milp_constraints);
                        
                        % merge allPaths(preceding, current) with
                        % allPaths(current, sink)
                        prob.allPathsLayer(previous, last) = ...
                            ConnAllPaths(prob.allPathsLayer(previous, current), ...
                                         prob.allPathsLayer(current, last));

                    else
                        % create new allPaths from oneHop(preceding,
                        % current) and allPaths(current, sink)
                        prob.allPathsLayer(previous, last) = ...
                            ConnAllPaths(prob.oneHopLayer(previous, current), ...
                                         prob.allPathsLayer(current, last));
                    end
                    
                    % add constraints
                    new_layer = prob.allPathsLayer(previous, last);
                    milp_constraints = ...
                        new_layer.getConstraints(milp_constraints);
                end
            end             
        end
        
        
        function res = canBeConnected( prob, T1, T2 )
            % check if two component types can be connected by definition.
            % If the corresponding submatrix of the adjacency matrix has
            % only zeros, then components cannot be connected.
            [row_keys, col_keys] = ...
                prob.AdjacencyMatrix.getMatchingKeys(T1, T2);
            for i = row_keys
                for j = col_keys
                    if prob.ConnectionRules(char(i),char(j)) == 1
                        res = 1;
                        return;
                    end
                end
            end
            res = 0;
        end
        
        
        function res = canBeConnectedBySubtype( prob, idx1, idx2 )
            % check if two input components can be connected by subtype (i.e.
            % there are no restrictions of connecting these components
            % because they are mapped to lib components that have
            % incompatible subtypes). Function checks if inputs are compatible
            % by analyzing their mappings and looking them up in the
            % restrictions list stored in the problem object.
            %
            % Inputs (currently) are indices in the overall adjacency
            % matrix (not a submatrix), so before calling this function one
            % has to map local submatrix indices to global ones.
            %
            % Output is "1" if components can be connected, "0" otherwise.
            res = 1;
            if isempty(keys(prob.restrictions))
                % return immediately if there are no stored restrictions               
                return;
            end           
            T1 = idx1;
            T2 = idx2;
            [m,index_map] = prob.AdjacencyMatrix.getMatrixNumeric;
            
            % if inputs are numerical indices from the adjacency matrix,
            % associate them with corresponding component types and spatial
            % classes by reverse-mapping of indices to chars
            if isa(idx1, 'double') && isa(idx2, 'double')                
                for key = keys(index_map)
                    % check indices for each component type/spatial class
                    % to find the type/class where the inputs belong
                    current_indices = index_map(char(key));
                    if (idx1 >= current_indices(1)) && (idx1 <= current_indices(2))
                        T1 = char(key);
                        % map global (input) index to a local for current
                        % type submatrix (e.g. if DCBus Left in the full
                        % matrix has index 25 but it's the 2nd bus out of 4
                        % then we need to get local index 2)
                        idx1_local = idx1 - current_indices(1) + 1;
                    end
                    if (idx2 >= current_indices(1)) && (idx2 <= current_indices(2))
                        T2 = char(key);
                        idx2_local = idx2 - current_indices(1) + 1;
                    end
                end
            end
            
            % check obj.restrictions structure for any restrictions related
            % to input types and their current library mappings. If a
            % restriction is found, immediately return "0" and terminate
            if isKey(prob.restrictions, T1) && isKey(prob.restrictions, T2)                                                                       
                curr_type = prob.restrictions(T1);
                if isKey(curr_type, T2)
                    curr_restrictions = curr_type(T2);
                    for restriction = curr_restrictions
                        mapping1 = round(value(restriction{1}));
                        mapping2 = round(value(restriction{2}));
                        if (mapping1(idx1_local) && mapping2(idx2_local))...
                                || (mapping1(idx2_local) && mapping2(idx1_local))
                            % both inputs are mapped to types/subtypes
                            % that have a restriction and cannot be
                            % connected
                            res = 0;
                            return;
                        end
                    end
                end                
            end
            
            % separate component type from spacial type (e.g., we need
            % "Generator" instead of "Generator Left" to get a proper
            % submatrix from adjacency matrix).
            T1_general = strsplit(T1); 
            T1_general = T1_general{1}; 
            T2_general = strsplit(T2); 
            T2_general = T2_general{1};                         
            
            % map global indices to local indices for general type (e.g.
            % index of "DCBus Left" (global) should be mapped to index in
            % the DCBus-DCBus submatrix (local))
            T1_general_indices = index_map(T1_general);
            T2_general_indices = index_map(T2_general);
            idx1_general = idx1 - T1_general_indices(1) + 1;
            idx2_general = idx2 - T2_general_indices(1) + 1;
            
            % check if there are restrictions for general types (e.g. if we
            % analyze connectivity between DCBus Left and DCBus Left then a
            % constraint regarding DCBus Left might be absent, but there
            % might be a more general one related to all DC buses)
            if isKey(prob.restrictions, T1_general) && isKey(prob.restrictions, T2_general)                                                                       
                curr_type = prob.restrictions(T1_general);
                if isKey(curr_type, T2_general)
                    curr_restrictions = curr_type(T2_general);
                    for i = 1:length(curr_restrictions)
                        restriction = curr_restrictions{i};
                        mapping1 = round(value(restriction{1}));
                        mapping2 = round(value(restriction{2}));
                        if (mapping1(idx1_general) && mapping2(idx2_general))...
                                || (mapping1(idx2_general) && mapping2(idx1_general))
                            % both inputs are mapped to types/subtypes
                            % that have a restriction and cannot be
                            % connected
                            res = 0;
                            return;
                        end
                    end
                end                
            end
            
            %% ToDo: if inputs are chars (component types) - NOT YET IMPLEMENTED
            %
            % ...
        end
        
        
        function [ milp_constraints ] = createReliabilityConstraints( prob )
            % formulate reliability constraints for the problem and add
            % them to the list of auxiliary constraints
            prob.numberOfPaths              = containers.Map;
            prob.approximate_reliabilities  = containers.Map;
            prob.numberOfPaths_index        = containers.Map;
            prob.estimated_reliabilities    = MapN;            
            milp_constraints                = [];
            
            str = sprintf('%s: Creating auxiliary data structures and variables (reliability)...', class(prob));
            disp(str);
            
            for i = 1:length(prob.Flow)-1
                current = prob.Flow{i};
                sink = prob.Flow{end};
                all_paths = prob.allPathsLayer(current, sink);                                                
                
                % g/pg, b/pb and so on
                num_components = size(all_paths.Matrix,1);
                num_sinks = size(all_paths.Matrix,2);
                powers = (1:num_components);
                
                % currently is is assumed that all components of the same
                % type have the same failure probability (though in the
                % library it is possible to have different values). We just
                % take the first value from the list of failure prob-s.
                fprobs = prob.Library.FailureProbabilities(current);
                prob_component_failure = fprobs(1);
                
                % approximate reliabilities for components of current type
                % (can be 1, p, 2p^2, 3p^3...). For each component one of
                % these values will be used depending on its degree of
                % redundancy.
                prob.approximate_reliabilities(current) = ...
                    [1 powers].*[1 prob_component_failure.^powers];
                
                %% define decision variables for reliability
                % numberOfPaths(T)(1,j) = k if exactly k components of type
                % T are connected by at least one path to jth sink
                prob.numberOfPaths(current) = intvar(1, num_sinks);
                
                % numberOfPaths_index(T)(i,j) = 1 if exactly i-1 components
                % of type T are connected by at least one path to jth sink;
                % else numberOfPaths_index(T)(i,j) = 0;
                prob.numberOfPaths_index(current) = ...
                    binvar(num_components + 1, num_sinks, 'full');
                
                % Auxiliary variables for the different contributions to the failure 
                % probability of each sink due to different component types
                prob.estimated_reliabilities(prob.Flow{i}, prob.Flow{end}) = ...
                    sdpvar(1, num_sinks);
                
                % CONSTRAINTS
                % bounds for numberOfPaths (minimum - zero paths, maximum -
                % equal to max number of components of current type)
                milp_constraints = [milp_constraints, ...
                    0 <= prob.numberOfPaths(current) <= num_components];
                
                % bounds for estimated failure prob-s (between 0 and 1)
                milp_constraints = [milp_constraints, ...
                    Patterns.isProbability(prob.estimated_reliabilities(current, sink))];
                
                % numberOfPaths vector is related to allPaths structure for
                % current component and sink
                milp_constraints = [milp_constraints, ...
                    prob.numberOfPaths(current) == sum(all_paths.Matrix(:,:,1),1)];
                
                % numberOfPaths_index(T)(i,j) = 1 if exactly i-1 components
                % of type T are connected by at least one path to jth sink;
                % else numberOfPaths_index(T)(i,j) = 0;
                current_num_paths = prob.numberOfPaths(current);
                current_index = prob.numberOfPaths_index(current);
                for j = 1:num_sinks
                    for k = 0:num_components                        
                        milp_constraints = [milp_constraints, ...
                            implies(current_num_paths(j) == k, ...
                            current_index(k+1,j) == 1, 1e-2)];
                    end
                end
                
                % consistency of numberOfPaths_index(T): each column must
                % have single 1, rest - zeros.
                milp_constraints = [milp_constraints, ...
                    sum(prob.numberOfPaths_index(current)) == ones(1, num_sinks)];
                
                % estimated failure probablities are obtained from
                % multiplying approximate reliabilities vector and
                % numberOfPaths_index matrix
                milp_constraints = [milp_constraints, ...
                    prob.estimated_reliabilities(current, sink) == ...
                    prob.approximate_reliabilities(current) * ...
                    prob.numberOfPaths_index(current)];                                                                
            end
            
            % final reliability constraint as a sum of contribututions of
            % all components in the flow
            prob.estimated_reliabilities(prob.Flow{end}) = sdpvar(1, num_sinks);
            milp_constraints = [milp_constraints, ...
                    Patterns.isProbability(prob.estimated_reliabilities(prob.Flow{end}))];
                
            for i = 1:num_sinks
                reliability_at_sink = 0;
                for j = 1:length(prob.Flow)-1
                    reliability_current_sink = ...
                        prob.estimated_reliabilities(prob.Flow{j}, prob.Flow{end});
                    reliability_at_sink = reliability_at_sink + ...
                        reliability_current_sink(i);
                end
                
                est_rel = prob.estimated_reliabilities(prob.Flow{end});
                milp_constraints = [milp_constraints, est_rel(i) == reliability_at_sink];                              
            end
        end
        
        
        function [ milp_constraints ] = createTimingConstraints( prob )
            %
        end
        
        
        function [ milp_constraints ] = createFlowRateMatrices( prob )
            str = sprintf('%s: Creating auxiliary data structures and variables (flow)...', class(prob));
            disp(str);
            milp_constraints = [];
            prob.flowRateMatrices = containers.Map;
            prob.inputRates       = containers.Map;
            prob.procRates        = containers.Map;
            prob.idleRates        = containers.Map;
            E = prob.AdjacencyMatrix;
            
            if isempty(prob.Requirement.Operation)
                % compose a default operation mode, i.e., input flow rate
                % from every source has the value taken from the library for
                % corresponding source type
                default.map = containers.Map;
                default.mode = '';
                for tag = prob.Tags
                    default.map(char(tag)) = 1;
                    if isempty(default.mode)
                        default.mode = [default.mode, '1', char(tag)];
                    else
                        default.mode = [default.mode, '+1', char(tag)];
                    end
                end
                %prob.Requirement.Operation{1} = default;
                prob.SetRequirement(default);
            end                          
            
            % create a flow rate matrix for each op mode and each tag 
            for req = prob.Requirement.Operation
                req = req{1};
                op_mode = req.map;
                Q_set = containers.Map;
                
                for tag = prob.Tags
                    tag = char(tag);
                    % create a new flow matrix using the QuantityMatrix class
                    Q = QuantityMatrix(prob.NumComponents, ...
                        prob.ConnectionRules);

                    % set ordering to be same to the adjacency matrix
                    Q.setOrdering(E.Ordering);

                    % add basic quantity matrix constraints to output
                    %% seems useless
                    %milp_constraints = [milp_constraints, Q.getConstraints];

                    for j = 1:length(prob.Flow)-1 
                        curr_type = prob.Flow{j};
                        next_type = prob.Flow{j+1};                    

                        if j == 1
                            sink = prob.Flow{end};
                            source_to_next = Q.getSubMatrixNumeric(curr_type, next_type);                        
                            C = E.getSubMatrixNumeric(curr_type, next_type);
                            input_rates = prob.Library.InputFlowRates(prob.Flow{j});    
                            
                            curr_source = sprintf('%s %s', curr_type, char(tag));                              
                            source_global_idx      = Q.getGlobalIndices(curr_type);  
                            curr_source_global_idx = Q.getGlobalIndices(curr_source);  
                            total_curr_rate = 0;
                            for k = source_global_idx(1):source_global_idx(2)
                                if any(k == curr_source_global_idx(1):curr_source_global_idx(2))
                                    rate = input_rates(k);
                                    coef = op_mode(char(tag));
                                    rate = rate * coef;
                                    s = size(source_to_next,2);
                                    rate_vect(1:s) = rate;
                                    total_curr_rate = total_curr_rate + rate;
                                else
                                    rate_vect = zeros(1,size(C,2));
                                end
                                % NOTE: we assume that a source can be
                                % connected to at most one conveyor
                                milp_constraints = [milp_constraints, ...
                                    source_to_next(k,:) == rate_vect .* C(k,:)];
                            end

                            % sinks must accept only dedicated product type
                            curr_sink    = sprintf('%s %s', sink, char(tag));
                            last_to_sink = Q.getSubMatrixNumeric(prob.Flow{end-1}, curr_sink);
                            milp_constraints = [milp_constraints, ...
                                sum(sum(last_to_sink)) == total_curr_rate];
                                                        
                        else
                            curr_to_next = [];
                            C = [];
                            if Helpers.canBeConnected(prob, curr_type, curr_type)
                                f_sub = Q.getSubMatrixNumeric(curr_type, curr_type);
                                c_sub = E.getSubMatrixNumeric(curr_type, curr_type);
                                curr_to_next = [curr_to_next, f_sub];
                                C = [C, c_sub];
                            end
                            f_sub = Q.getSubMatrixNumeric(curr_type, next_type);
                            c_sub = E.getSubMatrixNumeric(curr_type, next_type);
                            curr_to_next = [curr_to_next, f_sub];
                            C = [C, c_sub];

                            for k = 1:size(curr_to_next,1)
                                for l = 1:size(curr_to_next,2)
                                    % product of continuous and bin variables
                                    % is nonlinear, we use linearization.
                                    [product, cons] = ...
                                        Linearization.linearizeProduct(curr_to_next(k,l), C(k,l));
                                    milp_constraints = [milp_constraints, cons];

                                    % update the matrix entry with new linearized version                               
                                    milp_constraints = [milp_constraints, ...
                                        curr_to_next(k,l) == product];
                                end
                            end                        
                        end
                    end
                    
                    Q_set(tag) = Q;
                                        
                    flow_numeric = Q.getMatrixNumeric;
                    if op_mode(char(tag)) > 0
                        % lower bound for all flow values (must be >= 0)
                        milp_constraints = [milp_constraints, flow_numeric >= 0];
                        % upper bound (to avoid lousy big-M relaxation)
                        milp_constraints = [milp_constraints, flow_numeric <= total_curr_rate];
                    else
                        % flow of current product type in current op_mode
                        % is zero (all values of Q must be zero)
                        milp_constraints = [milp_constraints, flow_numeric == 0];
                    end
                end
                
                prob.flowRateMatrices(req.mode) = Q_set;                                               
            end
        end                
        
        
        function TS = getMappingToSubtype(prob, T, S)
            % returns vector TS of decision variables: TS(k) = 1 iff
            % virtual component k of type T is mapped to real component 
            % with subtype S (otherwise TS(k) = 0).
            
            % separate component type from tag (e.g., we need "Generator" 
            % instead of "Generator Left" to get a proper submatrix from E)            
            T_lib = prob.Library.findRelatedType(T);

            % get component subtypes from the library for the types
            % present in the constraint (e.g. get all subtypes of
            % generators and AC buses)
            subtypes_for_T = prob.Library.Subtypes(T_lib);

            % get associations of library components to the subtypes,
            % connections between which we need to restrict
            T_S_lib_components = subtypes_for_T(S);

            % get mappings of virtual components to real from library
            T_mapping = Helpers.getMappingMatrix(prob, T);

            % from the mappings get vectors that store mapping of each
            % virtual component to the subtypes from the constraint input
            % ("1" if current component is mapped to the required subtype,
            %  "0" otherwise)
            TS = T_S_lib_components * T_mapping;
        end
        
        
        function cell_out = removeZeros(cell_in)
            % removes zero entries from a cell array
            cell_out = {};
            j = 1;
            for i = 1:length(cell_in)
                if isa(cell_in{i}, 'sdpvar') || cell_in{i} ~= 0
                    cell_out{j} = cell_in{i};
                    j = j + 1;
                end
            end
        end
    end
    % static methods end
end

