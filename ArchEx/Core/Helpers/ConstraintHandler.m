classdef ConstraintHandler < handle
% CONSTRAINTHANDLER: this class is responsible for processing constraints
% specified in the problem input file: 
% - parsing;
% - storing each constraint in a unified format structure;
% - calling methods of the Patterns class, which return the MILP formulations
%   for different patterns;
% - classifying constraints to corresponding groups (e.g. Flow, Timing etc)
%
% Instance of the ConstraintHandler class must be created in the Problem
% class within the ProcessConstraints method. During initialization the
% input file will be parsed for constraint patterns. User can later query
% the handler for MILP formulations of particular constraint groups to
% create lists of primary and auxiliary constraints using the
% getConstraintsMILP method. In such a way it is easy to quickly modify
% formulations for monolithic and iterative approaches of a problem by
% including/excluding certain constraints with only few lines of code.
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: December 2016

    properties (SetAccess = private, GetAccess = public)
        Storage
        
        % constraint categories that are currently supported
        General
        Interconnection
        Path
        Flow
        Balance
        Workload
        Timing
        Reliability
        Operation
        Mapping
        
        % AuxVariables array stores MILP constraints for certain helper
        % data structures (e.g. path matrices, flow matrices), which are
        % required to express these structures in terms of primary decision
        % variables and make them consistent to each other.
        AuxVariables
    end
    
    
    methods (Access = public)
        function obj = ConstraintHandler(prob, constraint_list)
            if nargin == 2
                obj.Storage = containers.Map;
                obj.General         = [];
                obj.Interconnection = [];
                obj.Path            = [];
                obj.Flow            = [];
                obj.Balance         = [];
                obj.Workload        = [];
                obj.Timing          = [];
                obj.Reliability     = [];
                obj.Operation       = [];
                
                obj.AuxVariables.Path        = [];
                obj.AuxVariables.Reliability = [];
                obj.AuxVariables.Flow        = [];
                
                obj.Parse(prob, constraint_list);
            else
                disp('No input file provided. Empty ConstraintHandler created.');
            end            
        end
        
        
        function Parse(obj, prob, constraint_list)
            % parse the constraints section of the input problem file. Each
            % of found constraints is stored in a struct. Structs are
            % classified according to constraint type (e.g. flow,
            % reliability etc.) User can then get MILP formulations of
            % particular constraint groups to formulate his/her problem. 
            %
            % NOTE: explanation of the parsing flow is given only for the
            % first set of patterns. Rest procedure is similar.
            str = sprintf('%s: Processing problem constraints...', class(prob));
            disp(str);                                    
            
            %% GENERAL PATTERNS
            %
            %% [at_least|at_most|exactly]_N_components( T, S', val )
            expr = '(?m)^[(at_least)|(at_most)|(exactly)]+_N_components[^;]*';
            items = regexp(constraint_list, expr, 'match');
            
            % from each found constraint extract pattern name and its
            % parameters
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                params = strsplit(params, ',');
                
                % create new constraint struct and fill its fields
                constraint = obj.initNewConstraint;
                if length(params) == 2
                    constraint.T1    = strtrim(params{1});
                    constraint.val   = str2double(params{2});
                else
                    constraint.T1    = strtrim(params{1});
                    constraint.S1    = strtrim(params{2});
                    constraint.val   = str2double(params{3});
                end                
                
                % NOTE: Actual pattern from Pattern class is not called
                % here, because getting MILP formulations of some of them
                % can be time-consuming. Instead, the call is done from the
                % getConstraintsMILP() method (so only if user actually
                % needs to use these constraints in his/her problem).
                %
                % Here we store a command, with which the method of
                % Patterns class can be called (as a string). To make the
                % call, eval() function is used.
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1, constraint.S1, constraint.val)';
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);                
                constraint.name     = pattern_name;
                constraint.category = 'general';
                
                % store the constraint in a corresponding category list
                obj.General = [obj.General, constraint];                                
            end            
                      
            %% INTERCONNECTION PATTERNS %%
            %
            %% [at_least|at_most|exactly]_N_connections[_if_used]( T1, S1', T2, S2', val )
            expr = '(?m)^[(at_least)|(at_most)|(exactly)]+_N_connections[^;]*';
            items = regexp(constraint_list, expr, 'match');           
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                params = strsplit(params, ',');
                
                constraint = obj.initNewConstraint;
                if length(params) == 3
                    constraint.T1       = strtrim(params{1});
                    constraint.T2       = strtrim(params{2});
                    constraint.val   = str2double(params{3});
                else
                    constraint.T1       = strtrim(params{1});
                    constraint.S1       = strtrim(params{2});
                    constraint.T2       = strtrim(params{3});
                    constraint.S2       = strtrim(params{4});
                    constraint.val   = str2double(params{5});
                end                
                
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1, constraint.S1, constraint.T2, constraint.S2, constraint.val)';
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'interconnection';
                obj.Interconnection = [obj.Interconnection, constraint];                                
            end
            
            %% [no_]in_conn_implies_[no_]out_conn( T, T_in, T_out )
            expr = '(?m)^((no_)*in_conn_implies_(no_)*out_conn)[^;]*';
            items = regexp(constraint_list, expr, 'match');                        
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                params = strsplit(params, ',');
                
                constraint = obj.initNewConstraint;
                current  = strtrim(params{1});
                incoming = params{2};
                outgoing = params{3};
                %incoming = incoming(~isspace(incoming));
                %outgoing = outgoing(~isspace(outgoing));
                %incoming = strsplit(incoming, '|');
                %outgoing = strsplit(outgoing, '|');
                constraint.T1 = current;
                constraint.T2 = incoming;
                constraint.T3 = outgoing;                
                
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1, constraint.T2, constraint.T3)';                                
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'interconnection';
                obj.Interconnection = [obj.Interconnection, constraint];                                
            end
            
            %% [no_]out_conn_implies_[no_]in_conn( T, T_out, T_in )
            expr = '(?m)^((no_)*out_conn_implies_(no_)*in_conn)[^;]*';
            items = regexp(constraint_list, expr, 'match');                  
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                params = strsplit(params, ',');
                
                constraint = obj.initNewConstraint;
                current  = strtrim(params{1});
                outgoing = params{2};
                incoming = params{3};
                %outgoing = outgoing(~isspace(outgoing));
                %incoming = incoming(~isspace(incoming));    
                %outgoing = strsplit(outgoing, '|');
                %incoming = strsplit(incoming, '|');                
                constraint.T1 = current;
                constraint.T2 = outgoing;
                constraint.T3 = incoming;
                
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1, constraint.T2, constraint.T3)';                                
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'interconnection';
                obj.Interconnection = [obj.Interconnection, constraint];                                
            end
            
            %% [no_]bidirectional_connection( T1, T2 )
            expr = '(?m)^((no_)*bidirectional_connection)[^;]*';
            items = regexp(constraint_list, expr, 'match');
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                params = strsplit(params, ',');
                
                constraint = obj.initNewConstraint;
                constraint.T1 = strtrim(params{1});
                constraint.T2 = strtrim(params{2});                
                
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1, constraint.T2)';                 
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'interconnection';
                obj.Interconnection = [obj.Interconnection, constraint];                                
            end
            
            %% no_self_loops( T )
            expr = '(?m)^(no_self_loops)[^;]*';
            items = regexp(constraint_list, expr, 'match');           
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'

                constraint = obj.initNewConstraint;
                constraint.T1 = strtrim(char(params));
                constraint.milp = Patterns.no_self_loops(prob, constraint.T1);
                
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1)';
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'interconnection';
                obj.Interconnection = [obj.Interconnection, constraint];
            end
            
            %% cannot_connect( T1, S1, T2, S2 )
            expr = '(?m)^(cannot_connect)[^;]*';
            items = regexp(constraint_list, expr, 'match');          
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                params = strsplit(params, ',');
                
                constraint = obj.initNewConstraint;
                constraint.T1    = strtrim(params{1});
                constraint.S1    = strtrim(params{2});
                constraint.T2    = strtrim(params{3});
                constraint.S2    = strtrim(params{4});
                
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1, constraint.S1, constraint.T2, constraint.S2)';                
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'interconnection';
                obj.Interconnection = [obj.Interconnection, constraint];
            end
            
            %% PATH PATTERNS
            %
            %% [at_least|at_most|exactly]_N_paths( T1, S1', T2, S2', val )
            expr = '(?m)^[(at_least)|(at_most)|(exactly)]+_N_paths[^;]*';
            items = regexp(constraint_list, expr, 'match');           
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                params = strsplit(params, ',');
                
                constraint = obj.initNewConstraint;
                if length(params) == 3
                    constraint.T1     = strtrim(params{1});
                    constraint.T2     = strtrim(params{2});
                    constraint.val = str2double(params{3});
                else
                    constraint.T1     = strtrim(params{1});
                    constraint.S1     = strtrim(params{2});
                    constraint.T2     = strtrim(params{3});
                    constraint.S2     = strtrim(params{4});
                    constraint.val = str2double(params{5});
                end                                
                
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1, constraint.S1, constraint.T2, constraint.S2, constraint.val)';                                
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'path';
                obj.Path = [obj.Interconnection, constraint];                                
            end
            
            %% OPERATION PATTERNS
            % has_operation_mode( T )
            expr = '(?m)^(has_operation_mode)[^;]*';
            items = regexp(constraint_list, expr, 'match');   
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                
                constraint = obj.initNewConstraint;
                % store the operation mode in T1 field
                constraint.T1 = strtrim(params);
                
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1)';                   
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'operation';
                obj.Operation = [obj.Operation, constraint];
            end
            
            %% FLOW PATTERNS
            %
            %% flow_balance( T, S' )
            expr = '(?m)^(flow_balance)[^;]*';
            items = regexp(constraint_list, expr, 'match'); 
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                params = strsplit(params, ',');
                
                constraint = obj.initNewConstraint;
                constraint.T1    = strtrim(params{1});
                if length(params) == 2
                    constraint.S1 = strtrim(params{2}); 
                end                

                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1, constraint.S1)';                      
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'flow';
                obj.Flow = [obj.Flow, constraint];
            end
            
            %% BALANCE PATTERNS
            %
            %% has_sufficient_power[_if_connected]( T1, S1', T2, S2' )
            expr = '(?m)^(has_sufficient_power(_if_connected)*)[^;]*';
            items = regexp(constraint_list, expr, 'match');
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                params = strsplit(params, ','); 
                
                constraint = obj.initNewConstraint;                
                if length(params) == 2
                    % if there are several componenents (e.g. A+B) - split them
                    constraint.T1       = strtrim(params{1});
                    constraint.T2       = strtrim(params{2});
                    %constraint.val   = str2double(str_item{3});
                else
                    constraint.T1     = strtrim(params{1});
                    constraint.S1     = strtrim(params{2});
                    constraint.T2     = strtrim(params{3});
                    constraint.S2     = strtrim(params{4});
                    %constraint.val = str2double(str_item{5});
                end  
                
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1, constraint.S1, constraint.T2, constraint.S2)';                 
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'balance';
                obj.Balance = [obj.Balance, constraint];
            end                        
            
            %% WORKLOAD PATTERNS
            %
            %% no_overloads( T, S' )
            expr = '(?m)^(no_overloads)[^;]*';
            items = regexp(constraint_list, expr, 'match'); 
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                params = strsplit(params, ',');
                
                constraint = obj.initNewConstraint;
                constraint.T1    = strtrim(params{1});
                if length(params) == 2
                    constraint.S1 = strtrim(params{2}); 
                end 
                                
                try
                    T_lib = prob.Library.findRelatedType(constraint.T1);
                    processing_rates = prob.Library.ProcessingRates(T_lib);
                catch
                    str = sprintf('%s: Components of type %s are not labeled with processing rates in the library', ...
                        class(prob), constraint.T1);
                    warning(str);
                    return;
                end                
                
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1, constraint.S1)'; 
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'workload';
                obj.Workload = [obj.Workload, constraint];
            end
            
            %% TIMING PATTERNS
            %
            %% max_cycle_time( T, S', val )
            expr = '(?m)^(max_cycle_time)[^;]*';
            items = regexp(constraint_list, expr, 'match');            
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                params = strsplit(params, ',');
                
                constraint = obj.initNewConstraint;
                if length(params) == 2
                    constraint.T1     = strtrim(params{1});
                    constraint.val = str2double(params{2});
                else
                    constraint.T1     = strtrim(params{1});
                    constraint.S1     = strtrim(params{2});
                    constraint.val = str2double(params{3});
                end
                
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1, constraint.S1, constraint.val)';
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'timing';
                obj.Timing = [obj.Timing, constraint];
            end
            
            %% max_idle_rate_sum( T, S', val )
            expr = '(?m)^(max_idle_rate_sum)[^;]*';
            items = regexp(constraint_list, expr, 'match');
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                params = strsplit(params, ',');
                
                constraint = obj.initNewConstraint;
                if length(params) == 2
                    constraint.T1     = strtrim(params{1});
                    constraint.val = str2double(params{2});
                else
                    constraint.T1     = strtrim(params{1});
                    constraint.S1     = strtrim(params{2});
                    constraint.val = str2double(params{3});
                end                
                
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1, constraint.S1, constraint.val)';
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'timing';
                obj.Timing = [obj.Timing, constraint];
            end
            
            %% RELIABILITY PATTERNS
            %
            %% min_redundant_components( T, S', val )
            expr = '(?m)^(min_redundant_components)[^;]*';
            items = regexp(constraint_list, expr, 'match');
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                params = strsplit(params, ',');
                
                constraint = obj.initNewConstraint;
                if length(params) == 2
                    constraint.T1     = strtrim(params{1});
                    constraint.val = str2double(params{2});
                else
                    constraint.T1     = strtrim(params{1});
                    constraint.S1     = strtrim(params{2});
                    constraint.val = str2double(params{3});
                end                
                
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1, constraint.S1, constraint.val)';
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'reliability';
                obj.Reliability = [obj.Reliability, constraint];
            end
            
            %% max_failprob_of_connection( T, S', val )
            expr = '(?m)^(max_failprob_of_connection)[^;]*';
            items = regexp(constraint_list, expr, 'match');            
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                params = strsplit(params, ',');
                
                constraint = obj.initNewConstraint;
                if length(params) == 2
                    constraint.T1     = strtrim(params{1});
                    constraint.val = str2double(params{2});
                else
                    constraint.T1     = strtrim(params{1});
                    constraint.S1     = strtrim(params{2});
                    constraint.val = str2double(params{3});
                end
                
                if isfield(prob.Requirement, 'Reliability')
                    prob.SetRequirement(constraint.T1, constraint.S1, constraint.val);
                end                
                
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1, constraint.S1, constraint.val)';
                
                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'reliability';
                obj.Reliability = [obj.Reliability, constraint];                                
            end                        
            
            %% MAPPING
            %
            %% (no_)in_flow_implies_[no_]mapping_to( T, Tag, S )
            expr = '(?m)^((no_)*in_flow_implies_(no_)*mapping_to)[^;]*';
            items = regexp(constraint_list, expr, 'match');            
            for i = 1:length(items)
                current = strsplit(items{i}, '(');
                pattern_name = current{1};
                params = current{2};
                params = params(1:end-1);   % cut the ')'
                params = strsplit(params, ',');
                
                constraint = obj.initNewConstraint;
                % incoming to type T
                constraint.T1    = strtrim(params{1});
                % from nodes with tag Tag (stored in T2)
                constraint.T2    = strtrim(params{2});
                % then must (not) be mapped to S2
                constraint.S2 = strtrim(params{3});
                
                pattern_name_ext = strcat('Patterns.', pattern_name);
                params_list      = '(prob, constraint.T1, constraint.T2, constraint.S2)';

                constraint.pattern  = strcat(pattern_name_ext, params_list);
                constraint.name     = pattern_name;
                constraint.category = 'mapping';
                obj.Mapping = [obj.Mapping, constraint];
            end
            
        end
        
        
        function res = getConstraintsMILP(obj, prob, varargin)
            % return the list of all MILP formulations corresponding to
            % constraint categories from input (variable)
            nVarargs = length(varargin);
            path_vars_added = 0;
            rel_vars_added  = 0;
            flow_vars_added = 0;
            res = [];
            for i = 1:nVarargs
                category = varargin{i};
                for j = 1:length(category)
                    constraint = category(j);                    
                    
                    % create auxiliary variables if required by current
                    % category (e.g. path matrices, flow matrices etc.)
                    if isempty(obj.AuxVariables.Path) && ...
                                (strcmp(constraint.category, 'path') || ...
                                 strcmp(constraint.category, 'reliability'))
                        obj.AuxVariables.Path = Helpers.createPathMatrices(prob);
                    end
                    
                    if isempty(obj.AuxVariables.Reliability) && ...
                            strcmp(constraint.category, 'reliability')
                        obj.AuxVariables.Reliability = ...
                            Helpers.createReliabilityConstraints(prob);
                    end
                    
                    if isempty(obj.AuxVariables.Flow) && ...
                            (strcmp(constraint.category, 'flow') || ...
                             strcmp(constraint.category, 'workload') || ...
                             strcmp(constraint.name, 'max_idle_rate_sum'))
                         obj.AuxVariables.Flow = ...
                             Helpers.createFlowRateMatrices(prob);
                    end
                    % check if there is a need of adding certain auxiliary
                    % variables (and related constraints) - they are stored
                    % separately from constraints specified by the user.
                    % Some constraints (e.g. path constraints) will not
                    % work without corresponding auxiliary variables.
                    if strcmp(constraint.category, 'path') == 1 && ~path_vars_added                        
                        path_vars_added = 1;
                        res = [res, obj.AuxVariables.Path];
                    elseif strcmp(constraint.category, 'reliability') == 1 && ~rel_vars_added                        
                        rel_vars_added  = 1;
                        res = [res, obj.AuxVariables.Path, obj.AuxVariables.Reliability];
                        if ~path_vars_added
                            % reliability constraints also depend on paths
                            path_vars_added = 1;
                            res = [res, obj.AuxVariables.Path];
                        end
                    elseif (strcmp(constraint.category, 'flow') == 1 ...
                            || strcmp(constraint.category, 'workload') == 1) ...
                            && ~flow_vars_added
                        flow_vars_added = 1;
                        res = [res, obj.AuxVariables.Flow];
                    end
                    
                    % get the MILP formulation of the constraint by running
                    % the command (pattern call) stored in
                    % constraint.pattern using eval()
                    constraint.milp = eval(constraint.pattern);
                    
                    % add the MILP constraint formulation
                    res = [res, constraint.milp];
                end
            end
        end
        % public methods end
    end
    
    
    methods (Access = private)
        function res = initNewConstraint(obj)
            res.T1          = '';
            res.T2          = '';
            res.T3          = '';
            res.S1          = '';
            res.S2          = '';
            res.S3          = '';            
            res.name        = '';
            res.category    = '';
            res.pattern     = '';
            res.val         = 0;
            res.milp        = [];
        end  
    end
end

