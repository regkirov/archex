classdef LayeredLayout < Abstractlayout
% EPSLayout is a layout class for Graphviz4Matlab for the EPS
% (electrical power system) problem. It is a layered layout, where each
% layer corresponds to a certain type of nodes/components (e.g.,
% generators, rectifiers and so on).
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: July 2016
    
    properties
        xmin;               % The left most point on the graph axis in data units           
        xmax;               % The right most point on the graph axis in data units
        ymin;               % The bottom most point on the graph axis in data units
        ymax;               % The top most point on the graph axis in data units
        adjMatrix;          % The adjacency matrix
        maxNodeSize;        % The maximum diameter of a node in data units
        image;              % An image for the button that will lanuch this layout
        name;               % A unique name for instances of this class
        shortDescription;   % A description for use in the tooltips
        nodeSize;           % The calculated node size, call dolayout() before accessing
        centers;            % The calculated node centers in an n-by-2 matrix
        
        level_indices;      % Indices of first elements of each layer in the array of nodes
    end
    
    methods
        function obj = LayeredLayout(name, indices)      
        % constructor
            if(nargin < 2)
                error('LayeredLayout: not enough input parameters');
            else
                obj.name = name;
                obj.level_indices = indices;
            end
            load glicons;
            obj.image = icons.energy;
            obj.shortDescription = 'Layered Layout (ArchEx)';
        end
    end
    
    methods(Access = 'protected')
       
        function calcLayout(obj)
            % calculate layout parameters (depending on number of nodes)
            nnodes = size(obj.adjMatrix,1);
            obj.centers = zeros(nnodes,2);
            xspacePerNode = (obj.xmax - obj.xmin)/ceil(sqrt(nnodes));
            yspacePerNode = (obj.ymax - obj.ymin)/ceil(sqrt(nnodes));
            obj.nodeSize = min(min([xspacePerNode,yspacePerNode]./2),obj.maxNodeSize);
            
            % generate parameters for placement of nodes in the resulting graph
            num_levels = length(obj.level_indices);
            horizontal_spacing = cell(1,num_levels);
            for i = 1:num_levels
                if i == num_levels
                    nodes_on_curr_level = nnodes - obj.level_indices(end) + 1;
                else
                    nodes_on_curr_level = obj.level_indices(i+1) - obj.level_indices(i);
                end
                horizontal_spacing{i} = linspace(0.1, 0.9, nodes_on_curr_level);   
            end

            vertical_spacing = linspace(0.1, 0.9, num_levels);
            % flip the spacing, so top level will be shown on top (bottom by default)
            vertical_spacing = fliplr(vertical_spacing);
            
            % adjust the graph layout (move and distribute the nodes according 
            % to their levels.
            node_idx = 1;
            for i = 1:num_levels
                if i == num_levels
                    nodes_on_curr_level = nnodes - obj.level_indices(end) + 1;
                else
                    nodes_on_curr_level = obj.level_indices(i+1) - obj.level_indices(i);
                end

                curr_level_spacing = horizontal_spacing{i};
                for j = 1:nodes_on_curr_level
                    obj.centers(node_idx,:) = [curr_level_spacing(j), vertical_spacing(i)];
                    node_idx = node_idx + 1;
                end
            end
        end
        
        
    end
end

