classdef (Abstract) Problem < handle
% This is an abstract class representing a generic architecture exploration
% problem in ArchEx. It includes properties, which any specific problem
% object must initialize and use as well as properties that are used only
% by problems that require certain aspects (e.g. auxiliary data structures
% for reliability constraints). The latter are, however, not initialized in
% problems, in which they are not needed.
%
% The class also includes two groups of methods. First group are abstract
% methods, which every specific problem class must implement (e.g. for
% running the solver, showing results). Second group includes general methods
% for preprocessing certain sections of the problem description file 
% (applicable to any ArchEx problem).
%
% Developers can create new problem classes inheriting from Problem and
% implementing its abstract methods. We try to put properties and methods
% applicable in more than one problem to this class.
%
% REFERENCES:
% [1] D. Kirov, P. Nuzzo, R. Passerone and A. Sangiovanni-Vincentelli. 
%     "ArchEx: An Extensible Framework for the Exploration of
%     Cyber-Physical System Architectures". In Proc. of the 54th Design
%     Automation Conference (DAC), 2017.
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: January 2017
    
    properties (GetAccess = public, SetAccess = {?Problem,?Helpers})
        % problem name (string)
        Name
        
        % tags are problem-specific extensions, which help to sub-classify
        % same library component types, for instance they can represent
        % spatial property (left, right etc.)        
        Tags
        
        % functional flow (further explained in [1])
        Flow
        
        % input/library filenames and input data (read from file)
        input_filename
        lib_filename
        input
        
        % internal structures required to initialize the adjacency matrix
        NumComponents
        ConnectionRules                
        
        % primary data structures containing decision variables
        AdjacencyMatrix
        LibraryMapping
        
        % aux data structures for reasoning about paths
        oneHopLayer
        allHopsLayer
        allPathsLayer
        
        % library of components and lists of variable/fixed components
        Library
        components_variable
        components_fixed
                        
        % there are two lists of constraints: primary and auxiliary. The
        % former is used in both monolithic and iterative approaches. The
        % latter is combined with the former for solving monolithic problem
        Constraints
        AuxiliaryConstraints
        
        % handler object for parsing and creating MILP constraints
        ConstraintHandler
        
        % analysis object (e.g. reliability analysis)
        Analysis
        
        % name of the algorithm for optimization/mapping (e.g. "iterative")
        Algorithm
        
        % objective (cost) function of the problem expressed using decision
        % variables
        CostFunction
        
        % structure for storing the requirement (e.g. Timing, Reliability)
        Requirement
        
        % solution (solver input/output, graph, time elapsed)
        Solution
        
        % aux data structures (reliability)
        numberOfPaths
        numberOfPaths_index
        approximate_reliabilities
        estimated_reliabilities
        
        % aux data structures (flow, load)
        flowRateMatrices
        inputRates
        procRates
        idleRates
        
        % misc
        restrictions
        node_descriptions
        edge_cost
    end
    
    
    % temporary solution for accessing the requirement from Patterns class
%     properties (GetAccess = public, SetAccess = {?Problem, ?Patterns})
%         Requirement
%     end
    
    
    methods (Abstract)
        ProcessDescription        (obj)
        ProcessTemplate           (obj)
        ProcessConstraints        (obj)
        DefineDecisionVariables   (obj)
        DefineCostFunction        (obj)  
        SetRequirement            (obj, varargin)
        
        Solve                     (obj, options)
        LearnCons                 (obj, analysis_result)
        ShowResult                (obj)
    end
    
    
    methods
        function obj = Problem(name, lib_filename, input_filename, algorithm)
            if nargin > 0
                obj.Name                 = name;
                obj.Tags                 = {};
                obj.Constraints          = [];
                obj.AuxiliaryConstraints = [];
                obj.CostFunction         = 0;
                obj.edge_cost            = 0;
                obj.restrictions         = containers.Map;
                
                obj.lib_filename = lib_filename;
                obj.input_filename = input_filename;
                obj.Algorithm = algorithm;
                
                % init the solution
                obj.Solution.time_elapsed = Inf;
                obj.Solution.solver_data  = [];
                obj.Solution.graph        = [];                
                
                % read the input file
                path = './Problems/';
                path = strcat(path, obj.input_filename);
                obj.input.all = fileread(path);                                
            else
                error('Generic Problem: Not enough parameters.');
            end
        end
        
        
        function setComponentsLibrary(obj, lib)
            obj.Library = lib;
        end
        
        
        function setAlgorithm(obj, new_algorithm)
            obj.Algorithm = new_algorithm;
        end
        
        
        function addConstraint(obj, new_constraint)
            obj.Constraints = [obj.Constraints, new_constraint];
        end
        
        
        function addAuxConstraint(obj, new_constraint)
            obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, new_constraint];
        end
        
        
        function addConstraintFromPattern(obj, pattern)
            % parse the pattern (call parser?)
            % call Patterns class 
        end
        
        
        function ProcessDescriptionGeneric( obj )
            % parse the description part of the problem input file and
            % extract properties that must be present in any specific
            % problem: name, list of component types (variable and fixed),
            % functional flow, tags and cost of graph edges (connections).
            pattern = '(?m)^(BEGIN\s*\(Description\)).*(END\s*\(Description\))';
            subset = char(regexp(obj.input.all, pattern, 'match'));
            obj.input.description = subset;
            pattern = '(?m)(?<=^(Name:))[^;]*';
            name = char(regexp(subset, pattern, 'match'));
            name = strtrim(name);
            obj.Name = name;
            
            pattern = '(?m)(?<=^(Variable components:))[^;]*';
            res = char(regexp(subset, pattern, 'match'));
            obj.components_variable = strtrim(strsplit(res, ',')); 
            
            pattern = '(?m)(?<=^(Fixed components:))[^;]*';
            res = char(regexp(subset, pattern, 'match'));
            obj.components_fixed = strtrim(strsplit(res, ','));
            
            pattern = '(?m)(?<=^(Functional flow:))[^;]*';
            res = char(regexp(subset, pattern, 'match'));
            obj.Flow = strtrim(strsplit(res, ','));
            
            pattern = '(?m)(?<=^(Tags:))[^;]*';
            res = char(regexp(subset, pattern, 'match'));
            obj.Tags = strtrim(strsplit(res, ','));
            
            pattern = '(?m)(?<=^(Edge cost:))[^;]*';
            res = char(regexp(subset, pattern, 'match'));
            obj.edge_cost = str2double(res);
        end
        
        
        function ProcessTemplateGeneric( obj ) 
            % parse the template structure section of the problem input
            % file, which includes maximum available number of components 
            % of different types (or type+tag) and connection rules (list
            % of allowed connections between components)
            
            % parse the components number section
            pattern = '(?m)^(BEGIN\s*\(Template\)).*(END\s*\(Template\))';
            subset = char(regexp(obj.input.all, pattern, 'match'));
            obj.input.num_components = subset;
            
            % generate keys for the rows and columns of the helper
            % matrices (each key is a pair like "Generator Left")
            i = 1;
            key_pairs = {};
            for part1 = obj.Flow
                for part2 = obj.Tags
                    key = [char(part1), ' ', char(part2)];
                    key_pairs{i} = key;
                    i = i + 1;
                end
            end
            
            % initialize the NumComponents map with zeros
            obj.NumComponents = containers.Map;
            for key = key_pairs
                obj.NumComponents(char(key)) = 0;
            end
            
            % initialize the connection rules data structure with zeros (by
            % default anything cannot be connected to anything)
            obj.ConnectionRules = MapN;
            for row = key_pairs
                for col = key_pairs
                    obj.ConnectionRules(char(row), char(col)) = 0;
                end
            end
            
            % generate the NumComponents data structure, which stores
            % amounts of each component in the problem. We use this helper
            % structure to create the adjacency matrix.
            pattern = '(?m)^(\[\s*\w*\s*\]) [^;]*';
            res = regexp(subset, pattern, 'match');
            for row = res
                items = strsplit(char(row), ' ');
                items{1} = items{1}(2:end-1);
                key = [strtrim(items{2}), ' ', strtrim(items{1})];
                obj.NumComponents(key) = str2double(items{3});
            end
            
            % parse structure section (they are required to
            % initialize the adjacency matrix)
            % first get the corresponding subset of the input file
            pattern = '(?m)^(BEGIN\s*\(Composition rules\)).*(END\s*\(Composition rules\))';
            subset = char(regexp(obj.input.all, pattern, 'match'));
            obj.input.composition_rules = subset;
            
            % then get the list of constraints
            pattern = '(?m)^(\[\w*\]) [^;]*';
            rules = regexp(subset, pattern, 'match');
            
            for rule = rules
                % fill in the ConnectionRules structure ("1" - components
                % can be connected, "0" otherwise)
                items = strsplit(char(rule));
                if strcmp(items{1}(1), '%') == 1
                    % skip if the row is commented (starts with '%')
                    continue
                end
                type1 = [items{2}, ' ', items{1}(2:end-1)];
                type2 = [items{5}, ' ', items{4}(2:end-1)];
                obj.ConnectionRules(type1, type2) = 1;
            end            
        end
        
        
        function ProcessConstraintsGeneric( obj )
            % parse the constraints section of the file
            pattern = '(?m)^(BEGIN\s*\(Constraints\)).*(END\s*\(Constraints\))';
            subset = char(regexp(obj.input.all, pattern, 'match'));
            obj.input.constraints = subset;
        end                        
    end
    
end

