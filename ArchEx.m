%% ARCHitectural EXploration Framework (ArchEx 2.0)
%  NOTE: for the demo please use the demo scripts (*_DEMO.m).
%
% ArchEx 2.0 takes as input a problem description file, containing
% parameters of a reconfigurable architectural template (i.e., number of
% graph nodes, functional flow, tags, composition rules) and a library of
% components and selects an optimal (minimum-cost) system configuration,
% while meeting all the provided constraints.
%
% It implements two different methodologies based on Mixed Integer Linear
% Programming (MILP):
% - Monolithic ("eager") optimization of a problem with all provided
%   constraints: type 'help eagerAlgorithm' for further information.
% - Iterative ("lazy") algorithm, in which the calls to the MILP solver
%   alternate with a heuristic learning function: type 'help lazyAlgorithm'
%   for further details.
%
% Currently supported exploration problems include:
% - Electrical power network (EPN) of a passenger aircraft: synthesizing an
%   optimal configuration subject to interconnection, balance and
%   reliability constraints. Currently supported approaches: monolithic,
%   iterative.
% - Reconfigurable production line (RPL): selection of cost-effective
%   solution subject to interconnection, balance, flow, load and timing
%   constraints. Currently supported approaches: monolithic.
%
% The tool is launched by running this script. Particular problem to solve 
% as well as its description and the library are also selected here. One is
% also able to select the solver here as well as tune certain parameters of
% the solver.
% 
% Currently supported MILP solver: IBM ILOG CPLEX
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: February 2017

function archex_problem = ArchEx( problem_type, problem_file, library_file)
    %% main function for launching ArchEx
    %   - configure environment (update path, clean old logs etc.)
    %   - create the corresponding problem instance (problem_type)
    %   - solve it
    %   - save results
    
    % If no parameters are provided, debug version is launched. 
    if nargin < 3
        debug = 1;
    else
        debug = 0;
    end
    
    %% Include sub-components of ArchEx.
    addpath(genpath('ArchEx'));
    addpath(genpath('Libraries'));
    addpath(genpath('Problems'));
    addpath(genpath('Utilities'));
    
    % add local user folder to path (Graphviz is sometimes installed there)
    path1 = getenv('PATH');
    path1 = [path1 ':/usr/local/bin'];
    setenv('PATH', path1);
    clear('path1');

    % turn off some unnecessary warnings
    warning( 'off', 'MATLAB:lang:badlyScopedReturnValue' );

    %% create output directories for matlab data, cplex data and graphs
    if ~exist('Output', 'dir')
        mkdir('Output');
    end

    if ~exist('Output/graphs', 'dir')
        mkdir('Output/graphs');
    end

    if ~exist('Output/cplex', 'dir')
        mkdir('Output/cplex');
    end

    %% start ArchEx 2.0
    fprintf('\n');
    disp('======================================');
    disp('===           ArchEx 2.0           ===');
    disp('======================================');
    fprintf('\n');

    %% clean up old logs and graphs if needed
    disp('Running cleanup...');

    % cleaning old logs from the Output folder (if necessary)
    deleted = logCleaner('max_logs_allowed', 10, 'dir', 'Output');
    deleted = logCleaner('max_logs_allowed', 10, 'dir', 'Output/graphs');
    deleted = logCleaner('max_logs_allowed', 10, 'dir', 'Output/cplex');
    clear('deleted');

    %clear all;
    close all;

    disp('Cleanup completed.');
    fprintf('\n');

    %% create a problem instance
    %  INPUT:
    %  (1) - name of the problem;
    %  (2) - name of the components library TXT file;
    %  (3) - name of the problem input TXT file;
    %  (4) - algorithm ("eager" for monolothic or "lazy" for iterative).
    if debug   
        tic
        prob = EPNProblem('TEST EPN PROBLEM', 'demos/DEMO_EPN_LIB.txt', 'EPN_test.txt', 'lazy');
        %prob = RPLProblem('TEST RPL PROBLEM', 'RPL_library.txt', 'RPL_test.txt', 'eager');
        
        % smaller problems for regression testing
        %prob = EPNProblem('TEST EPN PROBLEM (small)', 'EPN_library_small.txt', 'EPN_test_small.txt', 'lazy');        
        %prob = RPLProblem('TEST RPL PROBLEM (small)', 'RPL_library.txt', 'RPL_test_small.txt', 'eager');

        % measure the setup time
        str = sprintf('Setup time: %0.2fs\n', toc);
        disp(str);
        tic
        
        %solver_options = cplex_options('debug');
        
        %% run the solver
        %prob.Solve(solver_options);

        %% save the problem data for future reference
        %prob.Save;        
    
    else
        tic
        if strcmp(problem_type, 'EPN')
            prob = EPNProblem('EPN DEMO', library_file, problem_file, 'lazy');            
        elseif strcmp(problem_type, 'RPL')        
            prob = RPLProblem('RPL DEMO', library_file, problem_file, 'eager');
        end
        
        % measure the setup time
        str = sprintf('Setup time: %0.2fs\n', toc);
        disp(str);
        tic
        
        %% setup the solver
        % currently CPLEX is used, corresponding config script is called
        solver_options = cplex_options('demo');
        
        %% run the solver
        prob.Solve(solver_options);
        
        %% save the problem data for future reference
        prob.Save;
    end
    
    % add the ArchEx problem object to the base workspace
    assignin('base', 'prob', prob);
end 
