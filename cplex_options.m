function [ options ] = cplex_options( type )
% SOLVER_OPTIONS returns the sdpsettings structure with solver parameters.
% Currently used solver is IBM CPLEX. 
%
% There are several configurations:
%   - demo    (minimum verbose)
%   - eager   (options for the monolithic optimization, max verbose)
%   - lazy    (options for the iterative approach, max verbose)
%   - default (only select the solver in the options, everything else by
%              default as created by YALMIP)

% create solver options structure via YALMIP command
options = sdpsettings;

% select IBM CPLEX as the solver
options.solver = 'cplex';

if strcmp(type, 'demo')    
    % number of threads (and CPU cores) that CPLEX can use. A value of 1 means
    % single-core, i.e., no parallelization. During the testing it has been
    % found out that this can make a significant speedup compared with multiple
    % threads/cores (probably due to heavy overhead for synchronizing them)
    options.cplex.threads = 1;

    % relative tolerance on the gap between the best integer objective and the
    % objective of the best node remaining. Ideally the solver terminates, when
    % the gap is 0, however, in certain problems it can get stuck without
    % progressing quickly down to smaller gap values (e.g. stuck around 10% gap
    % and improve in less than 1% in a couple of hours). By tuning this
    % parameter one can force the solver to terminate earlier (e.g. when the
    % gap is 10%). This is often useful and we observed that the solution is
    % typically the same or very similar to the one returned after exploring
    % the whole solution tree.
    options.cplex.mip.tolerances.mipgap = 0.01;

    % solver timeout
    options.cplex.timelimit = 10800; % 3h

    % save CPLEX input and output for further analysis and storing
    options.savesolverinput  = 1;
    options.savesolveroutput = 1;

    % this is a demo, so verbose is minimal
    options.debug        = 0;
    options.verbose      = 0;
    options.showprogress = 1;

    % don't save dual variables and constraints (if any)
    options.saveduals = 0;
    
elseif strcmp(type, 'eager')
    options.cplex.threads = 1;
    options.cplex.mip.tolerances.mipgap = 0.005;
    options.cplex.timelimit = 43200; % 12h
    
    options.savesolverinput  = 1;
    options.savesolveroutput = 1;

    % maximum verbose
    options.debug        = 1;
    options.verbose      = 2;
    options.showprogress = 1;

    options.saveduals = 0;
    
elseif strcmp(type, 'lazy')
    options.cplex.threads = 1;
    options.cplex.mip.tolerances.mipgap = 0;
    options.cplex.timelimit = 43200; % 12h
    
    options.savesolverinput  = 1;
    options.savesolveroutput = 1;

    % maximum verbose
    options.debug        = 1;
    options.verbose      = 2;
    options.showprogress = 1;

    options.saveduals = 0;
    
else
    % default options 
    options.savesolverinput  = 1;
    options.savesolveroutput = 1;
end

end

