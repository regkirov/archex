%% GRAVEYARD FOR OLD CODE
%  Here some old class methods and code snippets are stored, which might
%  still be (re)used in the future.
%
%% old LearnCons for EPSProblem (calls one of actual learning functions depending on user input)
% function res = LearnCons2(obj, result)
%     % LearnCons is a part of the "lazy" algorithm. For the EPS
%     % problem it takes the result of the reliability analysis as
%     % an input and using current solution provided by the optimizer
%     % adds new constraints to guide the solver to an optimal and
%     % satisfiable result.
%     %
%     % Currently for the EPS problem there are several learning
%     % strategies. The user can select one of them when formulating
%     % the problem. The execution of the "lazy" algorithm will have
%     % different timing and number of iterations, results might have
%     % different reliability and cost (depending on the strategy).
%     %
%     if strcmp(obj.learning_strategy, 'general') == 1
%         % general learning strategy from the 2015 DATE paper
%         % estimates, how many extra paths are needed from sinks to
%         % sources to satisfy the reliability requirement and adds
%         % them. Uses allPaths data structures and has several
%         % iterations, therefore, it is slower than eager approach.
%         res = obj.learning_general(result);
%     elseif strcmp(obj.learning_strategy, 'heuristic_optimistic') == 1
%         % heuristic strategy that adds one connection or component
%         % on each iteration. Avoids over-design (has smaller cost),
%         % but can have iterations that do not improve the result.
%         res = obj.learning_heuristic_slow(result, 'optimistic');
%     elseif strcmp(obj.learning_strategy, 'heuristic_conservative') == 1
%         % conservative version of the above, has less iterations by
%         % connecting a new component to all existing ones when
%         % added. Executes faster but has bigger cost (over-design)
%         res = obj.learning_heuristic_slow(result, 'conservative');
%     elseif strcmp(obj.learning_strategy, 'heuristic_fast') == 1
%         % a faster heuristic approach, also estimates the number of
%         % extra paths and uses this number to decide, how many
%         % connections and new components to add. It does not
%         % operate on paths inside the optimization problem though.
%         % Currently this is the fastest strategy.
%         res = obj.learning_heuristic_fast(result);
%     else
%         warning('LearnCons: selected learning strategy does not exist.');
%         res = 0;
%     end
% end   
%
%% learning_general - general learning function for EPS problem (using path matrices, i.e., very slow)
% function res = learning_general(obj, result)
%     %% original LearnCons algorithm from the 2015 DATE paper. 
%     % Based on the current reliability analysis
%     % results it estimates, how many extra paths (functional links) are
%     % required for each sink and adds them (as a new constraint).
%     % The algorithm also has a fine tuning part (when number of
%     % extra paths needed is 0 but the requirement is still UNSAT).
%     res = 0;
%     sink   = obj.Flow{end};
% 
%     % estimate the worst failure probability of a functional link
%     prob_path_failure = 0;
%     prob_edge_failure = obj.edge_failure;
%     for i = 1:length(obj.Flow)-1
%         current = obj.Flow{i};
%         fprobs = obj.Library.FailureProbabilities(current);
%         % take the worst failure prob (we need the worst case)
%         prob_component_failure = max(fprobs);
%         prob_path_failure = prob_path_failure + prob_component_failure + prob_edge_failure;                               
%     end
% 
%     % find the worst reliability and indices of sinks that have it
%     worst_result = max(result);
%     unsat_indices = find(result == worst_result);                                                
% 
%     % estimate the number of extra paths required and add a
%     % constraint to add these paths.
%     % ToDo: process each sink separately (as in heuristic_fast).
%     % Currently k is calculated based on the worst reliability.
%     k = max(floor(log(obj.Requirement/max(result))/log(prob_path_failure)));
%     %k = min(k,num_sources-1);
%     if k ~= 0
%         for i = 1:length(obj.Flow)-1
%             % iterate over all component types starting from source
%             current = obj.Flow{i};
%             all_paths_current = obj.allPathsLayer(current, sink);
%             all_paths_current = all_paths_current.Matrix;
%             all_paths_current = all_paths_current(:,:,1);
%             all_paths_values = round(value(all_paths_current));
%             max_paths_current = size(all_paths_current,1);
%             for j = unsat_indices
%                 % iterate over those sinks for which the
%                 % requirement is unsat
%                 current_paths = sum(all_paths_values(:,j),1);
%                 % check how many paths can be actually added (take
%                 % the minimum between k and path still available to
%                 % be added)
%                 k_still_available = min(k, max_paths_current - current_paths);
%                 if k_still_available == 0
%                     % nothing can be added on current level - skip
%                     continue;
%                 else
%                     % increase the number of paths
%                     obj.Constraints = [obj.Constraints,...
%                         sum(all_paths_current(:,j)) >= current_paths + k_still_available];
%                     res = res + 1;
%                 end
%             end
%         end
%     else
%         %% fine tuning
%         % When k is zero but requirement is still UNSAT, find a
%         % component type, which currently has minimum redundancy
%         % and add more paths to these components
%         [min_red_type, min_indices] = obj.findMinRedType;
%         all_paths_current = obj.allPathsLayer(min_red_type, sink);
%         all_paths_current = all_paths_current.Matrix;
%         all_paths_current = all_paths_current(:,:,1);
%         all_paths_values = round(value(all_paths_current));
%         max_paths_current = size(all_paths_current,1);                                
% 
%         for i = min_indices
%             current_paths = sum(all_paths_values(:,i),1);
%             if current_paths == max_paths_current
%                 % already nothing to add
%                 continue
%             else
%                 obj.Constraints = [obj.Constraints,...
%                     sum(all_paths_current(:,i)) >= current_paths + 1];
%                 res = res + 1;
%             end
%         end
%     end            
% end
%
%% learning_heuristic_slow for EPS problem - slow learning function (conservative)
% function res = learning_heuristic_slow(obj, result, type)
%     %% semi-general semi-heuristic learning procedure, which does
%     % not use auxiliary data structures (path matrices). These
%     % structures are not added to the problem and not processed by
%     % Yalmip (which takes considerable time). Therefore, the
%     % execution speed is much faster for the price of generality.
%     %
%     % This algorithm is based on comparing the degree of redundancy
%     % with current number of used components. It iterates over all
%     % component types starting from closest to the sink and tries
%     % to increase their redundancy degrees (first by trying to
%     % connect components together if horizontal connections are
%     % possible, second by adding new components and connecting them
%     % to already existing ones).
%     %
%     % There are two versions of this strategy: optimistic and
%     % conservative:
%     %
%     % Optimistic adds only one constraint on every iteration trying 
%     % to have more control on topology modifications and, therefore, 
%     % avoiding over-design. However, certain iterations can make
%     % the result worse or at least not better (due to reconnecting
%     % components; a "good" connection might be changed to 2 "bad"
%     % ones)
%     %
%     % Conservative approach avoids these "bad" iterations by adding
%     % more connectivity between components of the same type.
%     % Basically it tries to connect all of them together, which
%     % converges faster but brings some over-design (bigger cost).
%     [adjm, indices] = obj.AdjacencyMatrix.getMatrixNumeric;
%     adjm = round(value(adjm));
%     sink = obj.Flow{end};
%     res = 0;
% 
%     % get a merged list of indices (e.g. instead of
%     % mappings to "Generator Left", "Generator Middle" and "Generator
%     % Right" we need one merged index pair for "Generator")
%     ordering = obj.AdjacencyMatrix.Row_keys;
%     merged_indices = containers.Map;
%     previous_key_part1 = '';
%     for key = ordering
%         % go through the ordered list of adjacency matrix keys
%         key_part1 = strsplit(char(key));
%         key_part1 = key_part1{1};
%         curr_indices = indices(char(key));
%         if strcmp(key_part1, previous_key_part1) == 1
%             % update (extend) indices for current component type
%             curr_indices = merged_indices(key_part1);
%             extra_indices = indices(char(key));
%             curr_indices(2) = extra_indices(2);
%         end
%         previous_key_part1 = key_part1;
%         merged_indices(key_part1) = curr_indices;
%     end
% 
%     % estimate max_hops (number of adj matrix multiplications)
%     max_hops = 0;
%     for i = 1:length(obj.Flow)-1
%         current = obj.Flow{i};
%         submatrix = obj.AdjacencyMatrix.getSubMatrixNumeric(current, sink);
%         num_current_components = size(submatrix, 1);
%         max_hops = max_hops + num_current_components;
%     end
% 
%     % do adjacency matrix multiplications (up to maximum number of 
%     % hops) to evaluate the walk indicator matrix and degrees of 
%     % redundancy
%     walk_indicator_matrix = zeros(size(adjm,1));
%     for i = 1:max_hops
%         product = adjm;
%         for j = 1:i-1
%             % logical AND (multiplication)
%             product = product * adjm;
%             product = product > 0;
%         end
%         % logical OR (addition)
%         walk_indicator_matrix = walk_indicator_matrix + product;
%         % keep the values binary
%         walk_indicator_matrix = walk_indicator_matrix > 0;              
%     end
% 
%     % get degrees of redundancy from the walk indicator matrix
%     degrees_list = containers.Map;
%     for i = 1:length(obj.Flow)-1
%         current = obj.Flow{i};
%         current_indices_row = merged_indices(current);
%         current_indices_col = merged_indices(sink);
%         current_all_paths_matrix = ...
%             walk_indicator_matrix(current_indices_row(1):current_indices_row(2), ...
%                 current_indices_col(1):current_indices_col(2));
%         current_degree_of_redundancy = sum(current_all_paths_matrix,1);
%         degrees_list(current) = current_degree_of_redundancy;
%     end            
% 
%     %% First pass
%     % Iterate over all components and try increasing the degree of
%     % redundancy if it is less than current number of components.
%     % Try doing this by adding horizontal connections whenever
%     % possible.
%     for i = length(obj.Flow)-1:-1:1
%         % iterate over all components starting from closest to sink
%         current = obj.Flow{i};
%         preceding = obj.Flow{i+1};
%         % get current degree of redundancy
%         red_degree = degrees_list(current);                                                
% 
%         % get current number of instantiated components from
%         % library mappings
%         current_mapping = obj.LibraryMapping(current);
%         current_mapping_values = round(value(current_mapping));
%         current_num_components = sum(sum(current_mapping_values));
%         max_components = size(current_mapping,2);
% 
%         % skip if nothing can be added already
%         if current_num_components == max_components ...
%                 && current_num_components == min(red_degree)
%             continue;
%         end
% 
%         if sum(red_degree < current_num_components) && ...
%                 obj.canBeConnected(current, current)
%             % degree of redundancy still can be increased by
%             % connecting components together (horizontally) without
%             % adding new ones. This is likely cheaper.
%             current_current_matrix = ...
%                 obj.AdjacencyMatrix.getSubMatrixNumeric(current, current);
%             horizontal_conn_matrix_values = round(value(current_current_matrix));
%             current_preceding_matrix = ...
%                 obj.AdjacencyMatrix.getSubMatrixNumeric(current, preceding);
%             % add a horizontal connection
%             obj.Constraints = [obj.Constraints, ...
%                 sum(sum(current_current_matrix)) >= ...
%                 sum(sum(horizontal_conn_matrix_values)) + 1];
%             % only connect those components, which are currently
%             % instantiated (used)
%             obj.Constraints = [obj.Constraints, ...
%                 Patterns.incomingThenOutgoing(current_preceding_matrix', current_current_matrix)];
%             % status pring to console
%             str = sprintf('Red_degree = %d, num_components = %d.', ...
%                 min(red_degree), current_num_components);
%             disp(str);
%             str = sprintf('Adding horizontal connection for each %s connected to %s', ...
%                 current, preceding);
%             disp(str);
%             res = res + 2;
%             return;
%         elseif sum(red_degree < current_num_components) && ...
%                 ~obj.canBeConnected(current, current)
%             % current minimal degree of redundancy equals current
%             % number of instantiated components. Horizontal
%             % connections won't help in any case, the only possible
%             % move is to add (instantiate) extra components
%             obj.Constraints = [obj.Constraints, ...
%                 sum(sum(current_mapping)) >= current_num_components + 1];
%             str = sprintf('Red_degree < num_components. Adding an extra %s', current);
%             disp(str);
%             res = res + 1;
% 
%             % we are likely here if the closest component to the
%             % sink cannot have horizontal connections. This likely
%             % entails that the problem is unfeasible, but still we
%             % try to do what we can by going to an upper layer to
%             % try adding horizontal connections.
%             continue;
%             %return;
%         end
%     end
% 
%     %% Second pass
%     % Degree of redundancy on all levels is equal to current number
%     % of components. Increasing the reliability is only possible by
%     % adding new components and connecting them together.
%     for i = length(obj.Flow)-1:-1:1
%         current = obj.Flow{i};
%         % get current degree of redundancy
%         red_degree = degrees_list(current);                                                
% 
%         % get current number of instantiated components from
%         % library mappings
%         current_mapping = obj.LibraryMapping(current);
%         current_mapping_values = round(value(current_mapping));
%         current_num_components = sum(sum(current_mapping_values));
%         max_components = size(current_mapping,2);
% 
%         % skip if nothing can be added already
%         if current_num_components == max_components ...
%                 && current_num_components == min(red_degree)
%             continue;
%         end
% 
%         if ~sum(red_degree < current_num_components)
%             % add a new component
%             obj.Constraints = [obj.Constraints, ...
%                 sum(sum(current_mapping)) >= current_num_components + 1];
% 
%             if strcmp(type, 'conservative') == 1
%                 % "conservative" addition: if a component is
%                 % instantiated then it must be connected to all
%                 % other instantiated components on the same level.
%                 % This is an over-design (some connections will be
%                 % redundant), which does not affect the overall
%                 % reliability but increases the cost.
%                 %
%                 % This approach has less iterations and is faster.
%                 if obj.canBeConnected(current, current)
%                     current_current_matrix = ...
%                         obj.AdjacencyMatrix.getSubMatrixNumeric(current, current);
%                     for j = 1:size(current_current_matrix,2)
%                         obj.Constraints = [obj.Constraints, ...
%                             implies(sum(current_mapping(:,j)) == 1,...
%                                 sum(current_current_matrix(:,j)) >= current_num_components, 1e-2)];
%                     end
%                 end
%             end
% 
%             str = sprintf('Red_degree = num_components, but req still UNSAT. Adding an extra %s', current);
%             disp(str);
%             res = res + 2;
%             return;
%         end
%     end
% end
%
%% old version of GetReliabilityConstraints from EPSProblem (with some normalization attempts that didn't work)
% function GetReliabilityConstraints(obj)
%             % formulate reliability constraints for the problem and add
%             % them to the list of auxiliary constraints
%             obj.numberOfPaths = containers.Map;
%             obj.approximate_reliabilities = containers.Map;
%             obj.numberOfPaths_index = containers.Map;
%             obj.estimated_reliabilities = MapN;
%             
%             % SCALING
%             obj.scaling_factor = obj.getScalingFactor;
%             
%             for i = 1:length(obj.Flow)-1
%                 current = obj.Flow{i};
%                 sink = obj.Flow{end};
%                 all_paths = obj.allPathsLayer(current, sink);                                                
%                 
%                 % g/pg, b/pb and so on
%                 num_components = size(all_paths.Matrix,1);
%                 num_sinks = size(all_paths.Matrix,2);
%                 powers = (1:num_components);
%                 
%                 % currently is is assumed that all components of the same
%                 % type have the same failure probability (though in the
%                 % library it is possible to have different values). We just
%                 % take the first value from the list of failure prob-s.
%                 fprobs = obj.Library.FailureProbabilities(current);
%                 prob_component_failure = fprobs(1);
%                 %prob_component_failure = fprobs(1) / max(obj.Requirement); % normalized
%                 %prob_component_failure = fprobs(1).^-1; % normalized ver 2
%                 %prob_component_failure = fprobs(1) ^ obj.scaling_factor; % scaled
%                 
%                 % approximate reliabilities for components of current type
%                 % (can be 1, p, 2p^2, 3p^3...). For each component one of
%                 % these values will be used depending on its degree of
%                 % redundancy.
%                 obj.approximate_reliabilities(current) = ...
%                     [1 powers].*[1 prob_component_failure.^powers];
%                 
%                 %% define decision variables for reliability
%                 % numberOfPaths(T)(1,j) = k if exactly k components of type
%                 % T are connected by at least one path to jth sink
%                 obj.numberOfPaths(current) = intvar(1, num_sinks);
%                 
%                 % numberOfPaths_index(T)(i,j) = 1 if exactly i-1 components
%                 % of type T are connected by at least one path to jth sink;
%                 % else numberOfPaths_index(T)(i,j) = 0;
%                 obj.numberOfPaths_index(current) = ...
%                     binvar(num_components + 1, num_sinks, 'full');
%                 
%                 % Auxiliary variables for the different contributions to the failure 
%                 % probability of each sink due to different component types
%                 obj.estimated_reliabilities(obj.Flow{i}, obj.Flow{end}) = ...
%                     sdpvar(1, num_sinks);
%                 
%                 % CONSTRAINTS
%                 % bounds for numberOfPaths (minimum - zero paths, maximum -
%                 % equal to max number of components of current type)
%                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     0 <= obj.numberOfPaths(current) <= num_components];
%                 
%                 % bounds for estimated failure prob-s (between 0 and 1)
%                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     Patterns.isProbability(obj.estimated_reliabilities(current, sink))];
%                 
%                 % numberOfPaths vector is related to allPaths structure for
%                 % current component and sink
%                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     obj.numberOfPaths(current) == sum(all_paths.Matrix(:,:,1),1)];
%                 
%                 % numberOfPaths_index(T)(i,j) = 1 if exactly i-1 components
%                 % of type T are connected by at least one path to jth sink;
%                 % else numberOfPaths_index(T)(i,j) = 0;
%                 current_num_paths = obj.numberOfPaths(current);
%                 current_index = obj.numberOfPaths_index(current);
%                 for j = 1:num_sinks
%                     for k = 0:num_components
%                         
%                         obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                             implies(current_num_paths(j) == k, ...
%                             current_index(k+1,j) == 1, 1e-2)];
% %                         obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
% %                             implies(current_num_paths(j) < k, ...
% %                             current_index(k+1,j) == 0)];
%                     end
%                 end
%                 
%                 % consistency of numberOfPaths_index(T): each column must
%                 % have single 1, rest - zeros.
%                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     sum(obj.numberOfPaths_index(current)) == ones(1, num_sinks)];
%                 
%                 % estimated failure probablities are obtained from
%                 % multiplying approximate reliabilities vector and
%                 % numberOfPaths_index matrix
%                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     obj.estimated_reliabilities(current, sink) == ...
%                     obj.approximate_reliabilities(current) * ...
%                     obj.numberOfPaths_index(current)];                                                                
%             end
%             
%             % final reliability constraint as a sum of contribututions of
%             % all components in the flow
%             obj.estimated_reliabilities(obj.Flow{end}) = sdpvar(1, num_sinks);
%             obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     Patterns.isProbability(obj.estimated_reliabilities(obj.Flow{end}))];
%                 
%             for i = 1:num_sinks
%                 reliability_at_sink = 0;
%                 for j = 1:length(obj.Flow)-1
%                     reliability_current_sink = ...
%                         obj.estimated_reliabilities(obj.Flow{j}, obj.Flow{end});
%                     reliability_at_sink = reliability_at_sink + ...
%                         reliability_current_sink(i);
%                 end
%                 
%                 est_rel = obj.estimated_reliabilities(obj.Flow{end});
%                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     est_rel(i) == reliability_at_sink];              
%                 
%                 % estimated reliability at each sink should satisfy the
%                 % reliability requirement
% %                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
% %                     est_rel(i) <= obj.Requirement(i)];
%                 %normalized_req = obj.Requirement / max(obj.Requirement);
%                 %normalized_req = obj.Requirement.^-1;
%                 %scaled_req = obj.Requirement.^obj.scaling_factor;
%                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     est_rel(i) <= obj.Requirement(i)];
% %                 obj.estimated_reliabilities(obj.Flow{end}) = ...
% %                     [obj.estimated_reliabilities(obj.Flow{end}), reliability_at_sink];
%             end
%         end
%
%% OLD VERSIONS OF DATA STRUCTURES CREATION
% one hop layers
%             obj.GBB_layer   = ConnOneHop('Generator', 'ACBus', obj.AdjacencyMatrix);
%             obj.BBBB_layer  = ConnOneHop('ACBus', 'ACBus', obj.AdjacencyMatrix);
%             obj.BBR_layer   = ConnOneHop('ACBus', 'Rectifier', obj.AdjacencyMatrix);
%             obj.RDD_layer   = ConnOneHop('Rectifier', 'DCBus', obj.AdjacencyMatrix);
%             obj.DDDD_layer  = ConnOneHop('DCBus', 'DCBus', obj.AdjacencyMatrix);
%             obj.DDL_layer   = ConnOneHop('DCBus', 'Load', obj.AdjacencyMatrix);
            
%             dims = size(obj.GBB_layer.Matrix);
%             num_acbuses = dims(2);
%             dims = size(obj.RDD_layer.Matrix);
%             num_dcbuses = dims(2);

% diff layers all hops layers
%             obj.GBB_layer_new = ConnDiffLayersAllHops(obj.GBB_layer, obj.BBBB_layer, num_acbuses);
%             obj.BBR_layer_new = ConnDiffLayersAllHops(obj.BBR_layer, obj.BBBB_layer, num_acbuses);
%             obj.RDD_layer_new = ConnDiffLayersAllHops(obj.RDD_layer, obj.DDDD_layer, num_dcbuses);
%             obj.DDL_layer_new = ConnDiffLayersAllHops(obj.DDL_layer, obj.DDDD_layer, num_dcbuses);
            
%             obj.Constraints = obj.GBB_layer_new.getConstraints(obj.Constraints);
%             obj.Constraints = obj.BBR_layer_new.getConstraints(obj.Constraints);
%             obj.Constraints = obj.RDD_layer_new.getConstraints(obj.Constraints);
%             obj.Constraints = obj.DDL_layer_new.getConstraints(obj.Constraints);

% all paths layers
%             obj.BR_complete = ConnAllPaths(obj.BBR_layer_new);
%             obj.RD_complete = ConnAllPaths(obj.RDD_layer_new);
%             obj.DL_complete = ConnAllPaths(obj.DDL_layer_new);
%             obj.RL_complete = ConnAllPaths(obj.RDD_layer, obj.DL_complete);
%             obj.BL_complete = ConnAllPaths(obj.BR_complete, obj.RL_complete);
%             obj.GL_complete = ConnAllPaths(obj.GBB_layer, obj.BL_complete);
%             obj.BD_complete = ConnAllPaths(obj.BR_complete, obj.RD_complete);
%             obj.GD_complete = ConnAllPaths(obj.GBB_layer, obj.BD_complete);
%             
%             obj.Constraints = obj.BR_complete.getConstraints(obj.Constraints);
%             obj.Constraints = obj.RD_complete.getConstraints(obj.Constraints);
%             obj.Constraints = obj.DL_complete.getConstraints(obj.Constraints);
%             obj.Constraints = obj.RL_complete.getConstraints(obj.Constraints);
%             obj.Constraints = obj.BL_complete.getConstraints(obj.Constraints);
%             obj.Constraints = obj.GL_complete.getConstraints(obj.Constraints);
%             obj.Constraints = obj.BD_complete.getConstraints(obj.Constraints);
%             obj.Constraints = obj.GD_complete.getConstraints(obj.Constraints);

%% SOME OLD CODE FOR MAPPINGS 
% mapping using *_complete structures
%             mapping_layer = containers.Map;
%             for i = 1:length(obj.components_variable)
%                 component = obj.components_variable{i};
%                 path_indicator_matrix = obj.allPathsLayer(component, obj.Flow{end});
%                 mapping_layer(component) = path_indicator_matrix.Matrix(:,:,1);     % might crash if matrix is 2D
%             end
%
%             mapping_layer('Generator')  = obj.GL_complete.Matrix(:,:,1);
%             mapping_layer('ACBus')      = obj.BL_complete.Matrix(:,:,1);
%             mapping_layer('Rectifier')  = obj.RL_complete.Matrix(:,:,1);
%             mapping_layer('DCBus')      = obj.DL_complete.Matrix(:,:,1);
%
%             [obj.LibraryMapping('Generator'), obj.Constraints] = ...
%                 obj.Library.createMapping('Generator', mapping_layer_generator, obj.Constraints);
%             [obj.apu_mapping, obj.Constraints] = ...
%                 obj.Library.createMapping('Generator', mapping_layer_apu, obj.Constraints);
%             [obj.acbus_mapping, obj.Constraints] = ...
%                 obj.Library.createMapping('ACBus', mapping_layer_acbus, obj.Constraints);
%             [obj.rectifier_mapping, obj.Constraints] = ...
%                 obj.Library.createMapping('Rectifier', mapping_layer_rectifier, obj.Constraints);
%             [obj.dcbus_mapping, obj.Constraints] = ...
%                 obj.Library.createMapping('DCBus', mapping_layer_dcbus, obj.Constraints);
%
%% GetScalingFactor method from EPS problem (didn't work out)
% function res = getScalingFactor(obj)
%             % calculate the scaling factor for input values
%             % NOTEL currently only for reliability values. They might be
%             % too small, which will cause numerical problems in the solver.
%             % Therefore, it is necessary to scale them, but still keep in
%             % the range 0-1 (probabilities). This can help to work with
%             % smaller numbers (e.g. 1e-4).
%             fprobs = obj.getFailureProbabilitiesAsVector;
%             max_fprob = max(fprobs);
%             max_power = floor(log10(max_fprob));
%             res = -(1 / max_power);
%         end 
%
%% GetOperationalConstraints (from RPL problem), not used in current version + partly moved to Linearization.m (aux constraints)
% function GetOperationalConstraints(obj)
%     % ???
%     obj.numOutEdges = containers.Map;
%     obj.numOutEdgesIndex = containers.Map;
%     adj_matrix = obj.AdjacencyMatrix.getMatrixNumeric;
% 
%     for i = 1:length(obj.Flow)-1
%         current_list = {obj.Flow{i}};
%         next_list    = {obj.Flow{i+1}};
% 
%         current_adjm_keys = obj.AdjacencyMatrix.getMatchingKeys(obj.Flow{i}, obj.Flow{i});
%         next_adjm_keys    = obj.AdjacencyMatrix.getMatchingKeys(obj.Flow{i+1}, obj.Flow{i+1});
%         for item = 1:length(current_adjm_keys)
%             current_list{item+1} = current_adjm_keys{item};
%         end
% 
%         for item = 1:length(next_adjm_keys)
%             next_list{item+1} = next_adjm_keys{item};
%         end
% 
%         for j = 1:length(current_list)
%             current = current_list{j};
%             next    = next_list{j};
%             current_to_next = ...
%                 obj.AdjacencyMatrix.getSubMatrixNumeric(current, next);
%             num_current = size(current_to_next,1);
%             num_next    = size(current_to_next,2);
%             idx_current = obj.AdjacencyMatrix.getGlobalIndices(current);
%             idx_next    = obj.AdjacencyMatrix.getGlobalIndices(next);      
% 
%             % decision vars and constraints
%             obj.numOutEdges(current) = intvar(1, num_current);
% 
%             if Helpers.canBeConnected(obj, current, current)
%                 obj.numOutEdgesIndex(current) = binvar(num_next + num_current, num_current);
%             else
%                 obj.numOutEdgesIndex(current) = binvar(num_next + 1, num_current);
%             end
% 
%             % constraints for numOutEdges (int vars)
%             obj.addConstraint( 0 <= obj.numOutEdges(current) <= num_next); % bug?
% 
%             %% ASSUMPTION: only adjacent components can be connected (e.g. obj.Flow{i} can't be connected to obj.Flow{i+2})
%             obj.addConstraint( ...
%                 obj.numOutEdges(current) == ...
%                 sum(adj_matrix(idx_current(1):idx_current(2), ...
%                 [idx_current(1):idx_current(2), idx_next(1):idx_next(2)]),2)');
% 
%             current_num_edges   = obj.numOutEdges(current);
%             current_index       = obj.numOutEdgesIndex(current);
%             for k = 1:size(current_index,2)
%                 for l = 0:size(current_index,1)-1
%                     % binary unroll of numOutEdges
%                     obj.addConstraint( ...
%                         implies(current_num_edges(k) == l, ...
%                         current_index(l+1,k) == 1, 1e-2));
%                 end
%             end
% 
%             % only one "1" in each column of numOutEdgesIndex
%             obj.addConstraint( ...
%                 sum(obj.numOutEdgesIndex(current)) == ones(1,num_current));
%         end
%     end
% end
%
%% Old versions of some linearization routines (now moved to Linearization class but old versions will be kept just in case)
% function res = linearizeDenominator(obj, component, idx)
%     % this function provides a linearization to what is supposed to
%     % be a denominator in a division operation (which is nonlinear)
%     % Denominator is an intvar, instead, this function replaces it
%     % with a sum of binvars. This sum can then be used as a
%     % multiplier, not as a denominator.
%     %
%     % The function returns a vector of binvars multiplied by
%     % corresponding coefficients.
%     current_index = obj.numOutEdgesIndex(component);
%     res = [];
%     for i = 2:size(current_index,1)
%         %res = res + (1/i-1) * current_index(i,idx);
%         %res = [res, (1/(i-1)) * current_index(i,idx)];
%         res = [res, (1/(i-1)) * current_index(i,idx)];
%         %res{i-1} = (1/(i-1)) * current_index(i,idx);
%     end
% end
% 
% function res = linearizeProduct(obj, lhs, rhs)
%     % linearization of product of continuous variable and binary
%     % variable (lhs is continuous, rhs is binary)
%     if ~any(isa(lhs, 'sdpvar'))
%         % special case when lhs is just a double (e.g. when
%         % calculating flows between source and following component,
%         % lhs is a constant, not a decision var yet)
%         %lhs = cell2mat(lhs);
%         lhs = sum(lhs);
%         res = lhs * rhs;
%     else
%         res = sdpvar(1, length(lhs) * length(rhs), 'full');
%         idx = 1;
%         big_M = 10000;
%         for i = 1:length(lhs)
%             for j = 1:length(rhs)
%                 rhs_base = getbase(rhs(j));
%                 rhs_base = rhs_base(2);
%                 %res(idx) = res(idx) * rhs_base;
% 
%                 % sdpvar * binvar product linearization (z = A*x)
%                 % (A - sdpvar, x = binvar)
%                 % need to add 3 linearization constraints:
%                 % 1) z <= big_M * x
%                 A = lhs(i);
%                 x = rhs(j)/rhs_base;
%                 z = res(idx);
% 
%                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     z/rhs_base <= big_M * x];
%                 % 2) z <= A
%                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     z/rhs_base <= A];
%                 % 3) z >= A - (1-x)*big_M
%                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     z/rhs_base >= A - (1-x)*big_M];
%                 % 4) z >= 0;
%                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     z/rhs_base >= 0];
% 
%                 idx = idx + 1;
%             end                    
%         end
%     end
% end
% 
% function res = linearizeBinProduct(obj, lhs, rhs)
%     % linearize the product
%     % inputs: vectors, each entry is a binary decision variable
%     % (expect the special case)
%     % output: a vector of binvars, which, when summed, represent
%     % the product of lhs and rhs
%     %if ~any(cellfun('isclass', lhs, 'sdpvar'))
%     if ~any(isa(lhs, 'sdpvar'))
%         % special case when lhs is just a double (e.g. when
%         % calculating flows between source and following component,
%         % lhs is a constant, not a decision var yet)
%         %lhs = cell2mat(lhs);
%         lhs = sum(lhs);
%         res = lhs * rhs;
% %                 res = [];
% %                 for i = 1:length(rhs)
% %                     item = rhs(i);                    
% %                     res = [res, lhs * item{1}]; 
% %                 end
%     else
%         %lhs = lhs(lhs~=0);
%         %lhs = obj.removeZeros(lhs);
%         res = binvar(1, length(lhs) * length(rhs), 'full');
%         idx = 1;
%         for i = 1:length(lhs)
% %                     lhs_item_symbolic = char(sdisplay(lhs(i)));
% %                     lhs_item_symbolic = strsplit(lhs_item_symbolic, '*');
% %                     lhs_item_coef = lhs_item_symbolic{1};
%             %lhs_item = lhs{i};
%             %lhs_item = lhs_item{1};
%             lhs_base = getbase(lhs(i));
%             lhs_base = lhs_base(2);
%             for j = 1:length(rhs)                        
% %                         rhs_item_symbolic = char(sdisplay(rhs(j)));                        
% %                         rhs_item_symbolic = strsplit(rhs_item_symbolic, '*');                        
% %                         rhs_item_coef = rhs_item_symbolic{1};
%                 %rhs_item = rhs{i};
%                 %rhs_item = rhs_item{1};
%                 rhs_base = getbase(rhs(j));
%                 rhs_base = rhs_base(2);
%                 res(idx) = res(idx) * lhs_base * rhs_base;
% 
%                 % binvar product linearization (X = A*B)
%                 % need to add 3 linearization constraints:
%                 % 1) X <= A
%                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     res(idx)/(lhs_base*rhs_base) <= lhs(i)/lhs_base];
%                 % 2) X <= B
%                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     res(idx)/(lhs_base*rhs_base) <= rhs(j)/rhs_base];
%                 % 3) X >= A + B - 1
%                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     res(idx)/(lhs_base*rhs_base) >= ...
%                     lhs(i)/lhs_base + rhs(j)/rhs_base - 1];
% 
%                 idx = idx + 1;
%             end
%         end
%     end
% end
%
%% Initial version of creating flow rate matrices in RPL problem (is now generalized and moved to Helpers, most constraints definition are moved to Patterns)
% function flow_rate_matrix_set = createFlowRateMatrices(obj, op_mode)
%     % create a matrix (same dimensions as the adjacency matrix),
%     % whose entries represent the flow rate at a particular edge
%     flow_rate_matrix_set = containers.Map;
%     full = 0;
% 
%     adjmatrix_size = obj.AdjacencyMatrix.Size;
%     adj_matrix = obj.AdjacencyMatrix.getMatrixNumeric;
%     adjm_keys = obj.AdjacencyMatrix.Row_keys;                                 
% 
%     for i = keys(op_mode)
%         %% ToDo: generalize!
%         val = op_mode(char(i));
%         if val == 0
%             % set the flag
%             full = 1;
%         end
%     end
% 
%     for i = keys(op_mode)
%         % flow matrix is a 2D cell array, because we will store
%         % different data types (num and sdpvar)
%         flow_matrix = num2cell(zeros(adjmatrix_size));
% 
%         if op_mode(char(i)) == 0
%             % if the production line (e.g. line "A") is not used in
%             % this operation mode, we do not consider it
%             flow_rate_matrix_set(char(i)) = flow_matrix;
%             continue;
%         end
% 
%         for j = 1:length(obj.Flow)-1 % ?? overflow avoid
%             curr_component = obj.Flow{j};
%             next_component = obj.Flow{j+1};                  
% 
%             if ~full                                                    
%                 % use only subpart (e.g. "Machine1 A")                        
%                 curr_component = sprintf('%s %s', curr_component, char(i));
%                 next_component = sprintf('%s %s', obj.Flow{j+1}, char(i));
%             end
% 
%             if j == 1
%                 % this is a source
%                 if full
%                     curr_component = sprintf('%s %s', curr_component, char(i));
%                 end                        
%                 idx_current = obj.AdjacencyMatrix.getGlobalIndices(curr_component);
%                 idx_next = obj.AdjacencyMatrix.getGlobalIndices(next_component);                                                
% 
%                 input_rates = obj.Library.InputFlowRates(obj.Flow{j});
%                 for k = idx_current(1):idx_current(2)
%                     % add initial input rates to the flow matrix
%                     %% The ordering might not be correct (To check)
%                     rate = input_rates(k); % note!
%                     % multiply the original rate by a coefficient
%                     % related to required operation mode (e.g. 2A)
%                     coef = op_mode(char(i));
%                     rate = rate * coef;
% 
%                     for l = idx_next(1):idx_next(2)
%                         flow_matrix{k,l} = rate * adj_matrix(k,l);
%                     end
%                 end
%             else
%                 % not a source - proceed with recurrently deriving
%                 % the flow rate values                        
%                 idx_current = obj.AdjacencyMatrix.getGlobalIndices(curr_component);
%                 idx_next = obj.AdjacencyMatrix.getGlobalIndices(next_component);
%                 general_idx_current = obj.AdjacencyMatrix.getGlobalIndices(obj.Flow{j});
%                 idx_current_vector = [idx_current(1):idx_current(2)];
%                 general_idx_current_vector = [general_idx_current(1):general_idx_current(2)];
%                 idx_final = [];
% 
%                 if obj.canBeConnected(curr_component, curr_component)
%                     % merge current and next indices
%                     %% NOTE: we assume that both current and next indices are a single array (no need to merge them) - possible problem for complex scenarios
%                     for idx = idx_current(1):idx_current(2)
%                         idx_final = [idx_final, idx];
%                     end                            
%                 end
% 
%                 for idx = idx_next(1):idx_next(2)
%                     % in any case next indices should be there
%                     idx_final = [idx_final, idx];
%                 end
% 
%                 for k = idx_current(1):idx_current(2)
%                     for l = idx_final
%                         if k ~= l
%                             flow_matrix{k,l} = sdpvar(1,1);
%                             % sometimes values are <0, shouldn't be
%                             %% looks like a bound constraint
%                             obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints,...
%                                 flow_matrix{k,l} >= 0];
%                         end
%                     end
%                 end
% 
%                 for k = idx_current(1):idx_current(2)
%                     % derive the flow rate for edges, which
%                     % possibly can be connected to current
%                     % component
%                     kth_column_sum = [];
%                     for l = 1:idx_current(2)
%                         % sum the cell elements (can't use "sum")
%                         if isa(flow_matrix{l,k}, 'double') && (flow_matrix{l,k} == 0)
%                             continue;
%                         end
%                         kth_column_sum = [kth_column_sum, flow_matrix{l,k}];
%                     end
% 
%                     kth_row_sum = [];
%                     for l = 1:size(flow_matrix,2)
%                         if isa(flow_matrix{k,l}, 'double') && (flow_matrix{k,l} == 0)
%                             continue;
%                         end
%                         kth_row_sum = [kth_row_sum, flow_matrix{k,l}]; 
%                     end
% 
%                     % balance equation
%                     obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                         sum(kth_row_sum) == sum(kth_column_sum)];
% 
%                     % linearize the division
% %                             if full
% %                                 idx_mapped = find(ismember(general_idx_current_vector, idx_current_vector));                    
% %                                 idx_mapped = [idx_mapped(1):idx_mapped(end)];
% %                             else
% %                                 idx_mapped = [1:length(idx_current_vector)];
% %                             end
% %                             linear_factor = ...
% %                                 obj.linearizeDenominator(curr_component, idx_mapped(k-idx_current(1)+1));
% 
%                     for l = idx_final
%                         if k == l
%                             continue;
%                         end
%                         [current_flow, cons] = ...
%                             Linearization.linearizeProduct(flow_matrix{k,l}, adj_matrix(k,l));
%                         prob.addAuxConstraint(cons);
% %                                 current_flow = ...  
% %                                     obj.linearizeProduct(kth_column_sum, linear_factor);
% %                                 current_flow = ...
% %                                     obj.linearizeProduct(current_flow, adj_matrix(k,l));
%                         %flow_matrix{k,l} = sum(current_flow);
% 
%                         % balance-2 ??
%                         %% looks more like linearization still; move to linearization?
%                         obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                             flow_matrix{k,l} == sum(current_flow)];
%                             %kth_column_sum * adj_matrix(k,l);
%                             %(kth_column_sum / sum(adj_matrix(k,:))) * adj_matrix(k,l);
%                     end                            
%                 end
%             end
%         end
%         flow_rate_matrix_set(char(i)) = flow_matrix;
%         sink = obj.Flow{end};
%         curr_product_sink = sprintf('%s %s', sink, char(i));
%         idx_sink = obj.AdjacencyMatrix.getGlobalIndices(sink);
%         idx_curr_sink = obj.AdjacencyMatrix.getGlobalIndices(curr_product_sink);
%         idx_prev = obj.AdjacencyMatrix.getGlobalIndices(obj.Flow{end-1});
%         for j = idx_sink
%             if any(idx_curr_sink == j)
%                 % ignore current sink type
%                 continue;
%             end
%             sum_inputs_sink = 0;
%             for k = idx_prev(1):idx_prev(2)
%                 sum_inputs_sink = sum_inputs_sink + flow_matrix{k,j};                        
%             end
%             if isa(sum_inputs_sink, 'sdpvar')
%             % e.g. product A must not get to sink B
%                 obj.AuxiliaryConstraints = [obj.AuxiliaryConstraints, ...
%                     sum_inputs_sink == 0];
%             end
%         end
%     end
% end
%
%% Some trash from patterns
%% flow_balance
% for tag = keys(matrix_set)
%     flow_matrix = matrix_set(char(tag));
%     for k = idx_current(1):idx_current(2)
%         kth_column_items = [];
%         kth_row_items    = [];
% 
%         % Assumption: input flow can come only from
%         % preceding or current components
%         for l = 1:idx_current(2)                            
%             if isa(flow_matrix{l,k}, 'double') && (flow_matrix{l,k} == 0)
%                 continue;
%             end                
%             % create a vector of cell elements
%             kth_column_items = [kth_column_items, flow_matrix{l,k}];
%         end                        
% 
%         % Assumption: output flow can go only to current or
%         % succeeding components
%         for l = idx_current(1):size(flow_matrix,2)
%             if isa(flow_matrix{k,l}, 'double') && (flow_matrix{k,l} == 0)
%                 continue;
%             end
%             kth_row_items = [kth_row_items, flow_matrix{k,l}]; 
%         end
% 
%         % balance equation
%         milp_constraint = [milp_constraint, ...
%             sum(kth_row_items) == sum(kth_column_items)];
%     end
% end
%
%% no_overloads
% function [ milp_constraint ] = no_overloads( prob, T, S )
%     % Components of type T with subtype S must not be overloaded,
%     % i.e. the input flow rate must be less than or equal to
%     % component's processing rate. Otherwise component won't be
%     % able to handle the input traffic and the queue will grow
%     % infinitely.
%     % NOTE: S is optional
%     milp_constraint = [];
%     source = prob.Flow{1};
%     T_lib = prob.Library.findRelatedType(T);
%     processing_rates = prob.Library.ProcessingRates(T_lib);
%     input_rates = prob.Library.InputFlowRates(source);
% 
%     for op_mode = keys(prob.flowRateMatrices)
%         Q = prob.flowRateMatrices(char(op_mode));
% 
%         % get submatrix of Q representing input flow to T (Q_in)
%         Q_in = Helpers.getIncomingOutgoing(prob, Q, T);
% 
%     end
% 
%     for req = prob.Requirement.Operation
%         req = req{1};
%         op_mode = req.mode;
%         flow_rate_matrix_set = prob.flowRateMatrices(op_mode);
%         full = 0; % flag to use all product types                
%         for k = keys(req.map)
%             val = req.map(char(k));
%             if val == 0
%                 % set the flag
%                 full = 1;
%             end
%         end
% 
%         for k = keys(req.map)                    
%             if req.map(char(k)) == 0
%                 % if there is no flow of product k in this
%                 % operation mode then don't consider it
%                 continue
%             end
%             T = strsplit(T);
%             T = char(T{1});
%             if ~full
%                 %% ToDo: generalize to list of joinable product types!
%                 T = sprintf('%s %s', T, char(k));
%             end
% 
%             flow_rate_matrix = flow_rate_matrix_set(char(k));                        
%             T_mapping = prob.LibraryMapping(T);
%             mapped_proc_rates = processing_rates * T_mapping;
%             C = Helpers.getConnectivityMatrix(prob, T, T);
%             num_items = size(C,1);
%             global_idx = prob.AdjacencyMatrix.getGlobalIndices(T);
%             starting_idx = global_idx(1);
%             for i = 1:num_items
%                 % calculate the rate difference for a particular
%                 % machine and add it to the sum of rate diffs
%                 proc_rate  = mapped_proc_rates(i);
%                 input_rate = flow_rate_matrix(:,starting_idx+i-1)';
%                 input_rate_sum = 0;
%                 for j = 1:starting_idx-1
%                     % sum the cell elements (can't use "sum")
%                     if isa(input_rate{j}, 'double') && (input_rate{j} == 0)
%                         continue;
%                     end
%                     input_rate_sum = input_rate_sum + input_rate{j};
%                 end                       
%                 milp_constraint = [milp_constraint, input_rate_sum <= proc_rate];
%             end
%         end
%     end                        
% end