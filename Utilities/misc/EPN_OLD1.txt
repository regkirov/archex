%%% Sample problem for EPS %%%
% Problem name
BEGIN(Description)
Name:EPS test problem;
Variable components:Generator,ACBus,Transformator,LVACBus,Rectifier,DCBus;
Fixed components:Load;
Functional flow:Generator,ACBus,Transformator,LVACBus,Rectifier,DCBus,Load;
Spatial types:Left,Middle,Right;
Edge cost:1000;
Edge failure:0;
END(Description)

% This section includes template parameters (how many components of each type on each side).
% 
BEGIN(Number of components)
[Left] Generator 3;
[Left] ACBus 3;
[Left] Transformator 3;
[Left] LVACBus 3;
[Left] Rectifier 3;
[Left] DCBus 3;
[Left] Load 3;
[Right] Generator 3;
[Right] ACBus 3;
[Right] Transformator 3;
[Right] LVACBus 3;
[Right] Rectifier 3;
[Right] DCBus 3;
[Right] Load 3;
[Middle] Generator 1;
END(Number of components)

BEGIN(Requirements)
Reliability:1e-4,1e-4,1e-4,1e-4,1e-4,1e-4;
END(Requirements)

%% TODO: change so they look like constraints below
BEGIN(Structure)
[Left]      Generator   => [Left]     ACBus;
[Right]     Generator   => [Right]    ACBus;
[Middle]    Generator   => [Left]     ACBus;
[Middle]    Generator   => [Right]    ACBus;
[Left]      ACBus       => [Left]     ACBus;
[Left]      ACBus       => [Right]    ACBus;
[Left]      ACBus       => [Left]     Transformator;
[Right]     ACBus       => [Right]    Transformator;
[Right]     ACBus       => [Left]     ACBus;
[Right]     ACBus       => [Right]    ACBus;
[Left]      Transformator   => [Left]     LVACBus;
[Right]     Transformator   => [Right]    LVACBus;
[Left]      LVACBus       => [Left]     LVACBus;
[Left]      LVACBus       => [Right]    LVACBus;
[Left]      LVACBus       => [Left]     Rectifier;
[Right]     LVACBus       => [Right]    Rectifier;
[Right]     LVACBus       => [Left]     LVACBus;
[Right]     LVACBus       => [Right]    LVACBus;
[Left]      Rectifier   => [Left]     DCBus;
[Right]     Rectifier   => [Right]    DCBus;
%[Left]      DCBus       => [Right]    DCBus;
%[Left]      DCBus       => [Left]     DCBus;
[Left]      DCBus       => [Left]     Load;
%[Right]     DCBus       => [Left]     DCBus;
%[Right]     DCBus       => [Right]    DCBus;
[Right]     DCBus       => [Right]    Load;
END(Structure)

BEGIN(Constraints)
% NOTE: extra spaces in brackets are not handled at the moment and might
% cause troubles
at_most_N_connections(Generator Left,ACBus Left,1);
at_most_N_connections(Generator Right,ACBus Right,1);
at_most_N_connections(Transformator,ACBus,1);
at_most_N_connections(Transformator,LVACBus,1);
at_most_N_connections(Rectifier,LVACBus,1);
at_most_N_connections(Rectifier,DCBus,1);

%incoming_then_outgoing(DCBus,DCBus|Load,Rectifier);
incoming_then_outgoing(DCBus,Load,Rectifier);
incoming_then_outgoing(ACBus,ACBus|Transformator,Generator);
incoming_then_outgoing(LVACBus,LVACBus|Rectifier,Transformator);
incoming_then_outgoing(Transformator,LVACBus,ACBus);
incoming_then_outgoing(Rectifier,DCBus,LVACBus);

not_outgoing_then_not_incoming(ACBus,Transformator,ACBus);
%not_outgoing_then_not_incoming(DCBus,Load,DCBus);
not_outgoing_then_not_incoming(DCBus,Load,Rectifier);
not_outgoing_then_not_incoming(ACBus,Transformator|ACBus,Generator Middle); // ??? APU
not_outgoing_then_not_incoming(LVACBus,Rectifier|LVACBus,Transformator);

no_self_loops(ACBus);
no_self_loops(LVACBus);
%no_self_loops(DCBus);

exactly_N_connections(Load,DCBus,1);
exactly_N_connections(Generator Middle,ACBus Left,1); // ??? APU
exactly_N_connections(Generator Middle,ACBus Right,1);

bidirectional_connection(ACBus Left,ACBus Right);
bidirectional_connection(LVACBus Left,LVACBus Right);
%bidirectional_connection(DCBus Left,DCBus Right);

power_prefer(Generator Left,Load Left);
power_prefer(Generator Right,Load Right);
power_prefer(Generator Middle,Load Left+Load Right); // ???

%extra
at_most_N_connections(ACBus,Transformator,1);
at_most_N_connections(LVACBus,Rectifier,1);
%at_most_N_connections(ACBus Right,Generator Right,1);
%at_most_N_connections(ACBus Left,Generator Left,1);
%incoming_then_outgoing(ACBus Left,Generator Middle,Generator Left);
%incoming_then_outgoing(ACBus Right,Generator Middle,Generator Right);
%incoming_then_outgoing(DCBus,Rectifier,DCBus|Load);
%incoming_then_outgoing(ACBus,Generator,ACBus|Rectifier);
%incoming_then_outgoing(Rectifier,ACBus,DCBus);
not_outgoing_then_not_incoming(ACBus Left,Generator Left,Generator Middle);
not_outgoing_then_not_incoming(ACBus Right,Generator Right,Generator Middle);
END(Constraints)