function [ res ] = logCleaner( varargin )
%LOGCLEANER removes old log files from the Output folder. 
% INPUT: maximum number of logs to be stores in the folder (if current
% amount is bigger, some of them (the oldest) have to be delete) and number
% of days to log expiration (all logs older than this number will be
% deleted).
%
% Function can be run either with one or both of these parameters. In the
% latter case max_logs_allowed parameter has greater priority. Program
% first tries to satisfy this number. If there are still logs older than
% days_to_expire, then they will also be deleted afterwards.
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: July 2016

nVarargs = length(varargin);
max_logs_allowed = -1;
days_to_expire = -1;
res = 0;

% default working dir
working_dir = 'Output';

% process input arguments (either one or two parameters are possible)
if even(nVarargs)
    for i = 1:nVarargs
        if strcmp(varargin(i), 'max_logs_allowed') == 1
            max_logs_allowed = cell2mat(varargin(i+1));
        elseif strcmp(varargin(i), 'days_to_expire') == 1
            days_to_expire = cell2mat(varargin(i+1));
        elseif strcmp(varargin(i), 'dir') == 1
            working_dir = char(varargin(i+1));
        end
    end
else
    error('logCleaner: incorrect number of inputs. Each key must have a value.');
end

% go to working directory
previous_dir = pwd;
cd(working_dir);
logs_list = dir;

% get a cell array of existing log names
filenames = {};
% also create a list of timestamps
timestamps = {};
j = 1;
for i = 1:length(logs_list)
    item = logs_list(i);
    if (item.isdir == 1) || (strcmp(item.name(1), '.') == 1)
        % skip '.' and '..' names (current and parent dir) and others
        continue;
    end
    filenames{j} = item.name;            
    pattern = '(?<=[A-Z]+_problem_)\w*';
    stamp = regexp(item.name, pattern, 'match');
    timestamps{j} = char(stamp);
    j = j + 1;
end

% sort filenames by their timestamps
[val, idx] = sort(timestamps);
filenames = filenames(idx);

str = sprintf('LogCleaner: Cleaning old logs in %s...', working_dir);
disp(str);

% delete logs up to max_logs_allowed
% NOTE: it is assumed that logs are ordered (this is implicitly so due to
% file naming with timestamps). The first log in the list is the oldest.
if max_logs_allowed ~= -1
    while length(filenames) >= max_logs_allowed
        % keep deleting old logs until there are less than allowed
        filename = filenames{1};
        delete(filename);
        filenames = filenames(2:end);
        res = res + 1;
    end
end

% also delete logs older than days_to_expire
current_time = datetime('now');
if days_to_expire ~= -1
    %pattern = '_\d{2}_[A-Za-z]{3}_\d{4}_\d{2}_\d{2}_\d{2}';
    for i = 1:length(logs_list)
        item = logs_list(i);
        if (item.isdir == 1) || (strcmp(item.name(1), '.') == 1)
            % skip directories like '.', '..' and any nested ones
            continue;
        end
        
        diff = current_time - datetime(item.date);
        if diff > days_to_expire
            filename = item.name;
            delete(filename);
            res = res + 1;
        end
    end
end

if res > 0
    str = sprintf('LogCleaner: Done cleaning. Deleted %d old log(s) in %s.', res, working_dir);
    disp(str);
else
    str = sprintf('LogCleaner: There are currently no old logs to delete in %s', working_dir);
    disp(str);
end

% get back to original dir
cd(previous_dir);

end

