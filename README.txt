ArchEx Framework Release 2.0
ArchEx (Architecture Exploration) is an extensible tool for optimized 
selection of cyber-physical system (CPS) architectures.
  
Authors: Dmitrii Kirov,   University of Trento, Italy
         Pierluigi Nuzzo, University of Southern California, USA

Previous versions contributors:
         Nikunj Bajaj,  U.C. Berkeley
         Michele Lora,  U.C. Berkeley and U. Verona, Italy
         
For installation and using details, please refer to the wiki of this 
project. Implementation details related to the main modules of ArchEx 
can be found in corresponding .m files (type "help <module_name>" to get 
the available documentation). We are currently working on the offline 
version of the ArchEx manual.

Wiki link:      https://bitbucket.org/regkirov/archex/wiki/Home
Issue tracking: https://bitbucket.org/regkirov/archex/issues?status=new&status=open

---------------------------------------------------------------------
What is ArchEx? 
---------------------------------------------------------------------
ArchEx is an extensible framework for CPS architecture exploration. 
The problem is formulated as a mapping problem, where "virtual" components
are mapped into "real" ones from pre-defined libraries to minimize an 
objective function while guaranteeing that system requirements are satisfied.
The framework leverages an extensible set of patterns, a graph-based internal
representation of the system architecture, and a set of algorithms based on
mixed integer linear programming (MILP) to solve the mapping problem.

One of the key contributions of ArchEx is that it significantly lowers the
burden of formulating the exploration problem by hiding the complex details
of actual MILP representation from the user (designer) and allowing him/her 
to operate on a level of a pattern-based formal language. The latter requires
much less expertise.

References: 

- D. Kirov et al. "Optimized Selection of Wireless Network Topologies and
  Components via Efficient Pruning of Feasible Paths", Proc. DAC, Jun. 2018.
- D. Kirov et al. "ArchEx: An Extensible Framework for the Exploration of 
  Cyber-Physical System Architectures", Proc. DAC, Jun. 2017.
- N. Bajaj, et al. "Optimized Selection of Reliable and Cost-Effective
  Cyber-Physical System Architectures", Proc. DATE, Mar. 2015.
- P. Nuzzo, et al. "A contract-based methodology for aircraft electric 
  power system design", IEEE Access, vol. 2, pp. 1-25, 2014.


---------------------------------------------------------------------
Instructions 
---------------------------------------------------------------------
ArchEx installation instructions can be found in wiki:
https://bitbucket.org/regkirov/archex/wiki/Installation

Getting started:
https://bitbucket.org/regkirov/archex/wiki/Getting%20started%20with%20ArchEx


---------------------------------------------------------------------
License 
---------------------------------------------------------------------
 Copyright (c) 2014-18 University of California, Berkeley
                       University of Southern California
                       University of Trento

 All rights reserved.

 Redistribution and use of this software or part of it, with or without
 modification, are permitted provided that the following conditions are
 met:

 * Redistributions of this software or part of it must retain the above 
 copyright notice, this list of conditions and the following disclaimer.
 * Neither the name of the UC Berkeley nor the names of its
 contributors may be used to endorse or promote products derived
 from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
---------------------------------------------------------------------

---------------------------------------------------------------------
Acknowledgements
---------------------------------------------------------------------
This work was supported in part by the TerraSwarm Research Center, 
one of six centers supported by the STARnet phase of the Focus Center 
Research Program (FCRP), a Semiconductor Research Corporation program 
sponsored by MARCO and DARPA.
---------------------------------------------------------------------
