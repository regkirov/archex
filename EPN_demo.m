%% ARCHitectural EXploration Framework (ArchEx 2.0) - EPN DEMO
%
% This script demonstrates the ArchEx functionality on the case study of
% Electrical Power Networks (EPN). Provided architectural templates are
% based on the Honeywell patent for EPN of a passenger aircraft (e.g., 
% Boeing 787 Dreamliner).
%
% For details please refer to EPN_DEMO_README.txt.
%
% Authors: Dmitrii Kirov and Pierluigi Nuzzo
% Last modified: March 2017

%% Demo problem input file
%  Please select one of the problems and then just run the script.
%
%% ========================================================================
% EPN_DEMO1: EPN architecture from Honeywell patent, with High/Low voltage
% parts, which can be connected via a Transformer.
problem_file = 'demos/EPN_DEMO1.txt';
%% ========================================================================
% EPN_DEMO2: Similar to the first one, but with DC bus requirements.
% Result - more buses used, more expensive architecture.
%problem_file = 'demos/EPN_DEMO2.txt';
%% ========================================================================
% EPN_DEMO3: Similar to the first one, but it is possible to connect LV AC
% bus to APU and there are two mandatory TRUs, which connect HV and LV
% parts (one left and one right).
%problem_file = 'demos/EPN_DEMO3.txt';
%% ========================================================================
% EPN_DEMO4: example of an UNFEASIBLE problem.
%problem_file = 'demos/EPN_DEMO4.txt';
% =========================================================================

%% launch ArchEx for specified demo EPN problem
ArchEx('EPN', problem_file, 'demos/DEMO_EPN_LIB.txt');
